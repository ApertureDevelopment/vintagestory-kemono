"""
Script to generate player animations table and animation overrides.
"""

import csv
import json
import json5
from tabulate import tabulate
import os
import argparse


parser = argparse.ArgumentParser(description="Generate horse hair models from seraph hair models.")

parser.add_argument(
    "--path",
    metavar="path",
    type=str,
    help="Path to game assets, e.g. C:\\Users\\X\\AppData\\Roaming\\Vintagestory\\assets"
)

args = parser.parse_args()

path_assets = args.path

if path_assets is None:
    # assume this is windows and use appdata
    path_appdata = os.getenv("APPDATA")
    path_assets = os.path.join(path_appdata, "Vintagestory", "assets")

path_player = os.path.join(path_assets, "game", "entities", "humanoid", "player.json")

# load player entity json and collect animations
with open(path_player, "r") as f:
    player = json5.load(f)

animations = player["client"]["animations"]

# =============================================================================
# CREATE LIST OF ANIMATION NAMES
# =============================================================================
ANIMATIONS_DONE = \
"""
axechop,tenklop,Yes,
axechop-fp,tenklop,Copied from 3rd person -xeth,
axeheld,,,(Doesnt exist in vanilla lol -xeth)
axeheld-fp,,,(Doesnt exist in vanilla lol -xeth)
axeready,tenklop,Yes,(Based on axechop)
axeready-fp,tenklop,Copied from 3rd person -xeth,
bow,tenklop,Yes,
bowaim,tenklop,Yes,
bowhit,tenklop,Yes,
breakhand,tenklop,Yes,
breakhand-fp,tenklop,Copied from 3rd person -xeth,
breaktool,tenklop,Yes,Copied from smithing -xeth
breaktool-fp,tenklop,Yes,Copied from smithing -xeth
cheer,tenklop,Yes,
coldidle,tenklop,Yes,
coldidleheld,tenklop,,(Doesnt exist in vanilla lol -xeth)
cough,tenklop,Yes,
creativefly,xeth,Yes,
creativefly,xeth,Yes,
creativefly-fp,xeth,Copied from 3rd person -xeth,
crudeOarBackward,tenklop,Yes,
crudeOarForward,tenklop,Yes,
crudeOarHit,xeth,Yes,
crudeOarHit-fp,xeth,Copied from 3rd person -xeth,
crudeOarIdle,tenklop,Yes,
crudeOarIdle-fp,tenklop,Copied from 3rd person -xeth,
crudeOarReady,tenklop,Yes,
crudeOarStandingReady,tenklop,Yes?,Copied from crudeOarIdle -xeth
crudeOarStandingReady-fp,tenklop,Copied from 3rd person -xeth,
cry,tenklop,Yes,
die,tenklop,Yes,
eat,tenklop,Yes,
eat-fp,tenklop,Copied from 3rd person -xeth,
facepalm,tenklop,Yes,
falx,tenklop,Yes,
falx-fp,tenklop,Copied from 3rd person -xeth,
glide,tenklop,Yes,
glide-fp,tenklop,Copied from 3rd person -xeth,
hammerandchisel,tenklop,Yes,
hammerandchisel,tenklop,Yes,
hammerandchisel-ifp,tenklop,Copied from 3rd person -xeth,
headscratch,tenklop,Yes,
heavyimpact,tenklop,Yes,
helditemready,xeth,Yes,
helditemready-fp,xeth,Copied from 3rd person -xeth,
hit,tenklop,Yes,
hoe,tenklop,Yes,
hoe-fp,tenklop,Copied from 3rd person -xeth,
holdbothhands,tenklop,Yes,
holdbothhands-fp,tenklop,Copied from 3rd person -xeth,
holdbothhandslarge-fp,tenklop,Copied from 3rd person -xeth,
holdinglanternlefthand,xeth,Yes,
holdinglanternrighthand,xeth,Yes,
holdunderarm,tenklop,Yes,
hurt,tenklop,Yes,
idle1,xeth,Yes,
interactstatic,tenklop,Yes,
knap,tenklop,Yes,
knap-fp,tenklop,Copied from 3rd person -xeth,
knap-ifp,tenklop,Copied from 3rd person -xeth,
LadderIdle,tenklop,Yes,
ladderup,tenklop,Yes,
land-fp,xeth,Copied from 3rd person -xeth,
laugh,tenklop,Yes,
lie,tenklop,Yes,
newjump,xeth,Yes,
nod,tenklop,Yes,
panning,tenklop,Yes,
panning-fp,tenklop,Copied from 3rd person -xeth,
petlarge,tenklop,Yes,
petsmall,tenklop,Yes,
placeblock,tenklop,Yes,
pour,tenklop,Yes,
protecteyes,tenklop,Yes,
rage,tenklop,Yes,
scythe,tenklop,Yes,
scythe-fp,tenklop,Copied from 3rd person -xeth,
scytheIdle,,,
scytheIdle-fp,,,
scytheReady,,,
scytheReady-fp,,,
shears,tenklop,Yes,
shoveldig,tenklop,Yes,
shoveldig-fp,tenklop,Copied from 3rd person -xeth,
shrug,tenklop,Yes,
sitflooridle,xeth,Yes,
sitidle,tenklop,Yes,
slingaimbalearic,tenklop,Yes,
slingaimgreek,tenklop,Yes,
slingthrowbalearic,tenklop,Yes,
slingthrowgreek,tenklop,Yes,
smithing,tenklop,Yes,
smithing-fp,tenklop,Copied from 3rd person -xeth,
sneakidle,xeth,Yes,
sneakidle-fp,xeth,Copied from 3rd person -xeth,
sneakwalk,tenklop,Yes,
sneakwalk-fp,tenklop,Copied from 3rd person -xeth,
spearhit,tenklop,Yes,
spearhit-fp,tenklop,Copied from 3rd person -xeth,
sprint,xeth,Yes,
sprint-fp,xeth,Copied from 3rd person -xeth,
stretch,tenklop,Yes,
swim,tenklop,Yes,
swim-fp,tenklop,Copied from 3rd person -xeth,
swimidle,tenklop,Yes,
swordhit,tenklop,Yes,
throw,tenklop,Yes,
throwaim,tenklop,Yes,
twohandplaceblock,tenklop,Yes,
twohandplaceblock-fp,tenklop,Copied from 3rd person -xeth,
walk,xeth,Yes,
walk-fp,xeth,Copied from 3rd person -xeth,
water,tenklop,Yes,
wave,xeth,Yes,
yawn,tenklop,Yes,
"""
# load animations already done from text above
animations_done = {}
for line in ANIMATIONS_DONE.split("\n"):
    if line.strip() == "":
        continue
    chunks = line.split(",")
    animation_name = chunks[0]
    animator = chunks[1]
    done = chunks[2]
    animations_done[animation_name] = {
        "animator": animator.strip(),
        "done": done.strip().lower().startswith("yes"),
    }

# =============================================================================
# CREATE LIST OF ANIMATION NAMES
# =============================================================================
# table of animation name and entry to mark as done:
#   [walk, done?],
#   [idle1, done?],
#   ...
animation_names = []

for animation in animations:
    print(f"{animation['code']}: {animation['animation']}")
    anim_name = animation["animation"]
    if anim_name in animations_done:
        animator = animations_done[anim_name]["animator"]
        if animations_done[anim_name]["done"]:
            done = "Yes"
        else:
            done = ""
    else:
        animator = ""
        done = ""
    animation_names.append([animation["animation"], animator, done])

# sorted alphabetically
animation_names_sorted = sorted(animation_names, key=lambda x: x[0].lower())

# create markdown animations
animations_table = tabulate(
    animation_names_sorted,
    headers=["Animation Name", "Animator", "Done?"],
    tablefmt="github",
)

print(animations_table)

# save animations table to file
path_animations_table = os.path.join("scripts", "build", "animations.md")
with open(path_animations_table, "w+") as f:
    f.write(animations_table)

# write to csv
path_animations_csv = os.path.join("scripts", "build", "animations.csv")
with open(path_animations_csv, "w+", newline="") as f:
    writer = csv.writer(f)
    writer.writerow(["Animation Name", "Animator", "Done?"])
    writer.writerows(animation_names_sorted)

# =============================================================================
# CREATE CUSTOM ANIMATION BONE WEIGHT OVERRIDES
# =============================================================================

# vanilla "bone" names => new bone names
bone_remappings = {
    "UpperFootR": "FootUpperR",
    "UpperFootL": "FootUpperL",
    "LowerFootR": "FootLowerR",
    "LowerFootL": "FootLowerL",
    "UpperArmR": "ArmUpperR",
    "LowerArmR": "ArmLowerR",
    "UpperArmL": "ArmUpperL",
    "LowerArmL": "ArmLowerL",
}

animation_overrides = []
for animation in animations:
    element_weights = {}
    if "elementWeight" in animation:
        for element, weight in animation["elementWeight"].items():
            if element in bone_remappings:
                bone_name = bone_remappings[element]
            else:
                bone_name = element
            element_weights["b_" + bone_name] = weight
    
    if len(element_weights) > 0:
        animation_overrides.append({
            "code": animation["code"],
            "elementWeight": element_weights,
        })

# save animation overrides to file
path_animation_overrides = os.path.join("scripts", "build", "animation_overrides.json")
with open(path_animation_overrides, "w+") as f:
    json.dump(animation_overrides, f, indent=2)
