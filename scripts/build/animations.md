| Animation Name           | Animator   | Done?   |
|--------------------------|------------|---------|
| axechop                  | tenklop    | Yes     |
| axechop-fp               | tenklop    |         |
| axeheld                  |            |         |
| axeheld-fp               |            |         |
| axeready                 | tenklop    | Yes     |
| axeready-fp              | tenklop    |         |
| bow                      | tenklop    | Yes     |
| bowaim                   | tenklop    | Yes     |
| bowhit                   | tenklop    | Yes     |
| breakhand                | tenklop    | Yes     |
| breakhand-fp             | tenklop    |         |
| breaktool                | tenklop    | Yes     |
| breaktool-fp             | tenklop    | Yes     |
| cheer                    | tenklop    | Yes     |
| coldidle                 | tenklop    | Yes     |
| coldidleheld             | tenklop    |         |
| cough                    | tenklop    | Yes     |
| creativefly              | xeth       | Yes     |
| creativefly              | xeth       | Yes     |
| creativefly-fp           | xeth       |         |
| crudeOarBackward         | tenklop    | Yes     |
| crudeOarForward          | tenklop    | Yes     |
| crudeOarHit              | xeth       | Yes     |
| crudeOarHit-fp           | xeth       |         |
| crudeOarIdle             | tenklop    | Yes     |
| crudeOarIdle-fp          | tenklop    |         |
| crudeOarReady            | tenklop    | Yes     |
| crudeOarStandingReady    | tenklop    | Yes     |
| crudeOarStandingReady-fp | tenklop    |         |
| cry                      | tenklop    | Yes     |
| die                      | tenklop    | Yes     |
| eat                      | tenklop    | Yes     |
| eat-fp                   | tenklop    |         |
| facepalm                 | tenklop    | Yes     |
| falx                     | tenklop    | Yes     |
| falx-fp                  | tenklop    |         |
| glide                    | tenklop    | Yes     |
| glide-fp                 | tenklop    |         |
| hammerandchisel          | tenklop    | Yes     |
| hammerandchisel          | tenklop    | Yes     |
| hammerandchisel-ifp      | tenklop    |         |
| headscratch              | tenklop    | Yes     |
| heavyimpact              | tenklop    | Yes     |
| helditemready            | xeth       | Yes     |
| helditemready-fp         | xeth       |         |
| hit                      | tenklop    | Yes     |
| hoe                      | tenklop    | Yes     |
| hoe-fp                   | tenklop    |         |
| holdbothhands            | tenklop    | Yes     |
| holdbothhands-fp         | tenklop    |         |
| holdbothhandslarge-fp    | tenklop    |         |
| holdinglanternlefthand   | xeth       | Yes     |
| holdinglanternrighthand  | xeth       | Yes     |
| holdunderarm             | tenklop    | Yes     |
| hurt                     | tenklop    | Yes     |
| idle1                    | xeth       | Yes     |
| interactstatic           | tenklop    | Yes     |
| knap                     | tenklop    | Yes     |
| knap-fp                  | tenklop    |         |
| knap-ifp                 | tenklop    |         |
| LadderIdle               | tenklop    | Yes     |
| ladderup                 | tenklop    | Yes     |
| land-fp                  | xeth       |         |
| laugh                    | tenklop    | Yes     |
| lie                      | tenklop    | Yes     |
| newjump                  | xeth       | Yes     |
| nod                      | tenklop    | Yes     |
| panning                  | tenklop    | Yes     |
| panning-fp               | tenklop    |         |
| petlarge                 | tenklop    | Yes     |
| petsmall                 | tenklop    | Yes     |
| placeblock               | tenklop    | Yes     |
| pour                     | tenklop    | Yes     |
| protecteyes              | tenklop    | Yes     |
| rage                     | tenklop    | Yes     |
| scythe                   | tenklop    | Yes     |
| scythe-fp                | tenklop    |         |
| scytheIdle               |            |         |
| scytheIdle-fp            |            |         |
| scytheReady              |            |         |
| scytheReady-fp           |            |         |
| shears                   | tenklop    | Yes     |
| shoveldig                | tenklop    | Yes     |
| shoveldig-fp             | tenklop    |         |
| shrug                    | tenklop    | Yes     |
| sitflooridle             | xeth       | Yes     |
| sitidle                  | tenklop    | Yes     |
| slingaimbalearic         | tenklop    | Yes     |
| slingaimgreek            | tenklop    | Yes     |
| slingthrowbalearic       | tenklop    | Yes     |
| slingthrowgreek          | tenklop    | Yes     |
| smithing                 | tenklop    | Yes     |
| smithing-fp              | tenklop    |         |
| sneakidle                | xeth       | Yes     |
| sneakidle-fp             | xeth       |         |
| sneakwalk                | tenklop    | Yes     |
| sneakwalk-fp             | tenklop    |         |
| spearhit                 | tenklop    | Yes     |
| spearhit-fp              | tenklop    |         |
| sprint                   | xeth       | Yes     |
| sprint-fp                | xeth       |         |
| stretch                  | tenklop    | Yes     |
| swim                     | tenklop    | Yes     |
| swim-fp                  | tenklop    |         |
| swimidle                 | tenklop    | Yes     |
| swordhit                 | tenklop    | Yes     |
| throw                    | tenklop    | Yes     |
| throwaim                 | tenklop    | Yes     |
| twohandplaceblock        | tenklop    | Yes     |
| twohandplaceblock-fp     | tenklop    |         |
| walk                     | xeth       | Yes     |
| walk-fp                  | xeth       |         |
| water                    | tenklop    | Yes     |
| wave                     | xeth       | Yes     |
| yawn                     | tenklop    | Yes     |