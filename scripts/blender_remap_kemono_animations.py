"""
Script to help import and remap vanilla seraph animations into new kemono
armature format. Process done is:
1. Remove all animations from blender file.
2. Append all animations from seraph file.
3. Run this script inside blender in the file, it will generate new actions
    and remove old actions.

TODO: update this script to only run on a specified list of actions,
for future when we need to append animations that get added in future
updates to vanilla.
"""
import bpy

# Bone renaming to new convention.
KEMONO_FCURVE_DATA_PATH_MAPPING = {
    "LowerTorso": "TorsoLower",
    "UpperTorso": "TorsoUpper",
    "LowerArm": "ArmLower",
    "UpperArm": "ArmUpper",
    "LowerFoot": "FootLower",
    "UpperFoot": "FootUpper",
}

def axis_remapping(data_path: str, old_axis: int) -> (int, float):
    """Remap old armature axis to new armature axis.
    Previously, animations were done on mesh objects, which have
    different origins and rotation axes than the bones we are
    moving animations to.

    This has a hard-coded mapping from each old object name to new bone
    axis mapping.
    
    Proper way to do this is to calculate the transform between the old
    object and new bone, and apply that transform to the fcurve.
    This is really annoying, and hard-coded is close enough.
    """
    # default case
    axis = old_axis
    axis_scale = 1.0

    # data path format is "pose.bones["ArmLowerR"].location"
    bone_name = data_path.split("\"")[1]
    #print(f"bone: {bone_name}")

    if bone_name in {
        "ArmLowerR", "ArmUpperR", "ArmLowerL", "ArmUpperL",
        "FootLowerR", "FootUpperR", "FootLowerL", "FootUpperL",
    }:
        if old_axis == 0:
            axis_scale = -1.0 # mirror animation
            axis = 2
        elif old_axis == 1:
            axis_scale = 1.0
            axis = 0
        elif old_axis == 2:
            axis_scale = -1.0 # mirror animation
            axis = 1
        else:
            axis_scale = 1.0
            axis = old_axis
    elif bone_name in { # 90 deg rotation along x-axis
        "TorsoLower", "TorsoUpper", "Neck",
    }:
        if old_axis == 0:
            axis_scale = 1.0
            axis = 0
        elif old_axis == 1:
            axis_scale = -1.0
            axis = 2
        elif old_axis == 2:
            axis_scale = 1.0
            axis = 1
        else:
            axis_scale = 1.0
            axis = old_axis
    
    return axis, axis_scale

# for all actions, remove all fcurves
old_actions = bpy.data.actions
for old_action in old_actions:
    if old_action.name.endswith("_NEW"):
        continue
    
    new_action_name = old_action.name + "_NEW"
    if new_action_name in bpy.data.actions:
        new_action = bpy.data.actions[new_action_name]
    else:
        new_action = bpy.data.actions.new(new_action_name)
    
    # prevents deletion on file save
    new_action.use_fake_user = True

    # clean new action's fcurves and pose markers
    new_action_prev_fcurves = list(new_action.fcurves)
    for fc in new_action_prev_fcurves:
        new_action.fcurves.remove(fc)
    new_action_prev_markers = list(new_action.pose_markers)
    for marker in new_action_prev_markers:
        new_action.pose_markers.remove(marker)
    
    print(f"Updating fcurves from action: {old_action.name}")
    
    # copy pose markers
    for old_marker in old_action.pose_markers:
        new_marker = new_action.pose_markers.new(name=old_marker.name)
        new_marker.frame = old_marker.frame
    
    # print action groups
    #for group in old_action.groups:
    #   print(group)
    
    fcurves = list(old_action.fcurves)
    for old_fc in fcurves:
        group = old_fc.group
        data_path = old_fc.data_path
        old_axis = old_fc.array_index
        
        # do data path bone mapping
        for old_path, new_path in KEMONO_FCURVE_DATA_PATH_MAPPING.items():
            if old_path in data_path:
                data_path = data_path.replace(old_path, new_path)
                break
        
        # re-map axis and axis transform
        axis, axis_scale = axis_remapping(data_path, old_axis)
        
        # https://blender.stackexchange.com/questions/135759/is-it-possible-to-create-an-animation-in-blender-2-8-using-python-that-its-no
        new_fc = new_action.fcurves.new(data_path, index=axis)
        new_fc.keyframe_points.add(count=len(old_fc.keyframe_points))
        for old_kfp, new_kfp in zip(old_fc.keyframe_points, new_fc.keyframe_points):
            # keyframe_point.co = (frame_number, value)
            new_kfp.co = (old_kfp.co[0], old_kfp.co[1] * axis_scale)
            new_kfp.interpolation = "LINEAR"
        
        # assign fcurve group
        bone_name = data_path.split("\"")[1]
        if bone_name in new_action.groups:
            new_fc.group = new_action.groups[bone_name]
        else:
            new_group = new_action.groups.new(bone_name)
            new_fc.group = new_group

# DESTRUCTIVE PART:
# 1. remove animations without the "_NEW" tag
# 2. remove "_NEW" tag from those animations
all_actions = list(bpy.data.actions)
for action in all_actions:
    if not action.name.endswith("_NEW"):
        action.use_fake_user = False
        bpy.data.actions.remove(action)
all_actions = list(bpy.data.actions)
for action in all_actions:
    action.name = action.name.replace("_NEW", "")
