"""
Script that generates default horse hair models from game's default
seraph hair models. Copies all hair models from game files,
applies correct translation offsets to center hair on horse model,
outputs jsons to mod resources.
"""

import json
import os
import argparse
import numpy as np


parser = argparse.ArgumentParser(description="Generate horse hair models from seraph hair models.")

parser.add_argument(
    "--path",
    metavar="path",
    type=str,
    help="Path to game assets, e.g. C:\\Users\\X\\AppData\\Roaming\\Vintagestory\\assets"
)

parser.add_argument(
    "--path_out",
    metavar="path_out",
    type=str,
    help="Path to resources output"
)

args = parser.parse_args()

path_assets = args.path

if path_assets is None:
    # assume this is windows and use appdata
    path_appdata = os.getenv("APPDATA")
    path_assets = os.path.join(path_appdata, "Vintagestory", "assets")

if not os.path.exists(path_assets):
    print(f"ERROR: Path to game assets does not exist: {path_assets}")
    exit(1)

path_out = args.path_out
if path_out is None:
    path_out = os.path.join("resources", "assets", "kemono", "shapes", "entity", "horse", "skinparts", "hair")
    os.makedirs(path_out, exist_ok=True)

print(f"GAME FILES PATH: {path_assets}")
print(f"OUTPUT PATH: {path_out}")

path_hairparts = os.path.join(path_assets, "game", "shapes", "entity", "humanoid", "seraphskinparts", "hair-base")


hair_offset = np.array([-0.1, 0.38, 0.1]) + np.array([-3.0, 0.0, -3.0])
hair_scale = 1.16

for hairpart_file in os.listdir(path_hairparts):
    if not hairpart_file.endswith(".json"):
        continue

    print(f"{hairpart_file}")
    
    with open(os.path.join(path_hairparts, hairpart_file), "r") as f:
        hairpart = json.load(f)
    
    def process_elements(elements):
        """Recursively walks elements and modify."""
        for elem in elements:
            if "stepParentName" in elem:
                elem["stepParentName"] = "Hair" # set parent to bone hair

            # SCALE: apply uniform scale so hair fits pony head better
            for key in ["from", "to", "rotationOrigin"]:
                if key in elem:
                    # apply scale to fit head
                    new_pos = np.array(elem[key])
                    new_pos = new_pos * hair_scale

                    elem[key] = new_pos.tolist()
            
            if "children" in elem:
                process_elements(elem["children"])
    
    # scale elements
    process_elements(hairpart["elements"])

    # go through root elements, apply translation offsets to center hair
    for elem in hairpart["elements"]:
        for key in ["from", "to", "rotationOrigin"]:
            if key in elem:
                new_pos = np.array(elem[key])
                # translate
                new_pos = new_pos + hair_offset

                elem[key] = new_pos.tolist()
    
    # append "game" domain to hair texture
    new_texture_paths = {}
    for texture, texture_path in hairpart["textures"].items():
        new_texture_paths[texture] = f"game:{texture_path}"
    hairpart["textures"] = new_texture_paths

    # save to output
    with open(os.path.join(path_out, "hair-" + hairpart_file), "w") as f:
        json.dump(hairpart, f, indent=4)
