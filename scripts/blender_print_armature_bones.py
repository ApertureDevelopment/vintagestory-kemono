import bpy

# thanks chat gpt

armature_name = "Armature"  # Replace with the name of your armature

# Get the armature object by its name
armature = bpy.data.objects.get(armature_name)
if not armature or armature.type != 'ARMATURE':
    print("Armature not found or invalid.")
else:
    # Make sure the armature is active and in pose mode
    bpy.context.view_layer.objects.active = armature
    bpy.ops.object.mode_set(mode='POSE')

    # Print world positions of all bones
    for bone in armature.pose.bones:
        bone_name = bone.name
        bone_matrix = armature.matrix_world @ bone.matrix
        bone_world_position = bone_matrix.to_translation()
        
        # Convert blender space to VS space:
        # X -> Z
        # Y -> X
        # Z -> Y
        print(f"\"b_{bone_name}\": [{bone_world_position.y}, {bone_world_position.z}, {bone_world_position.x}],")

