# wishlist of api adjustments to make this mod easier

collection of api things that made me seethe & cope

## Make `EntityHeadController` an interface
https://github.com/anegostudios/vsapi/blob/e78624b6eee6920e45edcd25ba94e8199b2193af/Common/Model/Animation/EntityHeadController.cs

- **Problem**: I can't easily customize joint selection or controller logic
- **Solution**: Make this an interface with just `OnFrame`
- **Impact**: I can specify whatever joints needed and whatever `OnFrame`
    logic unique to my custom model.

## Add api method to clear texture atlas pixels to rgba = (0,0,0,0)
TODO