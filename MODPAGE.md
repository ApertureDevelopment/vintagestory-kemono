larp as anthro animals & pastel colored equines

man made horrors beyond your comprehension

alpha, would not recommend on real servr:
- expect breaking changes at any time x3
- still several crashes

looking 4 help :3, dm @xeth9000:
- blender animator (kemono model cleanup)
- blender modeling for more parts (kemono + horse)
- horse voices


status/todo:
| Model  | Animations | Parts     |
|--------|------------|-----------|
| Horse  |  ~90%      | Need more |
| Kemono |  cleanup   | Need more |


cute videos:  
https://files.catbox.moe/9y5nvc.mp4  
https://files.catbox.moe/3zy5dn.mp4  
https://files.catbox.moe/nfirb6.mp4  
https://files.catbox.moe/livb1h.mp4  
https://files.catbox.moe/uqr2ge.mp4  
https://files.catbox.moe/cna2q1.mp4  
https://files.catbox.moe/uvfr73.mp4  


credits:
- tenklop: horse animations
- xeth: code, horse + anthro models/rig, some anims
