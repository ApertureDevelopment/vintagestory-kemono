# Vintage Story Kemono Mod

man made horrors beyond your comprehension.

vintagestory kemono character mod.


# Dev stuff

This is setup for development from VS Code.

1. Download C#/dotnet/VS code plugins: <https://code.visualstudio.com/docs/languages/dotnet>

2. Launch run task in VS code (clr or mono)

3. Build actual release .zip using:  
`dotnet build -c release`


## Generating player skinnable part patch

Use python script from root directory to generate json patch for gui configs:
```
python scripts/patch_player.py
```

# Credits
tenklop - horse animations
xeth - code + models/rig