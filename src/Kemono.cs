﻿using ProtoBuf;
using Newtonsoft.Json;
using SkiaSharp;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using HarmonyLib;
using Vintagestory.API.Config;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Server;
using Vintagestory.API.Util;
using Vintagestory.GameContent;
using Vintagestory.API.MathTools;
using Vintagestory.API.Common.Entities;
using System;
using System.Reflection.Metadata.Ecma335;


[assembly: ModInfo("kemono",
    Description = "manmade horrors beyond your comprehension",
    Website     = "",
    Authors     = new []{ "xeth" })]
namespace kemono
{
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class KemonoCharacterSelectedState
    {
        public bool DidSelectSkin;
    }

    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class KemonoCharacterSelectionPacket
    {
        public bool DidSelectSkin;
        public string Model;
        public string Race;
        public string CharacterClass;
        public Dictionary<string, double> ModelScale;
        public Dictionary<string, string> SkinParts;
        public Dictionary<string, int> SkinColors;
        public int[] EmblemPixels;
        public string VoiceType;
        public string VoicePitch;
    }

    /// <summary>
    /// Kemono race trait system, similar to character classes:
    /// https://github.com/anegostudios/vssurvivalmod/blob/master/Systems/Character/Trait.cs
    /// </summary>
    public class KemonoRaceTrait
    {
        public string Code;
        public EnumTraitType Type;
        public Dictionary<string, double> Attributes;
    }

    /// <summary>
    /// Race system for kemono character.
    /// 
    /// Traits system similar to classes:
    /// https://github.com/anegostudios/vssurvivalmod/blob/master/Systems/Character/Trait.cs
    /// 
    /// But added complexity for race is skinpart requirements to
    /// qualify for a race selection, e.g. pegasus must have wings,
    /// earth pony cannot have wings. Race system supports boolean logic
    /// to specify required skinpart set.
    /// </summary>
    public class KemonoRace
    {
        public string Code;
        // optional icon visual
        public AssetLocation Icon;
        // list of KemonoRaceTrait code references given to character
        public string[] Traits;
        // player group privilege code required to choose race
        public string Privilege;
        // name of model required for race, null if any allowed
        public string Model;
        // map of skinpart type => set of required part names
        public Dictionary<string, HashSet<string>> RequiredParts;
        // map of skinpart type => set of banned part names
        public Dictionary<string, HashSet<string>> BannedParts;

        public static AssetLocation DefaultIcon = new AssetLocation("kemono:icon/icon-race-unknown.png");

        public static bool IsValid(KemonoRace race, IPlayer player, string model, IReadOnlyDictionary<string, string> parts)
        {
            // check if player privilege satisfied
            if (race.Privilege != null)
            {
                if (!player.HasPrivilege(Vintagestory.API.Server.Privilege.controlserver) && !player.HasPrivilege(race.Privilege)) return false;
            }

            // check if model satisfied
            if (race.Model != null && race.Model != model) return false;

            // check if skin parts satisfied
            if (parts != null)
            {
                foreach (var condition in race.RequiredParts)
                {
                    string partCode = condition.Key;
                    HashSet<string> required = condition.Value;
                    parts.TryGetValue(partCode, out string applied);
                    if (applied == null || !required.Contains(applied))
                    {
                        return false;
                    }
                }

                foreach (var condition in race.BannedParts)
                {
                    string partCode = condition.Key;
                    HashSet<string> banned = condition.Value;
                    parts.TryGetValue(partCode, out string applied);
                    if (applied != null && banned.Contains(applied))
                    {
                        return false;
                    }
                }
            }
            else
            {
                // no skin selected (should not normally happen)
                // only invalid race if any required part criteria exist
                return race.RequiredParts.Count == 0;
            }

            return true;
        }
    }

    public class KemonoMod : ModSystem
    {
        public static string PATH { get; } = "kemono";

        // server and client
        ICoreAPI api;

        // reference to original vs character system (manages classes, traits)
        public CharacterSystem baseCharSys;

        // server
        ICoreServerAPI sapi;
        IServerNetworkChannel serverToClientChannel;

        // client
        ICoreClientAPI capi;
        IClientNetworkChannel clientToServerChannel;
        
        GuiDialogCreateCharacterKemono createCharDlg;

        // path to save character and emblem image configs
        public string SaveDir { get; private set; }

        // default model for new characters
        public string DefaultModelCode = "kemono0";

        // default race code
        public string DefaultRaceCode = "unknown";

        // dict of available models by code
        public Dictionary<string, KemonoSkinnableModel> AvailableModels { get; private set; }
            = new Dictionary<string, KemonoSkinnableModel>();

        // flat list of available models, same objects as in AvailableModels
        public KemonoSkinnableModel[] AvailableModelList { get; private set; }
            = new KemonoSkinnableModel[0];
        
        // flat list of available model codes
        public string[] AvailableModelCodes { get; private set; }
            = new string[0];
        
        // dict mapping available model code to index in available models list
        public Dictionary<string, int> AvailableModelIndex { get; private set; }
            = new Dictionary<string, int>();
        
        // list of available skin presets by code (must be unique)
        public Dictionary<string, KemonoSkinnableModelPreset> SkinPresetsByCode { get; private set; }
            = new Dictionary<string, KemonoSkinnableModelPreset>();
        
        // flat list of available skin presets, same objects as in SkinPresetsByCode
        public KemonoSkinnableModelPreset[] SkinPresetsList { get; private set; }
            = new KemonoSkinnableModelPreset[0];
        
        // flat list of available skin preset code names
        public string[] SkinPresetCodes { get; private set; }
            = new string[0];
        
        // flat list of available races
        public List<KemonoRace> Races { get; private set; }
            = new List<KemonoRace>();
        
        // dict of available races by code
        public Dictionary<string, KemonoRace> RacesByCode { get; private set; }
            = new Dictionary<string, KemonoRace>();
        
        // race trait modifiers by code
        public Dictionary<string, Trait> RaceTraitsByCode = new Dictionary<string, Trait>();

        // store local player state whether player has finished character selection
        bool didSelectSkin = false;

        public override void StartPre(ICoreAPI api)
        {
            // create custom asset categories to store mod's models and presets
            // so we can share this data across all entities
            AssetCategory.categories["models"] = new AssetCategory("models", true, EnumAppSide.Universal);
            AssetCategory.categories["modelpresets"] = new AssetCategory("modelpresets", true, EnumAppSide.Universal);
            AssetCategory.categories["race"] = new AssetCategory("race", true, EnumAppSide.Universal);
            
            // harmony patching
            // Harmony.DEBUG = true;
            var harmony = new Harmony("kemono");
            harmony.PatchAll();
        }

        public override void Start(ICoreAPI api)
        {
            this.api = api;

            // reference to base character system
            baseCharSys = api.ModLoader.GetModSystem<CharacterSystem>();

            api.RegisterEntityBehaviorClass("kemonoskinnable", typeof(EntityBehaviorKemonoSkinnable));
        }

        public override void StartClientSide(ICoreClientAPI api)
        {
            this.capi = api;
            
            api.RegisterEntityRendererClass("KemonoSkinnableShape", typeof(EntityKemonoSkinnableShapeRenderer));
            api.RegisterEntityRendererClass("KemonoPlayerShape", typeof(EntityKemonoPlayerShapeRenderer));

            clientToServerChannel =
                api.Network.RegisterChannel("kemono")
                .RegisterMessageType<KemonoCharacterSelectionPacket>()
                .RegisterMessageType<KemonoCharacterSelectedState>()
                .SetMessageHandler<KemonoCharacterSelectedState>(ClientOnSelectedState)
            ;

            var parsers = api.ChatCommands.Parsers;

            api.ChatCommands
                .Create("kemonocharsel") // extra alias for charsel
                .RequiresPrivilege("")
                .HandleWith(OnCharSelCmd)
            ;

            api.ChatCommands
                .Create("skinsave")
                .RequiresPrivilege("")
                .WithArgs(parsers.Word("filename"))
                .HandleWith(OnCmdSkinSave)
            ;

            api.ChatCommands
                .Create("skinload")
                .RequiresPrivilege("")
                .WithArgs(parsers.Word("filename"))
                .HandleWith(OnCmdSkinLoad)
            ;

            api.ChatCommands
                .Create("skinreload")
                .RequiresPrivilege("")
                .WithArgs(parsers.OptionalWord("reloadtype0"), parsers.OptionalWord("reloadtype1"), parsers.OptionalWord("reloadtype2"))
                .HandleWith(OnCmdSkinReload)
            ;

            api.ChatCommands
                .Create("emblemsave")
                .RequiresPrivilege("")
                .WithArgs(parsers.Word("filename"))
                .HandleWith(OnCmdEmblemSave)
            ;

            api.ChatCommands
                .Create("emblemload")
                .RequiresPrivilege("")
                .WithArgs(parsers.Word("filename"))
                .HandleWith(OnCmdEmblemLoad)
            ;

            api.ChatCommands
                .Create("emblemtexload")
                .RequiresPrivilege("")
                .WithArgs(parsers.Word("filename"))
                .HandleWith(OnCmdEmblemTexLoad)
            ;

            api.ChatCommands
                .Create("kdebugtextures")
                .RequiresPrivilege("")
                .HandleWith(OnDebugTexturesCmd)
            ;

            api.Event.IsPlayerReady += EventIsPlayerReadyClient; // idk why but we need this
            api.Event.PlayerJoin += EventPlayerJoinClient;
        }

        public override void StartServerSide(ICoreServerAPI api)
        {
            this.sapi = api;

            serverToClientChannel =
                api.Network.RegisterChannel("kemono")
                .RegisterMessageType<KemonoCharacterSelectionPacket>()
                .RegisterMessageType<KemonoCharacterSelectedState>()
                .SetMessageHandler<KemonoCharacterSelectionPacket>(ServerOnCharacterSelection)
            ;

            var parsers = api.ChatCommands.Parsers;

            api.ChatCommands
                .Create("racechangeonce")
                .RequiresPrivilege("commandplayer")
                .WithArgs(parsers.OnlinePlayer("playername"))
                .HandleWith(OnRaceChangeOnceCmd)
            ;

            api.Event.PlayerJoin += EventPlayerJoinServer;
        }

        /// <summary>
        /// Load model and skin presets. These are stored in the mod's assets
        /// folders in custom directories "models" and "modelpresets".
        /// </summary>
        /// <param name="api"></param>
        public void ReloadModels(ICoreAPI api, bool reloadAssets = true)
        {
            if (reloadAssets)
            {
                api.Assets.Reload(AssetCategory.categories["models"]);
            }

            // load model assets into real models and addons
            // note: loading models by code first so models override
            // each other to prevent duplicate model codes
            Dictionary<string, KemonoSkinnableModel> modelsToInit = new Dictionary<string, KemonoSkinnableModel>();
            List<KemonoSkinnableModel> addonModels = new List<KemonoSkinnableModel>();
            List<AssetLocation> modelPaths = api.Assets.GetLocations("models/");
            foreach (var modelPath in modelPaths)
            {
                var modelAsset = api.Assets.TryGet(modelPath);
                var modelLoaded = modelAsset.ToObject<KemonoSkinnableModel>();
                if (modelLoaded.Addon != null)
                {
                    addonModels.Add(modelLoaded);
                }
                else
                {
                    if (modelsToInit.ContainsKey(modelLoaded.Code))
                    {
                        api.Logger.Warning($"[kemono] {modelPath} Overwriting model {modelLoaded.Code}");
                    }
                    modelsToInit[modelLoaded.Code] = modelLoaded;
                    api.Logger.Notification($"[kemono] Loaded model: {modelLoaded.Code}");
                }
            }

            // apply addon models
            foreach (var addon in addonModels)
            {
                if (modelsToInit.TryGetValue(addon.Addon, out KemonoSkinnableModel model))
                {
                    model.ApplyAddon(addon);
                    api.Logger.Notification($"[kemono] Loaded addon: {addon.Addon}");
                }
                else
                {
                    api.Logger.Warning($"[kemono] Addon for model not found: {addon.Addon}");
                }
            }

            // finish initializing models
            foreach (var model in modelsToInit)
            {
                AvailableModels[model.Value.Code] = model.Value.Initialize();
            }

            // create final flat list of available models and their names
            AvailableModelList = AvailableModels.Values.ToArray();
            AvailableModelCodes = AvailableModels.Keys.ToArray();
            AvailableModelIndex = new Dictionary<string, int>();
            for (int i = 0; i < AvailableModelCodes.Length; i++)
            {
                AvailableModelIndex[AvailableModelCodes[i]] = i;
            }

            // load model preset configs
            List<AssetLocation> presetPaths = api.Assets.GetLocations("modelpresets/");
            foreach (var presetPath in presetPaths)
            {
                var presetAsset = api.Assets.TryGet(presetPath);
                // get preset code from filename
                string presetCode = presetPath.Path.Split('/').Last().Split('.').First();
                var presetLoaded = presetAsset.ToObject<KemonoSkinnableModelPreset>();
                SkinPresetsByCode[presetCode] = presetLoaded;
            }

            // create flat lists of available skin presets and their names,
            // but sort codes by 1. model type and then 2. alphabetically by code in each type:
            // first sort presets into their model types
            Dictionary<string, List<string>> presetCodesByModel = new Dictionary<string, List<string>>();
            List<string> presetModelTypes = new List<string>();
            foreach (var preset in SkinPresetsByCode)
            {
                var presetCode = preset.Key;
                var presetVal = preset.Value;
                if (!presetCodesByModel.ContainsKey(presetVal.Model))
                {
                    presetCodesByModel[presetVal.Model] = new List<string>();
                    presetModelTypes.Add(presetVal.Model);
                }
                presetCodesByModel[presetVal.Model].Add(presetCode);
            }

            // sort preset codes in each model type
            foreach (var modelType in presetModelTypes)
            {
                presetCodesByModel[modelType].Sort();
            }

            // sort model type names
            presetModelTypes.Sort();

            // flatten sorted preset codes into a single list
            List<string> presetCodes = new List<string>();
            List<KemonoSkinnableModelPreset> presetList = new List<KemonoSkinnableModelPreset>();
            foreach (var modelType in presetModelTypes)
            {
                foreach (var presetCode in presetCodesByModel[modelType])
                {
                    presetCodes.Add(presetCode);
                    presetList.Add(SkinPresetsByCode[presetCode]);
                }
            }

            SkinPresetsList = presetList.ToArray();
            SkinPresetCodes = presetCodes.ToArray();
        }

        /// After assets loaded, load models and skin presets
        /// (treating models like assets, load during AssetsLoaded phase).
        public override void AssetsLoaded(ICoreAPI api)
        {
            base.AssetsLoaded(api);
            Debug.WriteLine($"[kemono] DataPathMods={GamePaths.DataPathMods}");
            Debug.WriteLine($"[kemono] DataPathServerMods={GamePaths.DataPathServerMods}");
            Debug.WriteLine($"[kemono] AssetsPath={GamePaths.AssetsPath}");

            // load models and model presets
            ReloadModels(api, false);

            // create user local save directory
            // (for user to save character + emblem configs)
            SaveDir = GamePaths.DataPathMods + Path.DirectorySeparatorChar + "kemono"; 
            if (!Directory.Exists(SaveDir))
            {
                api.Logger.Notification($"[Kemono] Creating save directory: {SaveDir}");
                Directory.CreateDirectory(SaveDir);
            }

            // load races and race traits
            List<AssetLocation> racePaths = api.Assets.GetLocations("race/");
            foreach (var racePath in racePaths)
            {
                // if path ends with trait.json or traits.json, load as trait
                if (racePath.Path.EndsWith("trait.json") || racePath.Path.EndsWith("traits.json"))
                {
                    var traits = api.Assets.Get(racePath).ToObject<List<Trait>>();
                    foreach (var trait in traits)
                    {
                        if (RaceTraitsByCode.ContainsKey(trait.Code))
                        {
                            api.Logger.Notification($"[kemono] {racePath} Overwriting trait {trait.Code}");
                        }
                        RaceTraitsByCode[trait.Code] = trait;
                    }
                }
                else // load race object definitions
                {
                    var raceAsset = api.Assets.TryGet(racePath);
                    var raceLoaded = raceAsset.ToObject<List<KemonoRace>>();
                    foreach (var race in raceLoaded)
                    {
                        AddRace(race);
                        api.Logger.Notification($"[kemono] Loaded race: {race.Code}");
                    }

                }
            }
        }

        public TextCommandResult OnCharSelCmd(TextCommandCallingArgs args)
        {
            if (createCharDlg == null)
            {
                createCharDlg = new GuiDialogCreateCharacterKemono(capi, this);
                createCharDlg.PrepAndOpen();
            }

            if (!createCharDlg.IsOpened())
            {
                createCharDlg.TryOpen();
            }

            return TextCommandResult.Success();
        }

        /// <summary>
        /// Runs on client when local player or another player joins game.
        /// </summary>
        /// <param name="byPlayer"></param>
        public void EventPlayerJoinClient(IClientPlayer byPlayer)
        {
            // refresh race traits
            if (byPlayer.Entity != null) // new player entity is null on server?
            {
                ApplyRaceTraits(byPlayer.Entity);
            }

            // local player, decide whether to open character selection dialog
            if (byPlayer.PlayerUID == capi.World.Player.PlayerUID)
            {
                // conditions to open dialog
                // - player has not selected skin yet
                // - player race is default or non-existent and there are more
                //   than 1 race. check is required to make mod robust to player
                //   adding new race mods or race mod codes changing
                string raceCode = byPlayer.Entity?.WatchedAttributes.GetString("characterRace");
                bool shouldSelectCharacter = !didSelectSkin ||
                    (Races.Count > 1 && (raceCode == null || raceCode == DefaultRaceCode || (raceCode != null && !RacesByCode.ContainsKey(raceCode))))
                ;

                // note: when debugging gui, disable this check
                if (shouldSelectCharacter)
                {
                    createCharDlg = new GuiDialogCreateCharacterKemono(capi, this);
                    createCharDlg.PrepAndOpen();
                    createCharDlg.OnClosed += () => capi.PauseGame(false);
                    capi.Event.EnqueueMainThreadTask(() => capi.PauseGame(true), "pausegame");
                }
            }
        }

        /// <summary>
        /// idk why but we need this:
        /// https://github.com/anegostudios/vssurvivalmod/blob/master/Systems/Character/Character.cs#L330
        /// </summary>
        private bool EventIsPlayerReadyClient(ref EnumHandling handling)
        {
            if (didSelectSkin) return true;

            handling = EnumHandling.PreventDefault;
            return false;
        }

        /// <summary>
        /// Runs on server when player joins game. Checks if player has 
        /// already selected skin and sends back to client.
        /// </summary>
        /// <param name="byPlayer"></param>
        public void EventPlayerJoinServer(IServerPlayer byPlayer)
        {
            bool playerDidSelectSkin = SerializerUtil.Deserialize(byPlayer.GetModdata("createCharacter"), false);
            
            if (byPlayer.Entity != null) // new player entity is null on server?
            {
                // refresh race traits
                ApplyRaceTraits(byPlayer.Entity);

                if (!playerDidSelectSkin)
                {
                    baseCharSys.setCharacterClass(byPlayer.Entity, baseCharSys.characterClasses[0].Code, false);
                }
            }

            serverToClientChannel.SendPacket(new KemonoCharacterSelectedState() { DidSelectSkin = playerDidSelectSkin }, byPlayer);
        }

        public void ServerTryApplySkin(IServerPlayer fromPlayer, KemonoCharacterSelectionPacket p)
        {
            // check if race is valid for skin parts, otherwise reject skin change
            bool didSelectBefore = SerializerUtil.Deserialize(fromPlayer.GetModdata("createCharacter"), false);
            bool didSelectRace = SerializerUtil.Deserialize(fromPlayer.GetModdata("didSelectRace"), false);

            if (!RacesByCode.TryGetValue(p.Race, out KemonoRace race))
            {
                fromPlayer.SendIngameError("racedoesnotexist", Lang.Get("kemono:msg-error-race-not-exist"));
                return;
            }
            if (!KemonoRace.IsValid(race, fromPlayer, p.Model, p.SkinParts))
            {
                fromPlayer.SendIngameError("raceskininvalid", Lang.Get("kemono:msg-error-skin-race-invalid"));
                return;
            }

            // check if race changed, only allow skin change if:
            // - has not selected race
            // - current race is default or null
            // - in creative mode
            if (didSelectRace && fromPlayer.WorldData.CurrentGameMode != EnumGameMode.Creative)
            {
                var currRace = GetEntityRace(fromPlayer.Entity);
                if (currRace != null && currRace.Code != DefaultRaceCode && p.Race != currRace.Code)
                {
                    fromPlayer.SendIngameError("racechangenotallowed", Lang.Get("kemono:msg-error-race-change-not-allowed"));
                    return;
                }
            }

            // do race selection
            fromPlayer.SetModdata("didSelectRace", SerializerUtil.Serialize(true));
            SetEntityRace(fromPlayer.Entity, race);

            // class selection: allow first time or in creative mode
            // if not allowed, continue with skin change but don't change class
            fromPlayer.SetModdata("createCharacter", SerializerUtil.Serialize(p.DidSelectSkin));

            if (!didSelectBefore || fromPlayer.WorldData.CurrentGameMode == EnumGameMode.Creative)
            {
                bool initializeGear = !didSelectBefore || fromPlayer.WorldData.CurrentGameMode == EnumGameMode.Creative;
                baseCharSys.setCharacterClass(fromPlayer.Entity, p.CharacterClass, initializeGear);
            }
            else
            {
                string currClass = fromPlayer.Entity.WatchedAttributes.GetString("characterClass");
                if (currClass != p.CharacterClass)
                {
                    fromPlayer.SendIngameError(Lang.Get("kemono:msg-error-class-change-not-allowed"));
                }
            }

            EntityBehaviorKemonoSkinnable bh = fromPlayer.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();

            if (bh != null)
            {
                // set model
                bh.SetModel(p.Model);

                // voice and pitch
                bh.ApplyVoice(p.VoiceType, p.VoicePitch, false);

                // apply model scale
                // if null, assume no scales (e.g. scales were cleared)
                // and clear any existing scales
                if (p.ModelScale != null)
                {
                    foreach (var scale in p.ModelScale)
                    {
                        bh.SetModelPartScale(scale.Key, scale.Value, false);
                    }
                }
                else
                {
                    var appliedScale = bh.GetAppliedScale();
                    if (appliedScale != null)
                    {
                        foreach (var scalePart in bh.Model.ScaleParts)
                        {
                            appliedScale.RemoveAttribute(scalePart.Code);
                        }
                    }
                }

                // apply skin part variants
                foreach (var skinpart in p.SkinParts)
                {
                    bh.SelectSkinPart(skinpart.Key, skinpart.Value, false);
                }

                // apply skin part colors
                foreach (var skincolor in p.SkinColors)
                {
                    bh.SetSkinPartColor(skincolor.Key, skincolor.Value, false);
                }

                // apply emblem texture
                if (p.EmblemPixels != null)
                {
                    bh.SetEmblemPixels(p.EmblemPixels);
                }
            }
            else
            {
                api.Logger.Error($"[kemono] Behavior EntityBehaviorKemonoSkinnable not found for {fromPlayer}!");
            }
        }

        public void ServerOnCharacterSelection(IServerPlayer fromPlayer, KemonoCharacterSelectionPacket p)
        {
            if (p.DidSelectSkin)
            {
                ServerTryApplySkin(fromPlayer, p);
            }

            fromPlayer.Entity.WatchedAttributes.MarkPathDirty("characterRace");
            fromPlayer.Entity.WatchedAttributes.MarkPathDirty("skinConfig");
            fromPlayer.BroadcastPlayerData(true);
        }


        /// <summary>
        /// Called when client finishes local character selection screen.
        /// 1. Sends client skin config to server.
        /// 2. Server validates skin, race, class
        /// 3. Server accepts or rejects change, then broadcasts updated
        ///    player skin config back to client (and other clients)
        /// </summary>
        /// <param name="characterRace"></param>
        /// <param name="characterClass"></param>
        /// <param name="didSelect"></param>
        public void ClientSelectionDone(string modelCode, string characterRace, string characterClass, bool didSelect)
        {
            Dictionary<string, double> modelScale = new Dictionary<string, double>();
            Dictionary<string, string> skinParts = new Dictionary<string, string>();
            Dictionary<string, int> skinColors = new Dictionary<string, int>();

            var bh = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();

            var appliedScale = bh.GetAppliedScale();
            if (appliedScale != null)
            {
                foreach (var scalePart in bh.Model.ScaleParts)
                {
                    double? scale = appliedScale.TryGetDouble(scalePart.Code);
                    if (scale != null)
                    {
                        modelScale[scalePart.Code] = (double) scale;
                    }
                }
            }

            var appliedParts = bh.AppliedSkinParts;
            foreach (var applied in appliedParts)
            {
                skinParts[applied.PartCode] = applied.Code;
                skinColors[applied.PartCode] = applied.Color;
            }

            // emblem texture pixels
            int[] emblemPixels = bh.GetEmblemPixels();

            // client-side validate race is valid before sending
            
            string chosenRace = DefaultRaceCode;
            if (RacesByCode.TryGetValue(characterRace, out KemonoRace race))
            {
                if (KemonoRace.IsValid(race, capi.World.Player, modelCode, skinParts))
                {
                    chosenRace = characterRace;
                }
            }

            capi.Network.GetChannel("kemono").SendPacket(new KemonoCharacterSelectionPacket()
            {
                DidSelectSkin = didSelect,
                Model = modelCode,
                Race = chosenRace,
                CharacterClass = characterClass,
                ModelScale = modelScale,
                SkinParts = skinParts,
                SkinColors = skinColors,
                EmblemPixels = emblemPixels,
                VoicePitch = bh.VoicePitch,
                VoiceType = bh.VoiceType
            });

            capi.Network.SendPlayerNowReady();

            createCharDlg = null;
        }

        /// Client handler for server packet whether player has already 
        /// done character selection. Called when player joins server.
        public void ClientOnSelectedState(KemonoCharacterSelectedState p)
        {
            didSelectSkin = p.DidSelectSkin;
        }

        public static void AllowRaceChange(IServerPlayer player)
        {
            player.SetModdata("didSelectRace", SerializerUtil.Serialize(false));
        }

        /// Common helper to save client player's skin config to local
        /// json file. Return true if successfully saved.
        public bool SkinSave(ICoreClientAPI capi, string filename)
        {
            if (filename == "")
            {
                capi.ShowChatMessage(Lang.Get("kemono:msg-skin-save-error-no-file"));
                return false;
            }

            // make sure filename ends in json
            string filenameJson = filename;
            if (!filenameJson.EndsWith(".json"))
            {
                filenameJson = filenameJson + ".json";
            }

            string savePath = SaveDir + Path.DirectorySeparatorChar + filenameJson;
            capi.ShowChatMessage(Lang.Get("kemono:msg-skin-save-file", filenameJson));

            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            var preset = skin.SavePreset();

            // serialize to json
            using (StreamWriter file = File.CreateText(savePath))
            {
                // make serializer with camelcase and 2 space indents
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
                    Formatting = Formatting.Indented
                };
                JsonSerializer serializer = JsonSerializer.Create(settings);
                serializer.Serialize(file, preset);
            }

            return true;
        }

        /// <summary>
        /// Add race to available races. If race by code already exists, this
        /// will overwrite that race, but keep the ordering in the Races list.
        /// This is because the race ordering may matter, but we still need
        /// different mods to be able to override races based on load order
        /// so mods can customize other race mods.
        /// </summary>
        /// <param name="race"></param>
        public void AddRace(KemonoRace race)
        {
            if (RacesByCode.ContainsKey(race.Code))
            {
                RacesByCode[race.Code] = race;
                for (int i = 0; i < Races.Count; i++)
                {
                    if (Races[i].Code == race.Code)
                    {
                        Races[i] = race;
                        break;
                    }
                }
            }
            else
            {
                // if race code is default, make sure it is at index 0
                if (race.Code == DefaultRaceCode)
                {
                    Races.Insert(0, race);
                }
                else
                {
                    Races.Add(race);
                }
                RacesByCode[race.Code] = race;
            }
        }

        /// <summary>
        /// Remove race with given code name string if it exists. Code is case
        /// sensitive. Returns true if race removed exists.
        /// </summary>
        /// <param name="code"></param>
        public bool RemoveRace(string code)
        {
            // find race by code in Races list and remove
            // then update races by filtering existing races list by code
            // (this preserves race list ordering)
            if (RacesByCode.Remove(code))
            {
                for (int i = 0; i < Races.Count; i++)
                {
                    if (Races[i].Code == code)
                    {
                        Races.RemoveAt(i);
                        break;
                    }
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Get race for entity, return null if race not found.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public KemonoRace GetEntityRace(Entity entity)
        {
            string raceCode = entity.WatchedAttributes.GetString("characterRace");
            if (raceCode != null && RacesByCode.TryGetValue(raceCode, out KemonoRace race))
            {
                return race;
            }
            return null;
        }

        /// <summary>
        /// Apply entity race selection by code. Throws error if race does
        /// not exist. If race does not exist, sets the 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="raceCode"></param>
        public void SetEntityRaceFromCode(Entity entity, string raceCode, bool applyTraits = true)
        {
            if (RacesByCode.TryGetValue(raceCode, out KemonoRace race))
            {
                SetEntityRace(entity, race, applyTraits);
            }
            else
            {
                // set race to default built-in race "unknown"
                api.Logger.Error($"[kemono] Invalid race during SetEntityRace: {raceCode}");
                if (RacesByCode.TryGetValue(DefaultRaceCode, out race))
                {
                    SetEntityRace(entity, race, applyTraits);
                }
            }
        }

        /// <summary>
        /// Apply race to entity. 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="race"></param>
        public void SetEntityRace(Entity entity, KemonoRace race, bool applyTraits = true)
        {
            entity.WatchedAttributes.SetString("characterRace", race.Code);
            entity.WatchedAttributes.MarkPathDirty("characterRace");
            if (applyTraits) ApplyRaceTraits(entity);
        }


        /// <summary>
        /// Apply race trait attributes, based on vanilla character class:
        /// https://github.com/anegostudios/vssurvivalmod/blob/master/Systems/Character/Character.cs#L257
        /// </summary>
        /// <param name="entity"></param>
        /// <exception cref="ArgumentException"></exception>
        public void ApplyRaceTraits(Entity entity)
        {
            // reset race traits 
            foreach (var stats in entity.Stats)
            {
                foreach (var statmod in stats.Value.ValuesByKey)
                {
                    if (statmod.Key == "race")
                    {
                        stats.Value.Remove(statmod.Key);
                        break;
                    }
                }
            }

            string raceCode = entity.WatchedAttributes.GetString("characterRace", DefaultRaceCode);
            if (!RacesByCode.TryGetValue(raceCode, out KemonoRace race))
            {
                // log error invalid race and return
                api.Logger.Error($"[kemono] Invalid race during ApplyRaceTraits: {raceCode}");
                return;
            }

            // apply traits
            string[] extraTraits = entity.WatchedAttributes.GetStringArray("extraTraits");
            IEnumerable<string> allTraits;
            if (race.Traits != null && extraTraits != null)
            {
                allTraits = race.Traits.Concat(extraTraits);
            }
            else if (race.Traits != null)
            {
                allTraits = race.Traits;
            }
            else if (extraTraits != null)
            {
                allTraits = extraTraits;
            }
            else // both are null
            {
                return;
            }

            foreach (var traitcode in allTraits)
            {
                if (RaceTraitsByCode.TryGetValue(traitcode, out Trait trait))
                {
                    foreach (var val in trait.Attributes)
                    {
                        string attrcode = val.Key;
                        double attrvalue = val.Value;

                        entity.Stats.Set(attrcode, "race", (float)attrvalue, true);
                    }
                }
            }

            entity.GetBehavior<EntityBehaviorHealth>()?.MarkDirty();
        }

        public bool HasRaceTrait(Entity entity, string trait)
        {
            string raceCode = entity.WatchedAttributes.GetString("characterRace");
            if (raceCode != null && RacesByCode.TryGetValue(raceCode, out KemonoRace race))
            {
                if (race.Traits != null) return race.Traits.Contains(trait);
            }
            return false;
        }

        public bool HasAnyRaceTrait(Entity entity, params string[] traits)
        {
            string raceCode = entity.WatchedAttributes.GetString("characterRace");
            if (raceCode != null && RacesByCode.TryGetValue(raceCode, out KemonoRace race))
            {
                if (race.Traits != null)
                {
                    foreach (var trait in traits)
                    {
                        if (race.Traits.Contains(trait)) return true;
                    }
                }
            }
            return false;
        }

        /// Common helper to load client player's skin config from
        /// local json files.
        public bool SkinLoad(ICoreClientAPI capi, string filename)
        {
            if (filename == "")
            {
                capi.ShowChatMessage(Lang.Get("kemono:msg-skin-load-error-no-file"));
                return false;
            }

            // make sure filename ends in json
            string filenameJson = filename;
            if (!filenameJson.EndsWith(".json"))
            {
                filenameJson = filenameJson + ".json";
            }

            string loadPath = SaveDir + Path.DirectorySeparatorChar + filenameJson;

            // check if exists
            if (!File.Exists(loadPath))
            {
                capi.ShowChatMessage(Lang.Get("kemono:msg-skin-load-error-no-file-exist", filenameJson));
                return false;
            }

            capi.ShowChatMessage(Lang.Get("kemono:msg-skin-load-file", filenameJson));

            string jsonString = File.ReadAllText(loadPath);
            var preset = JsonConvert.DeserializeObject<KemonoSkinnableModelPreset>(jsonString);
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            skin.LoadPreset(preset);

            return true;
        }

        /// Common helper to save client player's emblem texture to local
        /// png file. Return true if successfully saved.
        /// TODO: lang file strings
        public bool EmblemSave(ICoreClientAPI capi, string filename)
        {
            if (filename == "")
            {
                capi.ShowChatMessage(Lang.Get("kemono:msg-emblem-save-error-no-file"));
                return false;
            }

            // make sure filename ends in png
            string filenameImg = filename;
            if (!filenameImg.EndsWith(".png"))
            {
                filenameImg = filenameImg + ".png";
            }

            string imgPath = SaveDir + Path.DirectorySeparatorChar + filenameImg;
            capi.ShowChatMessage(Lang.Get("kemono:msg-emblem-save-file", filenameImg));

            // serialize to png
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            var bmp = skin.GetEmblemBitmap();
            using Stream fileStream = File.OpenWrite(imgPath);
            bmp.Encode(SKEncodedImageFormat.Png, 100).SaveTo(fileStream);

            return true;
        }

        /// Common helper to load client player's skin config from
        /// local json files.
        /// TODO: lang file strings
        public bool EmblemLoad(ICoreClientAPI capi, string filename)
        {
            if (filename == "")
            {
                capi.ShowChatMessage(Lang.Get("kemono:msg-emblem-load-error-no-file"));
                return false;
            }
            
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            if (skin == null)
            {
                capi.ShowChatMessage(Lang.Get("kemono:msg-error-player-not-kemono"));
                return false;
            }

            // make sure filename ends in json
            string filenameImg = filename;
            if (!filenameImg.EndsWith(".png"))
            {
                filenameImg = filenameImg + ".png";
            }

            string imgPath = SaveDir + Path.DirectorySeparatorChar + filenameImg;
            
            var result = skin.SetEmblemFromImagePath(imgPath);
            if (result.IsOk)
            {
                capi.ShowChatMessage(Lang.Get("kemono:msg-emblem-load-file", filenameImg));
                return true;
            }
            else // is error
            {
                capi.ShowChatMessage(result.Error);
                return false;
            }
        }

        /// Client command to save current skin config to json file.
        public TextCommandResult OnCmdSkinSave(TextCommandCallingArgs args)
        {
            string filename = args.Parsers[0].GetValue() as string;
            bool result = SkinSave(capi, filename);
            if (result == true)
            {
                return TextCommandResult.Success();
            }
            else
            {
                return TextCommandResult.Error(Lang.Get("kemono:msg-cmd-skin-save-error", filename));
            }
        }

        /// Client command to load skin config from json file.
        public TextCommandResult OnCmdSkinLoad(TextCommandCallingArgs args)
        {
            string filename = args.Parsers[0].GetValue() as string;
            bool result = SkinLoad(capi, filename);
            if (result == true)
            {
                // send new skin selection to server
                var entity = capi.World.Player.Entity;
                string modelCode = entity.WatchedAttributes.GetTreeAttribute("skinConfig")?.GetString(EntityBehaviorKemonoSkinnable.APPLIED_MODEL, DefaultModelCode);
                string raceCode = capi.World.Player.Entity.WatchedAttributes.GetString("characterRace", DefaultRaceCode);
                string classCode = capi.World.Player.Entity.WatchedAttributes.GetString("characterClass", "");
                ClientSelectionDone(modelCode, raceCode, classCode, true);

                return TextCommandResult.Success();
            }
            else
            {
                return TextCommandResult.Error(Lang.Get("kemono:msg-cmd-skin-load-error", filename));
            }
        }

        public TextCommandResult OnCmdSkinReload(TextCommandCallingArgs args)
        {
            bool reloadSkinparts = false;
            bool reloadClothing = false;
            bool reloadTextures = false;

            foreach (var parser in args.Parsers)
            {
                string arg = parser.GetValue() as string;
                if (arg == null) {
                    continue;
                }
                
                arg = arg.ToLower();

                if (arg == "parts" || arg == "skinpart" || arg == "skinparts")
                {
                    reloadSkinparts = true;
                }
                else if (arg == "cloth" || arg == "clothes" || arg == "clothing")
                {
                    reloadClothing = true;
                }
                else if (arg == "texture" || arg == "textures")
                {
                    reloadTextures = true;
                }
            }

            // reload models (reloads model assets folder)
            ReloadModels(capi);

            // TODO: reload all kemono entities loaded in world

            // get player entity
            var entity = capi.World.Player.Entity;
            var skin = entity.GetBehavior<EntityBehaviorKemonoSkinnable>();

            if (skin == null)
            {
                return TextCommandResult.Error(Lang.Get("kemono:msg-cmd-skin-reload-error-not-kemono"));
            }

            // force reload player model
            if (skin.Model != null)
            {
                skin.SetModel(skin.Model.Code, true);
            }

            (int numSkinpartsReloaded, int numClothingReloaded, int numTexturesReloaded) = skin.ReloadAssets(reloadSkinparts, reloadClothing, reloadTextures);

            capi.ShowChatMessage(Lang.Get("kemono:msg-cmd-skin-reload-main"));

            if (numSkinpartsReloaded > 0)
            {
                capi.ShowChatMessage(Lang.Get("kemono:msg-cmd-skin-reload-parts", numSkinpartsReloaded));
            }
            if (numClothingReloaded > 0)
            {
                capi.ShowChatMessage(Lang.Get("kemono:msg-cmd-skin-reload-clothing", numClothingReloaded));
            }
            if (numTexturesReloaded > 0)
            {
                capi.ShowChatMessage(Lang.Get("kemono:msg-cmd-skin-reload-textures", numTexturesReloaded));
            }

            return TextCommandResult.Success();
        }

        /// <summary>
        /// Client command to save current emblem image to png.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public TextCommandResult OnCmdEmblemSave(TextCommandCallingArgs args)
        {
            string filename = args.Parsers[0].GetValue() as string;
            bool result = EmblemSave(capi, filename);
            if (result == true)
            {
                return TextCommandResult.Success();
            }
            else
            {
                return TextCommandResult.Error(Lang.Get("kemono:msg-cmd-emblem-save-error", filename));
            }
        }

        /// <summary>
        /// Client command to load emblem from locally stored png file.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public TextCommandResult OnCmdEmblemLoad(TextCommandCallingArgs args)
        {
            string filename = args.Parsers[0].GetValue() as string;
            bool result = EmblemLoad(capi, filename);
            if (result == true)
            {
                // send new skin to server
                var entity = capi.World.Player.Entity;
                string modelCode = entity.WatchedAttributes.GetTreeAttribute("skinConfig")?.GetString(EntityBehaviorKemonoSkinnable.APPLIED_MODEL, DefaultModelCode);
                string raceCode = entity.WatchedAttributes.GetString("characterRace", DefaultRaceCode);
                string classCode = entity.WatchedAttributes.GetString("characterClass", "");
                ClientSelectionDone(modelCode, raceCode, classCode, true);

                return TextCommandResult.Success();
            }
            else
            {
                return TextCommandResult.Error(Lang.Get("kemono:msg-cmd-emblem-load-error", filename));
            }
        }

        /// <summary>
        /// Client command to load emblem from a emblem texture asset.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public TextCommandResult OnCmdEmblemTexLoad(TextCommandCallingArgs args)
        {
            string filename = args.Parsers[0].GetValue() as string;

            var entity = capi.World.Player.Entity;
            var skin = entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            if (skin == null)
            {
                return TextCommandResult.Error(Lang.Get("kemono:msg-cmd-emblem-load-error-not-kemono"));
            }
            
            try
            {
                var result = skin.SetEmblemFromTexture(new AssetLocation(filename));
                if (result.IsErr)
                {
                    capi.ShowChatMessage(result.Error);
                    return TextCommandResult.Error(Lang.Get("kemono:msg-cmd-emblem-load-error", filename));
                }
                
                // send new skin to server
                string modelCode = entity.WatchedAttributes.GetTreeAttribute("skinConfig")?.GetString(EntityBehaviorKemonoSkinnable.APPLIED_MODEL, DefaultModelCode);
                string raceCode = entity.WatchedAttributes.GetString("characterRace", DefaultRaceCode);
                string classCode = entity.WatchedAttributes.GetString("characterClass", "");
                ClientSelectionDone(modelCode, raceCode, classCode, true);

                return TextCommandResult.Success();
            }
            catch (Exception e)
            {
                entity.Api.Logger.Error($"Error loading emblem texture {filename}: " + e);
                return TextCommandResult.Error(Lang.Get("kemono:msg-cmd-emblem-load-error", filename));
            }
        }

        /// Client command to load skin config from json file.
        public TextCommandResult OnDebugTexturesCmd(TextCommandCallingArgs args)
        {
            var entity = capi.World.Player.Entity;
            var textures = entity.Properties.Client.Textures;

            capi.ShowChatMessage("entity.Properties.Client.Textures:");
            foreach (var tex in textures)
            {
                capi.ShowChatMessage($"- textures[{tex.Key}] = {tex.Value}");
            }

            return TextCommandResult.Success();
        }

        /// Server command to allow an online player to change their race.
        public TextCommandResult OnRaceChangeOnceCmd(TextCommandCallingArgs args)
        {
            IServerPlayer player = args.Parsers[0].GetValue() as IServerPlayer;
            if (player != null)
            {
                AllowRaceChange(player);
                return TextCommandResult.Success(Lang.Get("kemono:msg-cmd-racechange-success", player.PlayerName));
            }
            else
            {
                return TextCommandResult.Error(Lang.Get("kemono:msg-cmd-racechange-error-no-player", player.PlayerName));
            }
        }

    }
}
