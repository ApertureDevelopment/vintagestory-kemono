using Cairo;
using System;
using Vintagestory.API.Client;
using Vintagestory.API.Common;

namespace kemono
{
    /// https://raw.githubusercontent.com/anegostudios/vsapi/master/Client/UI/Elements/Impl/Static/GuiElementInset.cs
    /// Because this is private for whatever reason wtf?
    public class GuiElementImageButton : GuiElement
    {
        public AssetLocation Image;
        public Action OnClick;

        // color tint R, G, B, A array
        public double[] Tint;

        /// Create an clickable image button.
        public GuiElementImageButton(ICoreClientAPI capi, ElementBounds bounds, AssetLocation image, Action onClick, double[] tint) : base(capi, bounds)
        {
            Image = image.Clone().WithPathPrefixOnce("textures/").WithPathAppendixOnce(".png");
            OnClick = onClick;
            Tint = tint;
        }

        public override void OnMouseDownOnElement(ICoreClientAPI api, MouseEvent args)
        {
            base.OnMouseDownOnElement(api, args);

            if (OnClick != null) OnClick();
        }

        // reference:
        // https://github.com/anegostudios/vsapi/blob/master/Client/UI/Elements/Impl/Static/GuiElementImage.cs
        public override void ComposeElements(Context ctx, ImageSurface surface)
        {
            Bounds.CalcWorldBounds();
            
            if (Image != null)
            {
                var imageSurface = getImageSurfaceFromAsset(api, Image);
                float scale = (float) Math.Min(Bounds.InnerWidth / imageSurface.Width, Bounds.InnerHeight / imageSurface.Height);
            
                // ctx.Rectangle(Bounds.drawX, Bounds.drawY, Bounds.OuterWidth, Bounds.OuterHeight);
                ctx.PushGroup();
                // ctx.SetSource(pattern);
                // ctx.Rectangle(0, 0, Bounds.OuterWidth, Bounds.OuterHeight);
                ctx.Scale(scale, scale);
                ctx.SetSourceSurface(imageSurface, (int) (Bounds.drawX / scale), (int) (Bounds.drawY / scale));
                // ctx.SetSourceSurface(imageSurface, (int)Bounds.OuterWidth, (int)Bounds.OuterHeight);
                ctx.Paint();
                ctx.PopGroupToSource();
                ctx.Paint();

                // color tint
                if (Tint != null)
                {
                    Rectangle(ctx, Bounds.drawX, Bounds.drawY, Bounds.InnerWidth, Bounds.InnerHeight);
                    ctx.SetSourceRGBA(Tint[0], Tint[1], Tint[2], Tint[3]);
                    ctx.Fill();
                }

                imageSurface.Dispose();
            }
        }


        public override void RenderInteractiveElements(float deltaTime)
        {
            // if (image != null)
            // {
            //     api.Render.Render2DTexturePremultipliedAlpha(
            //         imageTexture.TextureId,
            //         Bounds.renderX,
            //         Bounds.renderY,
            //         (int) Bounds.OuterWidth,
            //         (int) Bounds.OuterHeight
            //     );
            // }
        }
    }


    public static partial class GuiComposerHelpers
    {
        public static GuiComposer AddImageButton(
            this GuiComposer composer,
            ElementBounds bounds,
            AssetLocation image,
            Action onClick,
            double[] tint = null,
            string key = null
        )
        {
            var elem = new GuiElementImageButton(
                composer.Api,
                bounds,
                image,
                onClick,
                tint
            );

            if (!composer.Composed)
            {
                composer.AddInteractiveElement(elem, key);
            }
            return composer;
        }

        public static GuiElementImageButton GetImageButton(this GuiComposer composer, string key)
        {
            return (GuiElementImageButton) composer.GetElement(key);
        }
    }
}
