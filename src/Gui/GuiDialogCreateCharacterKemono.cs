﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using Vintagestory.GameContent;

namespace kemono
{
    public class GuiDialogCreateCharacterKemono : GuiDialog
    {
        // constants:
        public const string CHARACTER = "kemonocreatecharacter";
        public const string RIGHTPANEL = "kemonocreaterightpanel";
        // gui tab integers
        public const int TAB_CHARACTER = 0;
        public const int TAB_EMBLEM = 1;
        public const int TAB_RACE = 2;
        public const int TAB_CLASS = 3;
        public const int TAB_DEBUG_RENDER = 4;

        // number of races per pagination page
        public const int RACES_PER_PAGE = 4;

        // tint for invalid race icon
        public static readonly double[] INVALID_RACE_TINT = {0.5, 0.0, 0.0, 0.5};

        // font for invalid race error message
        public static readonly CairoFont FONT_ERROR = CairoFont.WhiteMediumText().WithColor(new double[4] {0.8, 0, 0, 1.0});

        bool didSelect = false;
        protected IInventory characterInv;

        Dictionary<EnumCharacterDressType, int> DressPositionByTressType = new Dictionary<EnumCharacterDressType, int>();
        Dictionary<EnumCharacterDressType, ItemStack[]> DressesByDressType = new Dictionary<EnumCharacterDressType, ItemStack[]>();
        
        KemonoMod modSys;

        // GUI SIZING CONSTANTS
        const int innerWidth = 800;       // width of inner dialog, outer - 2*pad
        const int innerHeight = 440;      // height of inner dialog, outer - 2*pad
        const int titleHeight = 32;       // title bar height
        const int pad = 20;               // main screen padding on sides
        // outer width, height = inner + title + 2*pad
        const int outerWidth = innerWidth + 2 * pad;
        const int outerHeight = innerHeight + titleHeight + 2 * pad; 
        // inner screen elements
        const int charRenderWidth = 260;  // width of character renderer inset
        const int charRenderWidthHalf = charRenderWidth / 2; // half width of character renderer inset
        // vertical scrollbar width
        const int scrollbarWidth = 20;
        // width, height of right side panel (contains customization selections)
        const int rightPanelWidth = innerWidth - charRenderWidth - scrollbarWidth - 2 * 20;
        const int rightPanelHeight = innerHeight - 50;
        // y0 for screen top elements (buttons, save/load, etc.)
        const int yTop = titleHeight + 20;
        // y0 for inner screen elements
        const int y0 = yTop + 50;
        // x0 position for right side panel (right of char renderer)
        const int x0 = pad + charRenderWidth + 20;

        // character render inset on left for all tabs
        protected ElementBounds charRenderBounds;


        // MUTABLE STATE
        int selectedRaceIndex = 0;      // selected in gui, may not be valid
        int selectedRacePaginationPage = 0; // pagination page
        bool warnedRaceInvalid = false; // warned user race is invalid
        int selectedClassIndex = 0;
        int currTab = TAB_CHARACTER;

        // character tab right panel container
        GuiElementContainerWrapper CharacterPanel = null;
        
        // engine built-in skin preset
        public string LoadBuiltinSkin = "";
        public string[] BuiltinSkinPresetCodes = {""};

        // user local skin presets
        string saveSkinFilename = "";
        string[] loadSkinFilenames = {""};
        string loadSkinFilename = "";
        
        string saveEmblemFilename = "";
        string[] loadEmblemFilenames = {""};
        string loadEmblemFilename = "";

        float charZoom = 1f;
        bool charNaked = true;

        // color slider mode (default RGB, optional HSV)
        bool ColorSliderModeHSV = false;

        // emblem/cutie mark painter color state
        int colRed255 = 255;
        int colGreen255 = 0;
        int colBlue255 = 0;
        // double colAlpha255 = 255; // unused for now
        double colRed = 1.0;
        double colGreen = 0.0;
        double colBlue = 0.0;
        // double colAlpha = 1.0; // unused for now
        // color HSV format
        double colHue = 0.0;
        double colSat = 0.5;
        double colVal = 0.5;
        // color in vintage rgba format
        int colRGBA = ColorUtil.ColorFromRgba(255, 0, 0, 255);
        // canvas background color options
        const int CANVAS_COLOR_CLEAR = 0;
        const int CANVAS_COLOR_WHITE = ColorUtil.WhiteArgb; // ~0
        const int CANVAS_COLOR_BLACK = ColorUtil.OpaqueAlpha; // 255 << 24
        // canvas background color selected
        int canvasBackgroundColor = 0;

        // character render translate/offset
        // (for debugging)
        public double charRenderOffsetX = 0;
        public double charRenderOffsetY = 0;
        public double charRenderOffsetZ = 0;
        public float charRenderRotateX = -14;
        public float charRenderScaleX = -1;


        public GuiDialogCreateCharacterKemono(ICoreClientAPI capi, KemonoMod modSys) : base(capi)
        {
            this.modSys = modSys;

            // get race and class index from current entity
            var entity = capi.World.Player.Entity;
            string raceCode = entity.WatchedAttributes.GetString("characterRace");

            // determine initial race index
            if (raceCode != null)
            {
                // find first index in modSys.Races that matches raceCode
                selectedRaceIndex = modSys.Races.Select((value, index) => new { value, index = index + 1 })
                    .Where(pair => raceCode == pair.value.Code)
                    .Select(pair => pair.index)
                    .FirstOrDefault() - 1;
                if (selectedRaceIndex >= 0)
                {
                    selectedRacePaginationPage = selectedRaceIndex / RACES_PER_PAGE;
                }
                else
                {
                    selectedRaceIndex = 0;
                    selectedRacePaginationPage = 0;
                }
            }
            else
            {
                selectedRaceIndex = 0;
                selectedRacePaginationPage = 0;
            }

            // determine initial class code index
            string classCode = entity.WatchedAttributes.GetString("characterClass");

            selectedClassIndex = modSys.baseCharSys.characterClasses.Select((value, index) => new { value, index = index + 1 })
                .Where(pair => classCode == pair.value.Code)
                .Select(pair => pair.index)
                .FirstOrDefault() - 1;
            if (selectedClassIndex < 0) selectedClassIndex = 0;

            // initialize built-in skin presets from mod system
            string[] none = {""};
            BuiltinSkinPresetCodes = none.Concat(modSys.SkinPresetCodes).ToArray(); // TODO: load from resources

            // initializes correct RGBA colors from initial HSV values
            UpdateRgbaFromHsv();
        }

        protected void ComposeGuis()
        {
            characterInv = capi.World.Player.InventoryManager.GetOwnInventory(GlobalConstants.characterInvClassName);
            capi.World.Player.Entity.hideClothing = false;

            // clear old character panel
            CharacterPanel = null;

            ElementBounds tabBounds = ElementBounds.Fixed(0, -32, innerWidth, 32);
            
            GuiTab[] tabs;
            int[] tabToActiveIndex;
            if (modSys.Races.Count > 1)
            {
                tabs = new GuiTab[] {
                    new GuiTab() { Name = Lang.Get("kemono:gui-tab-character"), DataInt = TAB_CHARACTER },
                    new GuiTab() { Name = Lang.Get("kemono:gui-tab-emblem"), DataInt = TAB_EMBLEM },
                    new GuiTab() { Name = Lang.Get("kemono:gui-tab-race"), DataInt = TAB_RACE },
                    new GuiTab() { Name = Lang.Get("kemono:gui-tab-class"), DataInt = TAB_CLASS },
                };
                tabToActiveIndex = new int[] {0, 1, 2, 3, 4};
            }
            else
            {
                tabs = new GuiTab[] {
                    new GuiTab() { Name = Lang.Get("kemono:gui-tab-character"), DataInt = TAB_CHARACTER },
                    new GuiTab() { Name = Lang.Get("kemono:gui-tab-emblem"), DataInt = TAB_EMBLEM },
                    new GuiTab() { Name = Lang.Get("kemono:gui-tab-class"), DataInt = TAB_CLASS },
                };
                tabToActiveIndex = new int[] {0, 1, 2, 2, 3}; // no race tab
            }

            // tab-specific general layout
            string guiTitle;
            int charRenderHeight;
            int yCharRender;
            switch (currTab)
            {
                case TAB_CHARACTER:
                    guiTitle = Lang.Get("kemono:gui-title-character");
                    charRenderHeight = rightPanelHeight;
                    yCharRender = y0;
                    break;
                case TAB_EMBLEM:
                    guiTitle = Lang.Get("kemono:gui-title-emblem");
                    charRenderHeight = innerHeight;
                    yCharRender = yTop;
                    break;
                case TAB_RACE:
                    guiTitle = Lang.Get("kemono:gui-title-race");
                    charRenderHeight = innerHeight;
                    yCharRender = yTop;
                    break;
                case TAB_CLASS:
                    guiTitle = Lang.Get("kemono:gui-title-class");
                    charRenderHeight = innerHeight;
                    yCharRender = yTop;
                    break;
                default:
                    guiTitle = Lang.Get("kemono:gui-title-character");
                    charRenderHeight = rightPanelHeight;
                    yCharRender = y0;
                    break;
            };

            ElementBounds dialogBounds = ElementBounds
                .FixedSize(outerWidth, outerHeight)
                .WithAlignment(EnumDialogArea.CenterMiddle)
                .WithFixedAlignmentOffset(GuiStyle.DialogToScreenPadding, -40);
            
            ElementBounds bgBounds = ElementBounds
                .FixedSize(outerWidth, outerHeight);
            
            charRenderBounds = ElementBounds
                .Fixed(pad, yCharRender, charRenderWidth, charRenderHeight);
            
            Composers[CHARACTER] =
                capi.Gui
                .CreateCompo(CHARACTER, dialogBounds)
                .AddShadedDialogBG(bgBounds, true)
                .AddDialogTitleBar(guiTitle, OnTitleBarClose)
                .AddHorizontalTabs(tabs, tabBounds, onTabClicked, CairoFont.WhiteSmallText().WithWeight(Cairo.FontWeight.Bold), CairoFont.WhiteSmallText().WithWeight(Cairo.FontWeight.Bold), "tabs")
                .AddInset(charRenderBounds, 2)
                .BeginChildElements(bgBounds);
            
            var tabElem = Composers[CHARACTER].GetHorizontalTabs("tabs");
            tabElem.unscaledTabSpacing = 20;
            tabElem.unscaledTabPadding = 20;
            tabElem.activeElement = tabToActiveIndex[currTab];
            
            // tab specific rendering
            if (currTab == TAB_CHARACTER)
            {
                ComposeTabCharacter();
            }
            else if (currTab == TAB_EMBLEM)
            {
                ComposeTabEmblem();
            }
            else if (currTab == TAB_RACE)
            {
                ComposeTabRace();
            }
            else if (currTab == TAB_CLASS)
            {
                ComposeTabClass();
            }
            else if (currTab == TAB_DEBUG_RENDER)
            {
                ComposeTabDebugRender();
            }
            
            Composers[CHARACTER].Compose();
        }

        /// Create tab panel for main character model customization.
        private void ComposeTabCharacter()
        {
            // get saved skin filenames from local save directory
            GetLocalSkinFilenames();

            ElementBounds rightPanelBounds = ElementBounds.Fixed(x0, y0, rightPanelWidth, rightPanelHeight);
            ElementBounds rightPanelClipBounds = rightPanelBounds.ForkBoundingParent();
            ElementBounds rightPanelInsetBounds = ElementBounds.Fixed(x0 - 10, y0, rightPanelWidth + 26, rightPanelHeight);
            
            // vertical scrollbar
            ElementBounds scrollbarBounds = ElementBounds
                .FixedSize(0, rightPanelHeight)
                .WithAlignment(EnumDialogArea.RightFixed)
                .WithFixedWidth(20)
                .WithFixedAlignmentOffset(-pad, y0);
            
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();

            capi.World.Player.Entity.hideClothing = charNaked;
            var essr = capi.World.Player.Entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            essr.TesselateShape();

            CairoFont smallfont = CairoFont.WhiteSmallText();
            var textExt = smallfont.GetTextExtents(Lang.Get("Show dressed"));

            // buttons on top
            ElementBounds toggleDressedButtonBounds = ElementBounds
                .Fixed(EnumDialogArea.LeftTop, pad, yTop, 120, 20)
                .WithFixedPadding(10, 6);
            
            ElementBounds randomizeButtonBounds = ElementBounds
                .Fixed(EnumDialogArea.LeftTop, 0, yTop, 100, 20)
                .FixedRightOf(toggleDressedButtonBounds, 20)
                .WithFixedPadding(10, 6);

            ElementBounds loadInputTextBounds = ElementBounds
                .Fixed(EnumDialogArea.LeftTop, x0 - 10, yTop + 2, 130, 30);
            
            ElementBounds loadButtonBounds = ElementBounds
                .Fixed(EnumDialogArea.LeftTop, 0, yTop, 60, 20)
                .WithFixedPadding(10, 6)
                .FixedRightOf(loadInputTextBounds, 1);

            ElementBounds saveInputTextBounds = ElementBounds
                .Fixed(EnumDialogArea.LeftTop, 0, yTop + 2, 120, 30)
                .FixedRightOf(loadButtonBounds, 30);
            
            ElementBounds saveButtonBounds = ElementBounds
                .Fixed(EnumDialogArea.LeftTop, 0, yTop, 60, 20)
                .FixedRightOf(saveInputTextBounds, 1)
                .WithFixedPadding(10, 6);
            
            ElementBounds confirmButtonBounds = ElementBounds
                .Fixed(EnumDialogArea.RightTop, -pad + 2, yTop, 80, 20)
                .WithFixedPadding(10, 6);

            Composers[CHARACTER]
                .AddToggleButton(Lang.Get("kemono:gui-show-dressed-btn"), smallfont, OnToggleDressOnOff, toggleDressedButtonBounds, "showdressedtoggle")
                .AddSmallButton(Lang.Get("kemono:gui-randomize-btn"), OnRandomizeSkin, randomizeButtonBounds, EnumButtonStyle.Normal)
                .AddDropDown(loadSkinFilenames, loadSkinFilenames, 0, OnLoadSkinFilenameChange, loadInputTextBounds, "loadskinfilenamesdropdown")
                .AddSmallButton(Lang.Get("kemono:gui-load-btn"), OnLoadSkin, loadButtonBounds, EnumButtonStyle.Normal)
                // .AddTextInput(loadInputTextBounds, OnLoadInputTextChanged, null, "loadinputtext") // using dropdown instead now
                .AddTextInput(saveInputTextBounds, OnSaveSkinInputChanged, null, "saveinputtext")
                .AddSmallButton(Lang.Get("kemono:gui-save-btn"), OnSaveSkin, saveButtonBounds, EnumButtonStyle.Normal)
                .AddSmallButton(Lang.Get("kemono:gui-confirm-skin-btn"), OnNext, confirmButtonBounds, EnumButtonStyle.Normal)
            ;

            Composers[CHARACTER].GetToggleButton("showdressedtoggle").SetValue(!charNaked);
            // Composers[CHARACTER].GetTextInput("loadinputtext").SetPlaceHolderText(Lang.Get("kemono:gui-filename-placeholder"));
            Composers[CHARACTER].GetTextInput("saveinputtext").SetPlaceHolderText(Lang.Get("kemono:gui-filename-placeholder"));
            Composers[CHARACTER].GetTextInput("saveinputtext").SetValue(saveSkinFilename);
            
            Composers[CHARACTER]
                .AddInset(rightPanelInsetBounds, 2)
                .BeginClip(rightPanelClipBounds)
                .AddContainer(rightPanelBounds, "rightpanel");
            
            var rightPanel = new GuiElementContainerWrapper(capi, Composers[CHARACTER].GetContainer("rightpanel"));
            CharacterPanel = rightPanel;

            // ----------------------------------------
            // BEGIN RIGHT PANEL CUSTOMIZATION ELEMENTS
            // ----------------------------------------
            int columnWidth = 220; // width of each column in gui row
            int columnOffset = columnWidth + 30; // column offset in gui row

            string[] modelTypes = modSys.AvailableModelCodes;

            // get selected model index
            int selectedModelIndex = modSys.AvailableModelIndex[skin.Model.Code];

            // main model selection
            ElementBounds colorSliderModeLabelBounds = ElementBounds
                .Fixed(0, 10, 90, 30);
            ElementBounds colorSliderModeRGBBtnBounds = ElementBounds
                .Fixed(0, 10, 60, 30)
                .RightOf(colorSliderModeLabelBounds, 5);
            ElementBounds colorSliderModeHSVBtnBounds = ElementBounds
                .Fixed(0, 10, 60, 30)
                .RightOf(colorSliderModeRGBBtnBounds, 5);
            
            // main model selection
            ElementBounds modelSelectLabelBounds = ElementBounds
                .Fixed(0, 10, columnWidth, 30)
                .FixedUnder(colorSliderModeLabelBounds, 20);
            ElementBounds modelSelectBounds = ElementBounds
                .Fixed(0, 0, columnWidth, 25)
                .FixedUnder(modelSelectLabelBounds, 0);

            // presets load button and dropdown selection
            ElementBounds presetLoadButtonBounds = ElementBounds
                .Fixed(columnOffset, 10, columnWidth, 30)
                .FixedUnder(colorSliderModeLabelBounds, 20);
            
            ElementBounds presetLoadDropdownBounds = ElementBounds
                .Fixed(columnOffset, 0, columnWidth, 25)
                .FixedUnder(presetLoadButtonBounds, 0);
            
            rightPanel
                .AddRichtext(Lang.Get("kemono:gui-color-slider-mode"), CairoFont.WhiteSmallishText(), colorSliderModeLabelBounds)
                .AddToggleButton(
                    Lang.Get("kemono:gui-symbol-rgb"),
                    CairoFont.WhiteSmallText(),
                    (newVal) => { OnSetColorSliderModeHSV(false); },
                    colorSliderModeRGBBtnBounds,
                    "color-mode-btn-rgb"
                )
                .AddToggleButton(
                    Lang.Get("kemono:gui-symbol-hsv"),
                    CairoFont.WhiteSmallText(),
                    (newVal) => { OnSetColorSliderModeHSV(true); },
                    colorSliderModeHSVBtnBounds,
                    "color-mode-btn-hsv"
                )
                .AddRichtext(Lang.Get("kemono:gui-model-label"), CairoFont.WhiteSmallishText(), modelSelectLabelBounds)
                .AddDropDown(modelTypes, modelTypes, selectedModelIndex, OnSelectModel, modelSelectBounds, "dropdown-model")
                .AddSmallButton(Lang.Get("kemono:gui-preset-btn"), OnLoadBuiltinSkinPreset, presetLoadButtonBounds, EnumButtonStyle.Normal)
                .AddDropDown(BuiltinSkinPresetCodes, BuiltinSkinPresetCodes, 0, OnSelectBuiltinSkinPreset, presetLoadDropdownBounds, "dropdown-preset")
            ;

            // set color slider mode toggle button state
            rightPanel.GetElementAs<GuiElementToggleButton>("color-mode-btn-rgb").SetValue(!ColorSliderModeHSV);
            rightPanel.GetElementAs<GuiElementToggleButton>("color-mode-btn-hsv").SetValue(ColorSliderModeHSV);

            // start of customization options (dummy bounds)
            ElementBounds startCustomization = ElementBounds
                .Fixed(0, 0, 200, 20)
                .FixedUnder(presetLoadDropdownBounds, 40);
            
            int yRow = (int) startCustomization.fixedY;

            // ================================================================
            // SCALE PARTS CUSTOMIZATION
            // ================================================================
            // get current applied parts
            var appliedScale = skin.GetAppliedScale();

            int scaleTitleHeight = 40;
            int scaleTitlePadding = 4;
            ElementBounds boundsScaleTitle = ElementBounds
                .Fixed(0, yRow, rightPanelWidth, scaleTitleHeight);
            ElementBounds boundsScaleResetBtn = ElementBounds
                .Fixed(EnumDialogArea.RightFixed, 0, yRow, 120, scaleTitleHeight - 10);
            
            yRow += scaleTitleHeight + scaleTitlePadding;

            rightPanel
                .AddRichtext(
                    Lang.Get("kemono:gui-scalepart-title"),
                    CairoFont.WhiteSmallishText(),
                    boundsScaleTitle
                )
                .AddSmallButton(
                    Lang.Get("kemono:gui-scalepart-reset"),
                    OnPartScaleReset,
                    boundsScaleResetBtn,
                    EnumButtonStyle.Normal
                );

            foreach (var part in skin.Model.ScaleParts)
            {
                int yRowSize = 16;
                int yRowPadding = 12;
                int widthLabel = 120;
                int widthValue = 80;

                ElementBounds boundsSlider = ElementBounds.Fixed(0, yRow, rightPanelWidth, yRowSize);

                double initialScale = appliedScale?.TryGetDouble(part.Code) ?? 1.0;
                int initialScaleInt = (int) (initialScale * 100);
                int scaleMin = (int) (part.Min * 100);
                int scaleMax = (int) (part.Max * 100);

                rightPanel.AddScaleSlider(
                    Lang.Get("kemono:scalepart-" + part.Code),
                    part.Code,
                    boundsSlider,
                    widthLabel,
                    widthValue,
                    scaleMin,
                    scaleMax,
                    1,
                    initialScaleInt,
                    (scale) => { OnPartScaleChange(part.Code, ((double) scale) / 100.0); }
                );

                yRow += (yRowSize + yRowPadding);
            }
            
            // ================================================================
            // SKIN PARTS CUSTOMIZATION
            // ================================================================

            // get current applied parts
            var appliedVariants = skin.AppliedSkinParts;
            var appliedColors = skin.GetAppliedColors();

            foreach (var rowParts in skin.Model.GuiLayout)
            {
                // start new row in gui layout

                int x = 0;
                int yRowSize = 40; // row height = max { col left height, col right height }
                int yRowPadding = 40;

                foreach (var code in rowParts)
                {
                    // each skin part in the row

                    int y = yRow; // y position of element in row, increases as elements added
                    int columnHeight = 0; // calculated from elements, for setting yRowHeight
                    int dy = 0; // offset between elements in column
                    ElementBounds bounds = ElementBounds.Fixed(x, y, columnWidth, 40);

                    KemonoSkinnablePart skinpart;
                    if (!skin.Model.SkinPartsByCode.TryGetValue(code, out skinpart))
                    {
                        rightPanel.AddRichtext("MISSING: " + code, CairoFont.WhiteSmallishText(), bounds);
                        continue;
                    }

                    // label
                    rightPanel.AddRichtext(
                        Lang.Get("kemono:skinpart-" + code),
                        CairoFont.WhiteSmallishText(),
                        bounds = bounds.BelowCopy(0, 0).WithFixedSize(240, 30)
                    );

                    AppliedKemonoSkinnablePartVariant appliedVar = appliedVariants.FirstOrDefault(sp => sp.PartCode == code);

                    // dropdown variant selection
                    if (skinpart.UseDropDown)
                    {
                        if (skinpart.Variants.Length > 0)
                        {
                            string[] names = new string[skinpart.Variants.Length];
                            string[] values = new string[skinpart.Variants.Length];

                            int selectedIndex = 0;

                            for (int i = 0; i < skinpart.Variants.Length; i++)
                            {
                                names[i] = Lang.Get("kemono:skinpart-" + code + "-" + skinpart.Variants[i].Code);
                                values[i] = skinpart.Variants[i].Code;

                                //Console.WriteLine("\"" + names[i] + "\": \"" + skinpart.Variants[i].Code + "\",");

                                if (appliedVar?.Code == values[i]) selectedIndex = i;
                            }

                            string tooltip = Lang.GetIfExists("kemono:skinpartdesc-" + code);
                            if (tooltip != null)
                            {
                                rightPanel.AddHoverText(tooltip, CairoFont.WhiteSmallText(), 300, bounds = bounds.FlatCopy());
                            }

                            rightPanel.AddDropDown(
                                values,
                                names,
                                selectedIndex,
                                (variantcode, selected) => OnSelectSkinPart(code, variantcode),
                                bounds = bounds.BelowCopy(0, dy).WithFixedSize(columnWidth, 25),
                                "dropdown-" + code
                            );
                        }
                        else
                        {
                            bounds = bounds.BelowCopy(0, dy).WithFixedSize(columnWidth, 25);
                            rightPanel.AddInset(bounds, 2);
                        }

                        columnHeight += 45;
                        dy = 10;
                    }

                    // RGB color sliders
                    if (skinpart.UseColorSlider)
                    {
                        // set initial color from skinpart
                        int initialRed = 69;
                        int initialGreen = 69;
                        int initialBlue = 69;

                        int? initialColor = appliedColors?.TryGetInt(code);
                        if (initialColor != null) {
                            // unpack initial color
                            (initialRed, initialGreen, initialBlue) = KemonoColorUtil.ToRgb((int) initialColor);
                        } else {
                            // select initial color for skinpart
                            int packedColor = ColorUtil.ColorFromRgba(69, 69, 69, 255);
                            skin.SetSkinPartColor(code, packedColor);
                        }

                        string descPartColorCode = "kemono:skinpart-" + code + "-color";

                        // TODO: currently converting between RGB <-> HSV is LOSSY
                        // if you switch between modes, exact values will change
                        // this is due to the integer/double conversions and different
                        // resolutions used in HSV (0, 100) vs RGB (0, 255)
                        // need to decide how to handle this in future...
                        if ( ColorSliderModeHSV )
                        {
                            (double initialHue, double initialSat, double initialVal) = KemonoColorUtil.RgbToHsv(initialRed, initialGreen, initialBlue);
                            
                            rightPanel.AddHSVColorSliders(
                                Lang.Get(descPartColorCode),
                                descPartColorCode,
                                columnWidth, // width
                                16,          // height
                                bounds = bounds.BelowCopy(0, dy).WithFixedSize(columnWidth, 80),
                                initialHue,
                                initialSat,
                                initialVal,
                                (col) => { OnPartColorChange(code, col); }
                            );
                        }
                        else
                        {
                            rightPanel.AddRGBColorSliders(
                                Lang.Get(descPartColorCode),
                                descPartColorCode,
                                columnWidth, // width
                                16,          // height
                                bounds = bounds.BelowCopy(0, dy).WithFixedSize(columnWidth, 80),
                                initialRed,
                                initialGreen,
                                initialBlue,
                                (col) => { OnPartColorChange(code, col); }
                            );
                        }

                        columnHeight += 100;
                        dy = 10;
                    }

                    // transform sliders
                    // -> always place on right column side
                    if (skinpart.UseTransformSlider)
                    {
                        // TODO
                    }

                    x += columnOffset;

                    yRowSize = Math.Max(yRowSize, columnHeight);
                }

                yRow += (yRowSize + yRowPadding);
            }

            ElementBounds bottomPadding = ElementBounds.Fixed(0, yRow, 200, 40);
            rightPanel.AddPadding(bottomPadding);
            
            Composers[CHARACTER]
                .EndClip()
                .AddVerticalScrollbar(OnNewScrollbarValue, scrollbarBounds, "scrollbar");

            rightPanel.container.CalcTotalHeight();
            Composers[CHARACTER].GetScrollbar("scrollbar").Bounds.CalcWorldBounds();
            Composers[CHARACTER].GetScrollbar("scrollbar")?.SetHeights(
                (float)(rightPanelHeight),
                (float)(rightPanel.container.Bounds.fixedHeight)
            );
        }

        /// Create tab panel for emblem customization.
        private void ComposeTabEmblem()
        {
            // get saved emblem filenames from local save directory
            GetLocalEmblemFilenames();

            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();

            capi.World.Player.Entity.hideClothing = charNaked;
            var essr = capi.World.Player.Entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            essr.TesselateShape();
            
            // save/load emblem buttons on top
            ElementBounds loadInputTextBounds = ElementBounds
                .Fixed(EnumDialogArea.LeftTop, x0 - 10, yTop + 2, 130, 30);

            ElementBounds loadButtonBounds = ElementBounds
                .Fixed(EnumDialogArea.LeftTop, 0, yTop, 60, 20)
                .WithFixedPadding(10, 6)
                .FixedRightOf(loadInputTextBounds, 1);
            
            ElementBounds saveInputTextBounds = ElementBounds
                .Fixed(EnumDialogArea.LeftTop, 0, yTop + 2, 120, 30)
                .FixedRightOf(loadButtonBounds, 30);
            
            ElementBounds saveButtonBounds = ElementBounds
                .Fixed(EnumDialogArea.LeftTop, 0, yTop, 60, 20)
                .FixedRightOf(saveInputTextBounds, 1)
                .WithFixedPadding(10, 6);
            
            ElementBounds confirmButtonBounds = ElementBounds
                .Fixed(EnumDialogArea.RightTop, -pad + 2, yTop, 80, 20)
                .WithFixedPadding(10, 6);

            Composers[CHARACTER]
                .AddDropDown(loadEmblemFilenames, loadEmblemFilenames, 0, OnLoadEmblemFilenameChange, loadInputTextBounds, "loademblemfilenamesdropdown")
                .AddSmallButton(Lang.Get("kemono:gui-load-btn"), OnLoadEmblem, loadButtonBounds, EnumButtonStyle.Normal)
                // .AddTextInput(loadInputTextBounds, OnLoadInputTextChanged, null, "loademblemtext") // using dropdown instead now
                .AddTextInput(saveInputTextBounds, OnSaveEmblemInputChanged, null, "saveemblemtext")
                .AddSmallButton(Lang.Get("kemono:gui-save-btn"), OnSaveEmblem, saveButtonBounds, EnumButtonStyle.Normal)
                .AddSmallButton(Lang.Get("kemono:gui-confirm-emblem-btn"), OnNext, confirmButtonBounds, EnumButtonStyle.Normal)
            ;

            // Composers[CHARACTER].GetTextInput("loademblemtext").SetPlaceHolderText(Lang.Get("kemono:gui-filename-placeholder"));
            Composers[CHARACTER].GetTextInput("saveemblemtext").SetPlaceHolderText(Lang.Get("kemono:gui-filename-placeholder"));
            Composers[CHARACTER].GetTextInput("saveemblemtext").SetValue(saveEmblemFilename);

            // make painter

            // LEFT SIDE: canvas
            ElementBounds canvasBounds = ElementBounds
                .Fixed(x0, y0, 320, 320);
            
            // instructions on bottom
            ElementBounds instructionBounds = ElementBounds
                .Fixed(x0, -pad, 500, 30)
                .WithAlignment(EnumDialogArea.LeftBottom);
            Composers[CHARACTER].AddRichtext(
                Lang.Get("kemono:gui-canvas-instructions"),
                CairoFont.WhiteDetailText(),
                instructionBounds
            );

            // RIGHT SIDE: tools and color selection
            
            // sat/val selection box 
            ElementBounds satValBounds = ElementBounds
                .Fixed(0, y0, 150, 150)
                .FixedRightOf(canvasBounds, 10);
            // hue slider
            ElementBounds hueSliderBounds = ElementBounds
                .Fixed(0, 0, 150, 20)
                .FixedRightOf(canvasBounds, 10)
                .FixedUnder(satValBounds, 10);
            
            BitmapSimple initialBmp = new BitmapSimple(EntityBehaviorKemonoSkinnable.EMBLEM_SIZE, EntityBehaviorKemonoSkinnable.EMBLEM_SIZE, skin.GetEmblemPixels());
            
            Composers[CHARACTER].AddCanvas(canvasBounds, colRGBA, initialBmp, OnCanvasPaint, OnCanvasSelectColor, "canvas");
            Composers[CHARACTER].AddColorSatValSelector(satValBounds, colHue, colSat, colVal, OnPainterSatValChange, "satvalselector");
            Composers[CHARACTER].AddColorHueSlider(hueSliderBounds, colHue, OnPainterHueChange, "hueslider");

            // set background color selection 
            ElementBounds canvasBackgroundColorLabelBounds = ElementBounds
                .Fixed(0, 0, 150, 20)
                .FixedRightOf(canvasBounds, 10)
                .FixedUnder(hueSliderBounds, 16);
            
            ElementBounds canvasBackgroundColorClearBounds = ElementBounds
                .Fixed(0, 0, 20, 20)
                .FixedRightOf(canvasBounds, 10)
                .FixedUnder(canvasBackgroundColorLabelBounds, 4);
            ElementBounds canvasBackgroundColorClearLabelBounds = ElementBounds
                .Fixed(0, 0, 120, 22)
                .FixedRightOf(canvasBackgroundColorClearBounds, 4)
                .FixedUnder(canvasBackgroundColorLabelBounds, 4);
            
            ElementBounds canvasBackgroundColorWhiteBounds = ElementBounds
                .Fixed(0, 0, 20, 20)
                .FixedRightOf(canvasBounds, 10)
                .FixedUnder(canvasBackgroundColorClearLabelBounds, 4);
            ElementBounds canvasBackgroundColorWhiteLabelBounds = ElementBounds
                .Fixed(0, 0, 120, 22)
                .FixedRightOf(canvasBackgroundColorWhiteBounds, 4)
                .FixedUnder(canvasBackgroundColorClearLabelBounds, 4);
            
            ElementBounds canvasBackgroundColorBlackBounds = ElementBounds
                .Fixed(0, 0, 20, 20)
                .FixedRightOf(canvasBounds, 10)
                .FixedUnder(canvasBackgroundColorWhiteLabelBounds, 4);
            ElementBounds canvasBackgroundColorBlackLabelBounds = ElementBounds
                .Fixed(0, 0, 120, 22)
                .FixedRightOf(canvasBackgroundColorBlackBounds, 4)
                .FixedUnder(canvasBackgroundColorWhiteLabelBounds, 4);
            
            Composers[CHARACTER].AddRichtext(
                Lang.Get("kemono:gui-canvas-background-color-label"),
                CairoFont.WhiteSmallText(),
                canvasBackgroundColorLabelBounds
            );

            Composers[CHARACTER].AddInset(
                canvasBackgroundColorClearBounds,
                2
            );
            Composers[CHARACTER].AddToggleButton(
                Lang.Get("kemono:gui-canvas-background-color-clear-label"),
                CairoFont.WhiteSmallText(),
                (newVal) => { OnSetCanvasBackgroundColor(false, CANVAS_COLOR_CLEAR); },
                canvasBackgroundColorClearLabelBounds,
                "canvas-background-color-clear"
            );

            Composers[CHARACTER].AddColorRectangle(
                canvasBackgroundColorWhiteBounds,
                255,
                255,
                255
            );
            Composers[CHARACTER].AddToggleButton(
                Lang.Get("kemono:gui-canvas-background-color-white-label"),
                CairoFont.WhiteSmallText(),
                (newVal) => { OnSetCanvasBackgroundColor(true, CANVAS_COLOR_WHITE); },
                canvasBackgroundColorWhiteLabelBounds,
                "canvas-background-color-white"
            );

            Composers[CHARACTER].AddColorRectangle(
                canvasBackgroundColorBlackBounds,
                0,
                0,
                0
            );
            Composers[CHARACTER].AddToggleButton(
                Lang.Get("kemono:gui-canvas-background-color-black-label"),
                CairoFont.WhiteSmallText(),
                (newVal) => { OnSetCanvasBackgroundColor(true, CANVAS_COLOR_BLACK); },
                canvasBackgroundColorBlackLabelBounds,
                "canvas-background-color-black"
            );

            // initialize canvas back ground color
            switch ( canvasBackgroundColor )
            {
                case CANVAS_COLOR_CLEAR:
                    OnSetCanvasBackgroundColor(false, CANVAS_COLOR_CLEAR);
                    break;
                case CANVAS_COLOR_WHITE:
                    OnSetCanvasBackgroundColor(true, CANVAS_COLOR_WHITE);
                    break;
                case CANVAS_COLOR_BLACK:
                    OnSetCanvasBackgroundColor(true, CANVAS_COLOR_BLACK);
                    break;
                default: // clear
                    OnSetCanvasBackgroundColor(false, CANVAS_COLOR_CLEAR);
                    break;
            }

            // clear emblem button
            ElementBounds clearEmblemBtnBounds = ElementBounds
                .Fixed(0, 0, 144, 30)
                .FixedRightOf(canvasBounds, 10)
                .FixedUnder(canvasBackgroundColorBlackBounds, 20);

            Composers[CHARACTER].AddSmallButton(
                Lang.Get("kemono:gui-clear-emblem"),
                OnClearEmblem,
                clearEmblemBtnBounds
            );
        }

        /// Create tab panel for character race selection.
        private void ComposeTabRace()
        {
            var essr = capi.World.Player.Entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            essr.TesselateShape();
            
            // size of char selection toggle on top of right panel
            const int selectionHeight = 100;
            const int selectionBtnPad = 8;
            const int selectionBtnWidth = 60 - 2 * selectionBtnPad;
            const int selectionBtnOffset = 10;
            const int selectionMiddleWidth = rightPanelWidth - 2 * selectionBtnWidth - 2 * selectionBtnOffset;
            const int raceTooltipWidth = 300;

            ElementBounds prevButtonBounds = ElementBounds
                .Fixed(x0, yTop, selectionBtnWidth, selectionHeight)
                .WithFixedPadding(selectionBtnPad);

            ElementBounds centerTextBounds = ElementBounds
                .Fixed(0, yTop, selectionMiddleWidth, selectionHeight + 8)
                .FixedRightOf(prevButtonBounds, 4 + selectionBtnPad + selectionBtnOffset);
            
            ElementBounds charclassInset = centerTextBounds.ForkBoundingParent(4, 4, 4, 4);

            ElementBounds nextButtonBounds = ElementBounds
                .Fixed(0, yTop, selectionBtnWidth, selectionHeight)
                .WithFixedPadding(selectionBtnPad)
                .FixedRightOf(charclassInset, selectionBtnOffset);

            CairoFont font = CairoFont.WhiteMediumText();
            centerTextBounds.fixedY += (centerTextBounds.fixedHeight - font.GetFontExtents().Height / RuntimeEnv.GUIScale) / 2;

            ElementBounds nameBounds = ElementBounds
                .Fixed(x0, 0, rightPanelWidth, 20)
                .FixedUnder(prevButtonBounds, 28);
            
            ElementBounds descBounds = ElementBounds
                .Fixed(x0, 0, rightPanelWidth, 100)
                .FixedUnder(nameBounds, 16);

            ElementBounds confirmBtnBounds = ElementBounds
                .Fixed(-pad, -pad, 100, 20)
                .WithAlignment(EnumDialogArea.RightBottom)
                .WithFixedPadding(12, 6);
            
            // if invalid, put error text on side
            ElementBounds errorTextBounds = ElementBounds
                .Fixed(x0 + 120, -pad, rightPanelWidth - 100, 20)
                .WithAlignment(EnumDialogArea.LeftBottom);
            
            Composers[CHARACTER]
                .AddIconButton("left", (on) => changeRacePaginationPage(-1), prevButtonBounds.FlatCopy())
                // .AddInset(charclassInset, 2)
                // .AddDynamicText("Unknown", font.Clone().WithOrientation(EnumTextOrientation.Center), centerTextBounds, "raceName")
                .AddIconButton("right", (on) => changeRacePaginationPage(1), nextButtonBounds.FlatCopy())

                .AddRichtext("", CairoFont.WhiteSmallishText(), nameBounds, "raceName")
                .AddRichtext("", CairoFont.WhiteDetailText(), descBounds, "raceDesc")
                .AddSmallButton(Lang.Get("kemono:gui-confirm-race-btn"), OnNext, confirmBtnBounds, EnumButtonStyle.Normal, "confirmracebtn")
                .AddRichtext("", CairoFont.WhiteMediumText().WithColor(new double[4] {0.8, 0, 0, 1.0}), errorTextBounds, "raceError")
            ;

            // race selection paginated icons and text
            const int selectionIconPad = 4;
            const int selectionIconSize = ((selectionMiddleWidth + 8) / RACES_PER_PAGE) - 2*selectionIconPad;
            for (var n = 0; n < RACES_PER_PAGE; n++)
            {
                // actual selected race index
                int i = selectedRacePaginationPage * RACES_PER_PAGE + n;

                // x gui offset for selection icon
                int x = 4 + selectionBtnPad + selectionBtnOffset + n * (selectionIconSize + 2*selectionIconPad);

                int insetDepth = 1;

                ElementBounds selectionBounds = ElementBounds
                    .Fixed(0, yTop, selectionIconSize, selectionIconSize)
                    .FixedRightOf(prevButtonBounds, x)
                    .WithFixedPadding(selectionIconPad, selectionIconPad);
                
                ElementBounds iconBounds = selectionBounds.FlatCopy();
                
                ElementBounds raceLabelBounds = ElementBounds
                    .Fixed(0, 0, selectionIconSize, 20)
                    .FixedRightOf(prevButtonBounds, x)
                    .WithFixedPadding(selectionIconPad, 0)
                    .FixedUnder(selectionBounds, 4);
                
                CairoFont labelFont = CairoFont.WhiteSmallText().WithOrientation(EnumTextOrientation.Center);

                double[] iconTint = null;

                // selected race unique adjustments
                if (i == selectedRaceIndex)
                {
                    insetDepth = 12;
                    labelFont = labelFont.WithWeight(Cairo.FontWeight.Bold);
                }
                
                // character icon
                if (i < modSys.Races.Count)
                {
                    KemonoRace race = modSys.Races[i];
                    AssetLocation icon = race.Icon != null ? race.Icon : KemonoRace.DefaultIcon;
    
                    // check if race valid
                    var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
                    var skinparts = skin.GetSkinPartsTreeDictionary();
                    if (skinparts.tree != null)
                    {
                        bool raceIsValid = KemonoRace.IsValid(race, capi.World.Player, skin.Model.Code, skinparts);
                        if (!raceIsValid)
                        {
                            labelFont = labelFont.WithColor(new double[] {0.8, 0.0, 0.0, 1.0});
                            iconTint = INVALID_RACE_TINT;

                            if (i == selectedRaceIndex)
                            {
                                Composers[CHARACTER].GetButton("confirmracebtn").Enabled = false;
                                Composers[CHARACTER].GetRichtext("raceError").SetNewTextWithoutRecompose(Lang.Get("kemono:race-error-invalid"), FONT_ERROR);
                            }
                        }
                    }

                    // create race tooltip showing requirements
                    StringBuilder tooltip = new StringBuilder();

                    tooltip.AppendLine("<font weight=\"bold\" size=20>" + Lang.Get($"kemono:race-name-{race.Code}") + "</font>");
                    tooltip.AppendLine();

                    if (race.Privilege != null)
                    {
                        tooltip.AppendLine("<i>" + Lang.Get($"kemono:race-required-privilege") + "</i>: " + race.Privilege);
                        tooltip.AppendLine();
                    }

                    if (race.Model != null)
                    {
                        tooltip.AppendLine("<i>" + Lang.Get($"kemono:race-required-model") + "</i>: " + race.Model);
                        tooltip.AppendLine();
                    }
                    
                    if (race.RequiredParts.Count > 0)
                    {
                        tooltip.AppendLine("<i>" + Lang.Get($"kemono:race-required-parts") + "</i>:");
                        foreach (var required in race.RequiredParts)
                        {
                            tooltip.AppendLine("   " + Lang.Get($"kemono:skinpart-{required.Key}") + ":");
                            foreach (var variant in required.Value)
                            {
                                if (variant == "*")
                                {
                                    tooltip.AppendLine("     *");
                                }
                                else
                                {
                                    tooltip.AppendLine("     - " + Lang.Get($"kemono:skinpart-{required.Key}-{variant}"));
                                }
                            }
                        }
                        tooltip.AppendLine();
                    }

                    if (race.BannedParts.Count > 0)
                    {
                        tooltip.AppendLine("<i>" + Lang.Get($"kemono:race-banned-parts") + "</i>:");
                        foreach (var banned in race.BannedParts)
                        {
                            tooltip.AppendLine("   " + Lang.Get($"kemono:skinpart-{banned.Key}") + ":");
                            foreach (var variant in banned.Value)
                            {
                                tooltip.AppendLine("     - " + Lang.Get($"kemono:skinpart-{banned.Key}-{variant}"));
                            }
                        }
                        tooltip.AppendLine();
                    }

                    Composers[CHARACTER]
                        .AddImageButton(iconBounds, icon, () => {changeRace(i);}, iconTint)
                        .AddInset(selectionBounds, insetDepth)
                        .AddInset(raceLabelBounds.FlatCopy(), 1) // need to .FloatCopy() because text modifies inset
                        .AddDynamicText(Lang.Get($"kemono:race-name-{race.Code}"), labelFont, raceLabelBounds)
                        .AddHoverText(tooltip.ToString(), CairoFont.WhiteDetailText(), raceTooltipWidth, iconBounds)
                    ;
                }
                else // just insets
                {
                    Composers[CHARACTER]
                        .AddInset(selectionBounds, insetDepth)
                        .AddInset(raceLabelBounds.FlatCopy(), 1) // need to .FlatCopy() because text modifies inset
                    ;
                }

            }

            loadRaceDescription(selectedRaceIndex);
        }

        /// Create tab panel for character class selection.
        private void ComposeTabClass()
        {
            var essr = capi.World.Player.Entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            essr.TesselateShape();
            
            // size of char selection name
            const int selectionHeight = 50;
            const int selectionBtnPad = 8;
            const int selectionBtnWidth = 60 - 2 * selectionBtnPad;
            const int selectionBtnOffset = 20;
            const int selectionTitleWidth = rightPanelWidth - 2 * selectionBtnWidth - 2 * selectionBtnOffset;

            ElementBounds prevButtonBounds = ElementBounds
                .Fixed(x0, yTop, selectionBtnWidth, selectionHeight)
                .WithFixedPadding(selectionBtnPad);

            ElementBounds centerTextBounds = ElementBounds
                .Fixed(0, yTop, selectionTitleWidth, selectionHeight + 8)
                .FixedRightOf(prevButtonBounds, 4 + selectionBtnPad + selectionBtnOffset); // wtf?
            ElementBounds charclasssInset = centerTextBounds.ForkBoundingParent(4, 4, 4, 4);

            ElementBounds nextButtonBounds = ElementBounds
                .Fixed(0, yTop, selectionBtnWidth, selectionHeight)
                .WithFixedPadding(selectionBtnPad)
                .FixedRightOf(charclasssInset, selectionBtnOffset);

            CairoFont font = CairoFont.WhiteMediumText();
            centerTextBounds.fixedY += (centerTextBounds.fixedHeight - font.GetFontExtents().Height / RuntimeEnv.GUIScale) / 2;

            ElementBounds charTextBounds = ElementBounds.Fixed(x0, 0, rightPanelWidth, 100).FixedUnder(prevButtonBounds, 32);

            ElementBounds confirmBtnBounds = ElementBounds
                .Fixed(-pad, -pad, 100, 20)
                .WithAlignment(EnumDialogArea.RightBottom)
                .WithFixedPadding(12, 6);

            Composers[CHARACTER]
                .AddIconButton("left", (on) => changeClass(-1), prevButtonBounds.FlatCopy())
                .AddInset(charclasssInset, 2)
                .AddDynamicText("Commoner", font.Clone().WithOrientation(EnumTextOrientation.Center), centerTextBounds, "className")
                .AddIconButton("right", (on) => changeClass(1), nextButtonBounds.FlatCopy())

                .AddRichtext("", CairoFont.WhiteDetailText(), charTextBounds, "characterDesc")
                .AddSmallButton(Lang.Get("Confirm Class"), OnConfirm, confirmBtnBounds, EnumButtonStyle.Normal)
            ;

            changeClass(0);
        }

        /// DEBUGGING TAB
        /// Default engine RenderEntityToGui flips x-axis
        /// This tab is for moving character render to figure out how
        /// to re-flip it properly >:^(
        private void ComposeTabDebugRender()
        {
            ElementBounds translateLabel = ElementBounds
                .Fixed(x0, y0, 200, 20);
            ElementBounds translateXBounds = ElementBounds
                .Fixed(x0, 0, 200, 20)
                .FixedUnder(translateLabel, 4);
            ElementBounds translateYBounds = ElementBounds
                .Fixed(x0, 0, 200, 20)
                .FixedUnder(translateXBounds, 4);
            ElementBounds translateZBounds = ElementBounds
                .Fixed(x0, 0, 200, 20)
                .FixedUnder(translateYBounds, 4);
            
            ElementBounds rotateLabel = ElementBounds
                .Fixed(x0, 0, 200, 20)
                .FixedUnder(translateZBounds, 8);
            ElementBounds rotateXBounds = ElementBounds
                .Fixed(x0, 0, 200, 20)
                .FixedUnder(rotateLabel, 4);
            
            ElementBounds scaleLabel = ElementBounds
                .Fixed(x0, 0, 200, 20)
                .FixedUnder(rotateXBounds, 8);
            ElementBounds scaleXBounds = ElementBounds
                .Fixed(x0, 0, 200, 20)
                .FixedUnder(scaleLabel, 4);
            
            Composers[CHARACTER]
                .AddRichtext("Translate", CairoFont.WhiteSmallText(), translateLabel)
                .AddSlider(
                    (newVal) => { charRenderOffsetX = newVal; return true; },
                    translateXBounds,
                    "debugtranslatex"
                )
                .AddSlider(
                    (newVal) => { charRenderOffsetY = newVal; return true; },
                    translateYBounds,
                    "debugtranslatey"
                )
                .AddSlider(
                    (newVal) => { charRenderOffsetZ = newVal; return true; },
                    translateZBounds,
                    "debugtranslatez"
                )

                .AddRichtext("Rotate", CairoFont.WhiteSmallText(), rotateLabel)
                .AddSlider(
                    (newVal) => { charRenderRotateX = (float) newVal; return true; },
                    rotateXBounds,
                    "debugrotatex"
                )

                .AddRichtext("Scale", CairoFont.WhiteSmallText(), scaleLabel)
                .AddSlider(
                    (newVal) => { charRenderScaleX = ((float)newVal)/100f; return true; },
                    scaleXBounds,
                    "debugscalex"
                )
            ;

            Composers[CHARACTER].GetSlider("debugtranslatex").SetValues((int)charRenderOffsetX, -1000, 1000, 1);
            Composers[CHARACTER].GetSlider("debugtranslatey").SetValues((int)charRenderOffsetY, -1000, 1000, 1);
            Composers[CHARACTER].GetSlider("debugtranslatez").SetValues((int)charRenderOffsetZ, -1000, 1000, 1);
            Composers[CHARACTER].GetSlider("debugrotatex").SetValues((int) charRenderRotateX, -180, 180, 1);
            Composers[CHARACTER].GetSlider("debugscalex").SetValues((int)(charRenderScaleX * 100.0), -200, 200, 1);
        }

        public bool OnNext()
        {
            // handle manually because race tab may not exist
            switch (currTab)
            {
                case TAB_CHARACTER:
                    currTab = TAB_EMBLEM;
                    break;
                case TAB_EMBLEM:
                    currTab = (modSys.Races.Count > 1) ? TAB_RACE : TAB_CLASS;
                    break;
                case TAB_RACE:
                    currTab = TAB_CLASS;
                    break;
                default:
                    currTab = TAB_CHARACTER;
                    break;
            }
            ComposeGuis();
            return true;
        }

        public void onTabClicked(int tabid)
        {
            currTab = tabid;
            ComposeGuis();
        }

        public override void OnGuiOpened()
        {
            string charclass = capi.World.Player.Entity.WatchedAttributes.GetString("characterClass");
            if (charclass != null)
            {
                modSys.baseCharSys.setCharacterClass(capi.World.Player.Entity, charclass, true);
            }
            else 
            {
                modSys.baseCharSys.setCharacterClass(capi.World.Player.Entity, modSys.baseCharSys.characterClasses[0].Code, true);
            }

            ComposeGuis();
            var essr = capi.World.Player.Entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            essr.TesselateShape();
        }


        public override void OnGuiClosed()
        {
            KemonoRace race = modSys.Races[selectedRaceIndex];
            CharacterClass chclass = modSys.baseCharSys.characterClasses[selectedClassIndex];
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            
            modSys.ClientSelectionDone(skin.Model.Code, race.Code, chclass.Code, didSelect);

            capi.World.Player.Entity.hideClothing = false;
            var essr = capi.World.Player.Entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            essr.TesselateShape();
        }

        public bool OnConfirm()
        {
            // validate if selected race is valid for skin
            // if not, warn once
            KemonoRace race = modSys.Races[selectedRaceIndex];
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            var skinparts = skin.GetSkinPartsTreeDictionary();
            if (skinparts.tree != null)
            {
                bool isRaceValid = KemonoRace.IsValid(race, capi.World.Player, skin.Model.Code, skinparts);
                if (!isRaceValid && !warnedRaceInvalid)
                {
                    warnedRaceInvalid = true;
                    currTab = TAB_RACE;
                    ComposeGuis();
                    return true;
                }
            }

            didSelect = true;
            TryClose();
            return true;
        }

        protected virtual void OnTitleBarClose()
        {
            TryClose();
        }

        public void OnNewScrollbarValue(float value)
        {
            var rightPanel = Composers[CHARACTER].GetContainer("rightpanel");
            if (rightPanel != null) {
                rightPanel.Bounds.fixedY = 3 - value;
                rightPanel.Bounds.CalcWorldBounds();
            }
        }

        public void GetLocalSkinFilenames()
        {
            string[] none = {""};
            string[] files = Directory
                .GetFiles(modSys.SaveDir, "*.json")
                .Select(Path.GetFileName)
                .Select(filename => filename.EndsWith(".json") ? filename.Substring(0, filename.Length - 5) : filename)
                .ToArray();
            loadSkinFilenames = none.Concat(files).ToArray();
        }

        public void UpdateSkinLoadFilenamesDropdown()
        {
            var filenamesDropdown = Composers[CHARACTER].GetDropDown("loadskinfilenamesdropdown");
            if (filenamesDropdown != null)
            {
                filenamesDropdown.SetSelectedIndex(0);
                filenamesDropdown.SetList(loadSkinFilenames, loadSkinFilenames);
            }
        }

        public void OnLoadSkinFilenameChange(string variantcode, bool selected)
        {
            loadSkinFilename = variantcode;
        }

        public bool OnLoadSkin()
        {
            if (loadSkinFilename == "") return false; // ignore

            bool result = modSys.SkinLoad(capi, loadSkinFilename);

            // re-compose since most options may have changed
            if (result == true)
            {
                ComposeGuis();
            }
            
            return true;
        }

        public void OnSaveSkinInputChanged(string value)
        {
            saveSkinFilename = value;
        }

        public bool OnSaveSkin()
        {
            bool result = modSys.SkinSave(capi, saveSkinFilename);

            // if save successful, reload load skin filenames dropdown list
            if (result == true)
            {
                GetLocalSkinFilenames();
                UpdateSkinLoadFilenamesDropdown();
            }

            return true;
        }

        public void GetLocalEmblemFilenames()
        {
            string[] none = {""};
            string[] files = Directory
                .GetFiles(modSys.SaveDir, "*.png")
                .Select(Path.GetFileName)
                .Select(filename => filename.EndsWith(".png") ? filename.Substring(0, filename.Length - 4) : filename)
                .ToArray();
            loadEmblemFilenames = none.Concat(files).ToArray();
        }

        public void UpdateEmblemLoadFilenamesDropdown()
        {
            var filenamesDropdown = Composers[CHARACTER].GetDropDown("loademblemfilenamesdropdown");
            if (filenamesDropdown != null)
            {
                filenamesDropdown.SetSelectedIndex(0);
                filenamesDropdown.SetList(loadEmblemFilenames, loadEmblemFilenames);
            }
        }

        public void OnLoadEmblemFilenameChange(string variantcode, bool selected)
        {
            loadEmblemFilename = variantcode;
        }

        public bool OnLoadEmblem()
        {
            if (loadEmblemFilename == "") return false; // ignore

            bool result = modSys.EmblemLoad(capi, loadEmblemFilename);

            // re-compose since most options may have changed
            if (result == true)
            {
                ComposeGuis();
            }
            
            return true;
        }

        public void OnSaveEmblemInputChanged(string value)
        {
            saveEmblemFilename = value;
        }

        public bool OnSaveEmblem()
        {
            bool result = modSys.EmblemSave(capi, saveEmblemFilename);

            // if save successful, reload load skin filenames dropdown list
            if (result == true)
            {
                GetLocalEmblemFilenames();
                UpdateEmblemLoadFilenamesDropdown();
            }

            return true;
        }

        public void OnToggleDressOnOff(bool on)
        {
            charNaked = !on;
            capi.World.Player.Entity.hideClothing = charNaked;
            var essr = capi.World.Player.Entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            essr.TesselateShape();
        }

        public void OnSelectModel(string modelname, bool selected)
        {
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            skin.SetModel(modelname);
            // var essr = capi.World.Player.Entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            // essr.TesselateShape();
            ComposeGuis(); // re-compose, selection options will change
        }

        public void OnSelectBuiltinSkinPreset(string presetCode, bool selected)
        {
            LoadBuiltinSkin = presetCode;
        }

        public bool OnLoadBuiltinSkinPreset()
        {
            if (LoadBuiltinSkin == "") return false; // ignore

            if (modSys.SkinPresetsByCode.TryGetValue(LoadBuiltinSkin, out KemonoSkinnableModelPreset preset))
            {
                var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
                skin.LoadPreset(preset);
                ComposeGuis(); // re-compose, selection options will change
            }

            return true;
        }

        public void OnSetColorSliderModeHSV(bool useHSV)
        {
            if (useHSV != ColorSliderModeHSV)
            {
                ColorSliderModeHSV = useHSV;
                ComposeGuis(); // all sliders will change, need to re-compose
            }
            else
            {
                // no change in mode, just force state in toggle buttons
                if (CharacterPanel != null)
                {
                    CharacterPanel.GetElementAs<GuiElementToggleButton>("color-mode-btn-rgb").SetValue(!ColorSliderModeHSV);
                    CharacterPanel.GetElementAs<GuiElementToggleButton>("color-mode-btn-hsv").SetValue(ColorSliderModeHSV);
                }
            }
        }

        public bool OnPartScaleReset()
        {
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            var appliedScale = skin.GetAppliedScale();
            if (appliedScale == null) return true; // no scale applied
            foreach (var scalePart in skin.Model.ScaleParts)
            {
                appliedScale.RemoveAttribute(scalePart.Code);
            }

            ComposeGuis(); // this will re-tesselate shape
            return true;
        }

        public void OnPartScaleChange(string partCode, double scale)
        {
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            skin.SetModelPartScale(partCode, scale);
        }
        
        public void OnPartColorChange(string partCode, int color)
        {
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            skin.SetSkinPartColor(partCode, color);
        }

        public void OnSelectSkinPart(string partCode, string variantCode)
        {
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            skin.SelectSkinPart(partCode, variantCode);
        }

        public bool OnRandomizeSkin()
        {
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            skin.RandomizeSkinParts();

            var essr = capi.World.Player.Entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            essr.TesselateShape();

            ComposeGuis();

            return true;
        }

        // ====================================================================
        // EMBLEM PAINTING TAB
        // ====================================================================

        public void UpdateRgbaFromHsv()
        {
            // calculate rgb from hsv
            var rgb = KemonoColorUtil.HsvToRgb(this.colHue, this.colSat, this.colVal);
            this.colRed255 = rgb.r;
            this.colGreen255 = rgb.g;
            this.colBlue255 = rgb.b;
            this.colRed = rgb.r / 255.0;
            this.colGreen = rgb.g / 255.0;
            this.colBlue = rgb.b / 255.0;
            this.colRGBA = ColorUtil.ColorFromRgba(colRed255, colGreen255, colBlue255, 255); // r,g,b,a
        }

        public void UpdateHsvFromRgb()
        {
            // get normalized rgb
            this.colRed = this.colRed255 / 255.0;
            this.colGreen = this.colGreen255 / 255.0;
            this.colBlue = this.colBlue255    / 255.0;

            // calculate rgb from hsv
            var hsv = KemonoColorUtil.RgbToHsv(this.colRed, this.colGreen, this.colBlue);
            this.colHue = hsv.h;
            this.colSat = hsv.s;
            this.colVal = hsv.v;
        }
        
        public bool OnPainterHueChange(double newHue)
        {
            if ( newHue == this.colHue ) return false; // no change

            this.colHue = newHue;
            UpdateRgbaFromHsv();

            // set sat/val color selector rectangle
            Composers[CHARACTER].GetColorSatValSelector("satvalselector")?.SetHue(this.colHue);
            
            // set rgb box inputs
            // TODO

            // set canvas brush color
            Composers[CHARACTER].GetCanvas("canvas")?.SetBrushColor(this.colRGBA);

            return true;
        }

        public bool OnPainterSatValChange(double newSat, double newVal)
        {
            if ( newSat == this.colSat && newVal == this.colVal ) return false; // no change

            this.colSat = newSat;
            this.colVal = newVal;
            UpdateRgbaFromHsv();

            // set rgb box inputs
            // TODO

            // set canvas brush color
            Composers[CHARACTER].GetCanvas("canvas")?.SetBrushColor(this.colRGBA);
            
            return true;
        }

        public bool OnCanvasPaint()
        {
            int[] pixels = Composers[CHARACTER].GetCanvas("canvas").GetPixels();
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            skin.SetEmblemPixels(pixels);

            // re-render character
            var essr = capi.World.Player.Entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            essr.TesselateShape();

            return true;
        }

        public bool OnCanvasSelectColor(int color)
        {
            var rgb = KemonoColorUtil.ToRgb(color);
            this.colRed255 = rgb.r;
            this.colGreen255 = rgb.g;
            this.colBlue255 = rgb.b;
            this.colRGBA = ColorUtil.ColorFromRgba(rgb.r, rgb.g, rgb.b, 255); // r,g,b,a
            UpdateHsvFromRgb();

            // set hue/sat/val color selectors
            Composers[CHARACTER].GetColorHueSlider("hueslider")?.SetHue(this.colHue);
            Composers[CHARACTER].GetColorSatValSelector("satvalselector")?.SetHueSatVal(this.colHue, this.colSat, this.colVal);

            return true;
        }

        public bool OnSetCanvasBackgroundColor(bool useBackground, int backgroundColor)
        {
            this.canvasBackgroundColor = backgroundColor;

            switch (canvasBackgroundColor)
            {
                case CANVAS_COLOR_CLEAR:
                    Composers[CHARACTER].GetToggleButton("canvas-background-color-clear").SetValue(true);
                    Composers[CHARACTER].GetToggleButton("canvas-background-color-white").SetValue(false);
                    Composers[CHARACTER].GetToggleButton("canvas-background-color-black").SetValue(false);
                    break;
                case CANVAS_COLOR_WHITE:
                    Composers[CHARACTER].GetToggleButton("canvas-background-color-clear").SetValue(false);
                    Composers[CHARACTER].GetToggleButton("canvas-background-color-white").SetValue(true);
                    Composers[CHARACTER].GetToggleButton("canvas-background-color-black").SetValue(false);
                    break;
                case CANVAS_COLOR_BLACK:
                    Composers[CHARACTER].GetToggleButton("canvas-background-color-clear").SetValue(false);
                    Composers[CHARACTER].GetToggleButton("canvas-background-color-white").SetValue(false);
                    Composers[CHARACTER].GetToggleButton("canvas-background-color-black").SetValue(true);
                    break;
                default: // clear
                    Composers[CHARACTER].GetToggleButton("canvas-background-color-clear").SetValue(true);
                    Composers[CHARACTER].GetToggleButton("canvas-background-color-white").SetValue(false);
                    Composers[CHARACTER].GetToggleButton("canvas-background-color-black").SetValue(false);
                    break;
            }

            Composers[CHARACTER].GetCanvas("canvas").SetBackground(useBackground, backgroundColor);
            
            // set painting cursor color based on background color
            int cursorColor;
            switch (canvasBackgroundColor)
            {
                case CANVAS_COLOR_CLEAR:
                    cursorColor = CANVAS_COLOR_WHITE;
                    break;
                case CANVAS_COLOR_WHITE:
                    cursorColor = CANVAS_COLOR_BLACK;
                    break;
                case CANVAS_COLOR_BLACK:
                    cursorColor = CANVAS_COLOR_WHITE;
                    break;
                default: // clear
                    cursorColor = CANVAS_COLOR_BLACK;
                    break;
            }

            Composers[CHARACTER].GetCanvas("canvas").SetCursorColor(cursorColor);

            return true;
        }

        public bool OnClearEmblem()
        {            
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();

            // clear pixels and re-tessellate
            // I HAVE NO IDEA WHY THIS WORKS (?) LOL LMAO
            skin.ClearEmblemPixels(0, 0, 0, 0);
            skin.RenderEmblemTexture();

            var essr = capi.World.Player.Entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            essr?.TesselateShape();

            // recompose after emblem cleared
            ComposeGuis();

            return true;
        }

        // ====================================================================
        // Race TAB
        // ====================================================================

        void changeRacePaginationPage(int dir)
        {
            if (modSys.Races.Count > 0)
            {
                int pages = 1 + ((modSys.Races.Count - 1) / RACES_PER_PAGE);
                selectedRacePaginationPage = GameMath.Mod(selectedRacePaginationPage + dir, pages);
            }
            else
            {
                selectedRacePaginationPage = 0;
            }

            ComposeGuis();
        }

        void changeRace(int index)
        {
            selectedRaceIndex = GameMath.Mod(index, modSys.Races.Count);
            ComposeGuis();
        }

        void loadRaceDescription(int index)
        {            
            if (index < 0 || index > modSys.Races.Count) return;

            KemonoRace race = modSys.Races[index];

            Composers[CHARACTER].GetRichtext("raceName").SetNewText(Lang.Get("kemono:race-name-" + race.Code), CairoFont.WhiteSmallishText());

            StringBuilder fulldesc = new StringBuilder();
            StringBuilder attributes = new StringBuilder();

            fulldesc.AppendLine(Lang.Get("kemono:race-desc-" + race.Code));
            fulldesc.AppendLine();
            fulldesc.AppendLine(Lang.Get("game:traits-title"));

            var traits = race.Traits.Select(code => modSys.RaceTraitsByCode[code]).OrderBy(trait => (int)trait.Type);

            if (race.Traits.Length > 0)
            {
                foreach (var trait in traits) 
                {
                    attributes.Clear();
                    foreach (var val in trait.Attributes)
                    {
                        if (attributes.Length > 0) attributes.Append(", ");
                        attributes.Append(Lang.Get(string.Format(GlobalConstants.DefaultCultureInfo, "kemono:charattribute-{0}-{1}", val.Key, val.Value)));
                    }

                    if (attributes.Length > 0)
                    {
                        fulldesc.AppendLine(Lang.Get("game:traitwithattributes", Lang.Get("kemono:trait-" + trait.Code), attributes));
                    }
                    else
                    {
                        string desc = Lang.GetIfExists("kemono:traitdesc-" + trait.Code);
                        if (desc != null)
                        {
                            fulldesc.AppendLine(Lang.Get("game:traitwithattributes", Lang.Get("kemono:trait-" + trait.Code), desc));
                        }
                        else
                        {
                            fulldesc.AppendLine(Lang.Get("kemono:trait-" + trait.Code));
                        }
                    }
                }
            }
            else
            {
                fulldesc.AppendLine(Lang.Get("No positive or negative traits"));
            }

            Composers[CHARACTER].GetRichtext("raceDesc").SetNewText(fulldesc.ToString(), CairoFont.WhiteDetailText());
        }

        // ====================================================================
        // CLASS TAB
        // ====================================================================

        void changeClass(int dir)
        {
            selectedClassIndex = GameMath.Mod(selectedClassIndex + dir, modSys.baseCharSys.characterClasses.Count);

            CharacterClass chclass = modSys.baseCharSys.characterClasses[selectedClassIndex];
            Composers[CHARACTER].GetDynamicText("className").SetNewText(Lang.Get("characterclass-" + chclass.Code));

            StringBuilder fulldesc = new StringBuilder();
            StringBuilder attributes = new StringBuilder();

            fulldesc.AppendLine(Lang.Get("characterdesc-" + chclass.Code));
            fulldesc.AppendLine();
            fulldesc.AppendLine(Lang.Get("traits-title"));

            var chartraits = chclass.Traits.Select(code => modSys.baseCharSys.TraitsByCode[code]).OrderBy(trait => (int)trait.Type);

            foreach (var trait in chartraits) 
            {
                attributes.Clear();
                foreach (var val in trait.Attributes)
                {
                    if (attributes.Length > 0) attributes.Append(", ");
                    attributes.Append(Lang.Get(string.Format(GlobalConstants.DefaultCultureInfo, "charattribute-{0}-{1}", val.Key, val.Value)));
                }

                if (attributes.Length > 0)
                {
                    fulldesc.AppendLine(Lang.Get("traitwithattributes", Lang.Get("trait-" + trait.Code), attributes));
                }
                else
                {
                    string desc = Lang.GetIfExists("traitdesc-" + trait.Code);
                    if (desc != null)
                    {
                        fulldesc.AppendLine(Lang.Get("traitwithattributes", Lang.Get("trait-" + trait.Code), desc));
                    }
                    else
                    {
                        fulldesc.AppendLine(Lang.Get("trait-" + trait.Code));
                    }
                }
            }

            if (chclass.Traits.Length == 0)
            {
                fulldesc.AppendLine(Lang.Get("No positive or negative traits"));
            }

            Composers[CHARACTER].GetRichtext("characterDesc").SetNewText(fulldesc.ToString(), CairoFont.WhiteDetailText());

            modSys.baseCharSys.setCharacterClass(capi.World.Player.Entity, chclass.Code, true);

            var essr = capi.World.Player.Entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            essr.TesselateShape();
        }

        
        public void PrepAndOpen()
        {
            GatherDresses(EnumCharacterDressType.Foot);
            GatherDresses(EnumCharacterDressType.Hand);
            GatherDresses(EnumCharacterDressType.Shoulder);
            GatherDresses(EnumCharacterDressType.UpperBody);
            GatherDresses(EnumCharacterDressType.LowerBody);
            TryOpen();
        }

        public void GatherDresses(EnumCharacterDressType type)
        {
            List<ItemStack> dresses = new List<ItemStack>();
            dresses.Add(null);

            string stringtype = type.ToString().ToLowerInvariant();

            IList<Item> items = capi.World.Items;

            for (int i = 0; i < items.Count; i++)
            {
                Item item = items[i];
                if (item == null || item.Code == null || item.Attributes == null) continue;

                string clothcat = item.Attributes["clothescategory"]?.AsString();
                bool allow = item.Attributes["inCharacterCreationDialog"]?.AsBool() == true;

                if (allow && clothcat?.ToLowerInvariant() == stringtype)
                {
                    dresses.Add(new ItemStack(item));
                }
            }

            DressesByDressType[type] = dresses.ToArray();
            DressPositionByTressType[type] = 0;
        }


        public override bool CaptureAllInputs()
        {
            return IsOpened();
        }


        public override string ToggleKeyCombinationCode
        {
            get { return null; }
        }
        

        public override void OnMouseWheel(MouseWheelEventArgs args)
        {
            if (charRenderBounds.PointInside(capi.Input.MouseX, capi.Input.MouseY) && (currTab == TAB_CHARACTER || currTab == TAB_EMBLEM || currTab == TAB_DEBUG_RENDER))
            {
                charZoom = GameMath.Clamp(charZoom + args.deltaPrecise / 10f, 0.5f, 1f);
                args.SetHandled(true); // prevents further scroll event handling
            }
            base.OnMouseWheel(args);
        }

        public override bool PrefersUngrabbedMouse => true;


        #region Character render 
        protected float yaw = -GameMath.PIHALF + 0.3f;
        protected bool rotateCharacter;
        public override void OnMouseDown(MouseEvent args)
        {
            base.OnMouseDown(args);

            rotateCharacter = charRenderBounds.PointInside(args.X, args.Y);
        }

        public override void OnMouseUp(MouseEvent args)
        {
            base.OnMouseUp(args);

            rotateCharacter = false;
        }

        public override void OnMouseMove(MouseEvent args)
        {
            base.OnMouseMove(args);

            if (rotateCharacter) yaw += args.DeltaX / 100f;
        }


        Vec4f lightPos = new Vec4f(-1, -1, 0, 0).NormalizeXYZ();
        Matrixf mat = new Matrixf();

        public override void OnRenderGUI(float deltaTime)
        {
            base.OnRenderGUI(deltaTime);

            if (capi.IsGamePaused)
            {
                capi.World.Player.Entity.talkUtil.OnGameTick(deltaTime);
            }
            
            capi.Render.GlPushMatrix();

            // charRenderScaleX is setup to flip -X axis
            // because default RenderEntityToGui is inverted relative to game rendering
            capi.Render.GlScale(charRenderScaleX, 1f, 1f);
            capi.Render.GlRotate(charRenderRotateX, 1, 0, 0);
            capi.Render.GlTranslate(charRenderOffsetX, charRenderOffsetY, charRenderOffsetZ);

            mat.Identity();
            mat.RotateXDeg(-14);
            Vec4f lightRot = mat.TransformVector(lightPos);
            double pad = GuiElement.scaled(GuiElementItemSlotGridBase.unscaledSlotPadding);

            capi.Render.CurrentActiveShader.Uniform("lightPosition", new Vec3f(lightRot.X, lightRot.Y, lightRot.Z));

            double renderZoom;
            double renderYOffset;
            if (currTab == TAB_CHARACTER)
            {
                renderZoom = charZoom;
                renderYOffset = -220;
            }
            else if (currTab == TAB_EMBLEM)
            {
                renderZoom = charZoom;
                renderYOffset = -170;
            }
            else if (currTab == TAB_DEBUG_RENDER)
            {
                renderZoom = charZoom;
                renderYOffset = -170;
            }
            else
            {
                renderZoom = 0.5;
                renderYOffset = -170;
            }

            // get character model specific y render height offset
            var skin = capi.World.Player.Entity.GetBehavior<EntityBehaviorKemonoSkinnable>();
            if (skin != null)
            {
                renderYOffset += skin.Model.GuiRenderHeightOffset;
            }

            capi.Render.PushScissor(charRenderBounds);

            // render character into left inset
            capi.Render.RenderEntityToGui(
                deltaTime,
                capi.World.Player.Entity,
                -charRenderBounds.renderX - pad - GuiElement.scaled(460) * renderZoom - GuiElement.scaled(115 * (1-renderZoom)), // pos x
                charRenderBounds.renderY + pad + GuiElement.scaled(renderYOffset), // pos y (doesn't matter?)
                // charRenderBounds.renderY + pad + GuiElement.scaled(10 * (1 - renderZoom)), // pos y
                (float) GuiElement.scaled(1000), // pos z (must be large otherwise object clips with far plane)
                yaw, // yaw delta
                (float) GuiElement.scaled(330 * renderZoom), // size
                ColorUtil.WhiteArgb); // color
            
            capi.Render.PopScissor();

            capi.Render.CurrentActiveShader.Uniform("lightPosition", new Vec3f(1, -1, 0).Normalize());

            capi.Render.GlPopMatrix();
        }
        #endregion


        public override float ZSize
        {
            get { return (float)GuiElement.scaled(280); }
        }
    }
}
