using HarmonyLib;
using Vintagestory.API.Client;


namespace kemono
{
    // https://github.com/anegostudios/vsapi/blob/e78624b6eee6920e45edcd25ba94e8199b2193af/Client/Render/PerceptionEffects/DrunkPerceptionEffect.cs#L60
    // this is crashing my client on 1.19.0-pre.5
    // so im removing it until i can inspect source cause
    [HarmonyPatch(typeof(DrunkPerceptionEffect))]
    [HarmonyPatch("ApplyToTpPlayer")]
    class PatchDrunkPerceptionEffectApplyToTpPlayer
    {
        static bool Prefix() {
            return false;
        }
    }

}
