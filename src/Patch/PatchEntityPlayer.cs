using HarmonyLib;
using Vintagestory.API.Common;


namespace kemono
{
    // https://github.com/anegostudios/vsapi/blob/e78624b6eee6920e45edcd25ba94e8199b2193af/Common/Entity/EntityPlayer.cs#L392
    // need to overwrite the AnimManager.HeadController created after
    // base.OnTesselation call
    [HarmonyPatch(typeof(EntityPlayer))]
    [HarmonyPatch("OnTesselation")]
    class PatchEntityPlayerOnTesselation
    {
        static void Postfix(EntityPlayer __instance, ref Shape entityShape) {
            var skin = __instance.GetBehavior<EntityBehaviorKemonoSkinnable>();
            if (skin != null && skin.Model != null)
            {
                __instance.AnimManager.HeadController = new KemonoPlayerHeadController(
                    __instance.AnimManager,
                    __instance,
                    entityShape,
                    skin.Model.JointHead,
                    skin.Model.JointNeck,
                    skin.Model.JointTorsoUpper,
                    skin.Model.JointTorsoLower,
                    skin.Model.JointLegUpperL,
                    skin.Model.JointLegUpperR
                );
            }
        }
    }

}