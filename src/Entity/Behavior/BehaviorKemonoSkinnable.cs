/**
Implements customizable entity shape, skin, voice, etc.

Implementation notes:

Two main functions:
1. OnReloadSkin: 
    Reloads all customizable textures, writes textures to atlas.
2. OnTesselation:
    Reload skin parts, rebuilds entity shape using `AddSkinPart(...)`,
    and reloads animations if needed.

TODO:
- For optimization, since string codes constant, we can assign index to
  each part and use Array accesses instead of Dictionary accesses.
- For each texture target, store a CPU side pixels buffer and write
  to buffer instead of writing to atlas. Only do final write to atlas
  once per texture target. Maintain a "write stack" ordering for
  parts that write to same target, e.g. base. If body changes,
  then all overlaid textures (horn, eyes, etc.) also need to 
  rewrite to buffer.
*/

using Newtonsoft.Json;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.API.Util;

namespace kemono
{
    public enum EnumKemonoSkinnableType
    {
        Shape,
        Texture,
        Voice
    }

    /// Skinnable part in a model, e.g. hair, eyes, etc.
    [JsonObject(MemberSerialization.OptOut)]
    public class KemonoSkinnablePart
    {
        /// <summary>
        /// Part identifier, e.g. "hair", "eye", etc.
        /// </summary>
        public string Code;

        /// <summary>
        /// Part render order, lower numbers render first in model.
        /// Purpose of render order is to enable a modder to add a new
        /// model addon part and control its render order relative to
        /// default built-in parts.
        /// </summary>
        public int RenderOrder;

        /// <summary>
        /// Part type, e.g. shape, texture, voice, etc.
        /// </summary>
        public EnumKemonoSkinnableType Type;

        public string[] DisableElements;

        public CompositeShape ShapeTemplate;

        // available variants (models, textures, etc.)
        public KemonoSkinnablePartVariant[] Variants;

        // shape texture path
        public AssetLocation Texture;
        // texture render uv coordinates (x, y)
        public Vec2i TextureRenderTo = new Vec2i();
        // name of the texture target in the model, e.g. "hair"
        public string TextureTarget;
        // width of texture target atlas location
        // needed for allocating atlas texture space
        public int TextureTargetWidth;
        // height of texture target atlas location
        // needed for allocating atlas texture space
        public int TextureTargetHeight;
        // render paintable emblem texture instead of regular texture filepath
        public bool UseEmblemTexture = false;
        // flag to texture blend overlay
        // during initialization, if texture target is first in the render
        // order, this will be disabled (no blend)
        public bool TextureBlendOverlay = true;

        // gui layout configuration
        public bool UseDropDown = true;
        public bool UseColorSlider = false;
        public bool UseTransformSlider = false;

        // when player has any of this list of clothes or armor on,
        // use skinpart's alt model codes
        public EnumCharacterDressType[] AltClothedRequirement = {};
        public EnumCharacterDressType[] AltArmoredRequirement = {};

        // variant code => variant (same objects as in Variants)
        // (must be initialized after object created)
        [JsonIgnore]
        public Dictionary<string, KemonoSkinnablePartVariant> LoadedVariantsByCode = null;

        // lazy accessor for skin part code => skin part
        // creates lookup dictionary when first called
        [JsonIgnore]
        public Dictionary<string, KemonoSkinnablePartVariant> VariantsByCode
        {
            get
            {
                if (LoadedVariantsByCode == null)
                {
                    LoadedVariantsByCode = new Dictionary<string, KemonoSkinnablePartVariant>();
                    foreach (var variant in Variants)
                    {
                        LoadedVariantsByCode[variant.Code] = variant;
                    }
                }
                return LoadedVariantsByCode;
            }
        }
    }

    /// Variant of a skinnable part, e.g. hair types.
    /// TODO: move color out of here, it should be in applied part only
    public class KemonoSkinnablePartVariant
    {
        // identifier
        public string Code;
        // alt variant template code when model clothed
        public string AltClothed;
        // alt variant template code when model armored
        public string AltArmored;
        // path to shape file, for shape attachments
        public AssetLocation Shape;
        // path to alt clothed shape file, for shape attachments
        public AssetLocation ShapeClothed;
        // path to alt clothed shape file, for shape attachments
        public AssetLocation ShapeArmored;
        // path to texture file, for texture overlays
        public AssetLocation Texture;
        // path to sound file, for voices
        public AssetLocation Sound;
        // flag to skip this variant when rendering, for empty parts
        public bool Skip = false;
    }

    /// Applied variant of a skinnable part, contains final values after
    /// building the variants and mixing with other properties.
    public class AppliedKemonoSkinnablePartVariant
    {
        // part code string
        public string PartCode;
        // variant identifier
        public string Code;
        // alt variant code when model clothed
        public string AltClothed;
        // alt variant code when model armored
        public string AltArmored;
        // path to shape file, for shape attachments
        public AssetLocation Shape;
        // path to alt clothed shape file, for shape attachments
        public AssetLocation ShapeClothed;
        // path to alt clothed shape file, for shape attachments
        public AssetLocation ShapeArmored;
        // path to texture file, for texture overlays
        public AssetLocation Texture;
        // path to sound file, for voices
        public AssetLocation Sound;
        // flag to skip this variant when rendering, for empty parts
        public bool Skip;
        // applied color on top of variant texture
        public int Color;
        // applied glow value on part, only applied if > 0
        public int Glow;
        // loaded texture pixels
        public BitmapSimple TextureBitmap = null;
    }

    /// <summary>
    /// Wrapper around skin part's ITreeAttribute tree to implement
    /// IReadOnlyDictionary interface for KemonoRace.IsValid().
    /// </summary>
    public struct SkinPartsTreeDictionary : IReadOnlyDictionary<string, string>
    {
        public ITreeAttribute tree;

        public SkinPartsTreeDictionary(ITreeAttribute tree)
        {
            this.tree = tree;
        }

        public string this[string key] => tree.GetString(key);

        public IEnumerable<string> Keys => tree.Select(v => v.Key);

        public IEnumerable<string> Values => tree.Values.Select(v => (v as StringAttribute).value);

        public int Count => tree.Count;

        public bool ContainsKey(string key)
        {
            return tree.HasAttribute(key);
        }

        public bool TryGetValue(string key, out string value)
        {
            if (tree.HasAttribute(key))
            {
                value = tree.GetString(key);
                return true;
            }
            else
            {
                value = null;
                return false;
            }
        }
        
        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            foreach (var val in tree)
            {
                yield return new KeyValuePair<string, string>(val.Key, (val.Value as StringAttribute).value);
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    /// Override properties regular animation meta data:
    /// https://apidocs.vintagestory.at/api/Vintagestory.API.Common.AnimationMetaData.html
    public class KemonoAnimationOverride
    {
        public string Code;
        public string Animation;
        public float? AnimationSpeed;
        public EnumAnimationBlendMode? BlendMode;
        public Dictionary<string, EnumAnimationBlendMode> ElementBlendMode;
        public Dictionary<string, float> ElementWeight;

        public AnimationMetaData ToAnimationMeta()
        {
            return new AnimationMetaData
            {
                Code = Code,
                Animation = Animation,
                AnimationSpeed = AnimationSpeed ?? 1,
                BlendMode = BlendMode ?? EnumAnimationBlendMode.Add,
                ElementBlendMode = ElementBlendMode ?? new Dictionary<string, EnumAnimationBlendMode>(),
                ElementWeight = ElementWeight ?? new Dictionary<string, float>()
            };
        }
    }

    /// <summary>
    /// Defines a config for scaling a base model shape element part.
    /// Typically used to define min/max scaling range for a bone,
    /// which will scale all shape element children of the bone.
    /// </summary>
    public class KemonoModelScale
    {
        // name of this scaling part, e.g. "head"
        // we don't want to use bone name because bone name can be weird
        // and because we need this code for storing in entity tree,
        // lang lookup, etc.
        public string Code;
        // target shape name, e.g. "b_Head"
        public string Target;
        // min scale
        public double Min;
        // max scale
        public double Max;
        // whether this is the "main" scale of a model, e.g. the root
        // bone is the head of the model tree and scales everything.
        // this is used to decide if scale should affect model hitbox
        // and player eyeheight
        public bool IsMain = false;
    }

    /// A model shape and its skinnable parts.
    /// TODO: store model types per entity type, use EntityTypes attributes
    /// https://apidocs.vintagestory.at/api/Vintagestory.API.Common.IWorldAccessor.html#Vintagestory_API_Common_IWorldAccessor_EntityTypes
    [JsonObject(MemberSerialization.OptOut)]
    public class KemonoSkinnableModel
    {
        // model base name, e.g. "kemono"
        // should be unique, as models are accessed by code
        public string Code;
        
        // addon name, equal to model code it is addon for.
        // e.g. if this is "kemono0" it appends skinparts into the
        // "kemono0" model.
        public string Addon;

        // shapes folder path
        public AssetLocation ShapePath;

        // skin part textures folder path
        public AssetLocation TexturePath;

        // main model shape json file path
        public AssetLocation Model;

        // joint names used for head controller
        public string JointHead = "b_Head";
        public string JointNeck = "b_Neck";
        public string JointTorsoUpper = "b_TorsoUpper";
        public string JointTorsoLower = "b_TorsoLower";
        public string JointLegUpperL = "b_FootUpperL";
        public string JointLegUpperR = "b_FootUpperR";

        // flat list of all available model scaling parts
        public List<KemonoModelScale> ScaleParts = new List<KemonoModelScale>();

        // flat list of all available skin parts
        public List<KemonoSkinnablePart> SkinParts = new List<KemonoSkinnablePart>();

        // list of all animation overrides
        public List<KemonoAnimationOverride> AnimationOverrides = new List<KemonoAnimationOverride>();
        
        // character creation gui layout
        public List<string[]> GuiLayout = new List<string[]>();

        // gui character rendering y height offset
        // (different models different heights, use this to center vertically)
        public double GuiRenderHeightOffset = 0; 

        // eye height override (default seraph is 1.7)
        public double EyeHeight = 1.7;

        // hitbox size override
        public Vec2f HitBoxSize = new Vec2f(0.6f, 1.85f);

        // list of all shape paths, includes addon shape paths
        [JsonIgnore]
        public List<AssetLocation> ShapePaths = new List<AssetLocation>();

        // list of all texture paths, includes addon texture paths
        [JsonIgnore]
        public List<AssetLocation> TexturePaths = new List<AssetLocation>();

        // scale part target => scale part (NOT PART CODE)
        // this is used to lookup from element to scale part when walking
        // shape tree to apply scaling
        [JsonIgnore]
        public Dictionary<string, KemonoModelScale> ScalePartsByTarget;

        // skin parts sorted by part.RenderOrder value (low to high)
        [JsonIgnore]
        public List<KemonoSkinnablePart> SkinPartsByRenderOrder;

        // skin part code => skin part (same objects as in SkinParts list)
        // (must be initialized after object created)
        [JsonIgnore]
        public Dictionary<string, KemonoSkinnablePart> SkinPartsByCode;

        // set of all texture targets in this model, gathered from
        // skin parts TextureTarget properties
        // (must be initialized after object created)
        [JsonIgnore]
        public HashSet<string> TextureTargets;

        /// <summary>
        /// Post creation initialization, called by mod system.
        /// Returns self so this can be chained after new instance creation. <summary>
        /// </summary>
        /// <returns></returns>        
        public KemonoSkinnableModel Initialize()
        {
            // add self shape and texture paths
            ShapePaths.Add(ShapePath);
            TexturePaths.Add(TexturePath);

            // map scale parts target => scale part
            ScalePartsByTarget = new Dictionary<string, KemonoModelScale>();
            foreach (var part in ScaleParts)
            {
                ScalePartsByTarget[part.Target] = part;
            }

            // map skin part code => skin part
            SkinPartsByCode = new Dictionary<string, KemonoSkinnablePart>();
            foreach (var part in SkinParts)
            {
                SkinPartsByCode[part.Code] = part;
            }

            // sort skin parts by render order
            SkinPartsByRenderOrder = SkinParts
                .Where(p => p.Type != EnumKemonoSkinnableType.Voice)
                .OrderBy(p => p.RenderOrder)
                .ToList();

            // pre-load all texture targets
            TextureTargets = new HashSet<string>();
            
            foreach (var part in SkinPartsByRenderOrder)
            {
                if (part.TextureTarget != null)
                {
                    if (TextureTargets.Add(part.TextureTarget))
                    {
                        // if true, this is first skin part using this texture target
                        // for this first part, we want to render into atlas
                        // without blending so that it acts as base texture
                        part.TextureBlendOverlay = false;
                    }
                }
            }

            return this;
        }

        /// <summary>
        /// Apply another model as an "addon". Append another model's
        /// skin parts, animation overrides, and gui layout into this model.
        /// `Initialize()` needs to be called again after this.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public void ApplyAddon(KemonoSkinnableModel other)
        {
            // main model:
            // if other part model not null, overwrite this model
            if (other.Model != null)
            {
                Model = other.Model;
            }
            
            if (other.ShapePath != null)
            {
                ShapePaths.Add(other.ShapePath);
            }
            
            if (other.TexturePath != null)
            {
                TexturePaths.Add(other.TexturePath);
            }

            // scale parts:
            // directly append
            ScaleParts.AddRange(other.ScaleParts);

            // skinparts:
            // if part code already exists, append variants into existing part
            // otherwise, add new part
            foreach (var otherPart in other.SkinParts)
            {
                int idx = SkinParts.FindIndex(p => p.Code == otherPart.Code);
                if (idx != -1) // append variants
                {
                    var existingPart = SkinParts[idx];
                    existingPart.Variants = existingPart.Variants.Concat(otherPart.Variants).ToArray();
                }
                else // add new part
                {
                    SkinParts.Add(otherPart);
                }
            }

            // animation overrides:
            // if animation code already exists, replace existing
            // otherwise, add new
            foreach (var anim in other.AnimationOverrides)
            {
                int idx = AnimationOverrides.FindIndex(a => a.Code == anim.Code);
                if (idx != -1) // replace existing
                {
                    AnimationOverrides[idx] = anim;
                }
                else // add new
                {
                    AnimationOverrides.Add(anim);
                }
            }

            // append gui layout directly
            GuiLayout.AddRange(other.GuiLayout);
        }
    }

    /// Preset configuration for a skinnable model.
    [JsonObject(MemberSerialization.OptOut)]
    public class KemonoSkinnableModelPreset
    {
        // model name, e.g. "kemono0" or "horse0"
        public string Model { get; init; }
        
        // part code => selected variant
        public Dictionary<string, string> Parts { get; init; }
        
        // part code => selected colors in RGBA
        public Dictionary<string, KemonoColorRGB> Colors { get; init; }
    }

    public class EntityBehaviorKemonoSkinnable : EntityBehavior
    {
        // skin tree keys for applied skinnable config
        // NOTE: vanilla uses "appliedParts", use "kemo___" prefix to
        // avoid conflicts with vanilla character config
        public const string APPLIED_MODEL = "kemonoAppliedModel";
        public const string APPLIED_PARTS = "kemonoAppliedParts";
        public const string APPLIED_COLORS = "kemonoAppliedColors";
        public const string APPLIED_SCALE = "kemonoAppliedScale";
        public const string APPLIED_GLOW = "kemonoAppliedGlow";
        public const string EMBLEM = "kemonoEmblem";

        // emblem texture size
        public const int EMBLEM_SIZE = 32; // 32x32 pixels

        // global settings
        public string VoiceType = "altoflute";
        public string VoicePitch = "medium";
        
        // currently selected model (points to same object in mod system's AvailableModels)
        public KemonoSkinnableModel Model;

        // flag that model was changed and is dirty
        // signals OnTesselation to rebuild joints and animation cache
        public bool ClientModelChanged = true;

        // current selected race code string
        public string RaceCode;

        // entity tree attribute for selected skin parts
        public ITreeAttribute skintree { get; private set; }

        // re-used textures for emblem
        public LoadedTexture EmblemTexture = null;
        public LoadedTexture EmblemClearTexture = null; // TODO: can share this for all entities

        // DIRTY FLAGS
        // flag that emblem dirty and needs to be re-rendered
        public bool DirtyEmblem = true;
        // flags that skin part texture or color is dirty
        public HashSet<string> DirtyPartTexture = new HashSet<string>();

        // internal list of applied parts
        public List<AppliedKemonoSkinnablePartVariant> appliedTemp = new List<AppliedKemonoSkinnablePartVariant>();

        // public access to applied parts
        public IReadOnlyList<AppliedKemonoSkinnablePartVariant> AppliedSkinParts
        {
            get
            {
                appliedTemp.Clear();

                ITreeAttribute appliedTree = skintree.GetTreeAttribute(APPLIED_PARTS);
                ITreeAttribute appliedColors = skintree.GetTreeAttribute(APPLIED_COLORS);
                ITreeAttribute appliedGlow = skintree.GetTreeAttribute(APPLIED_GLOW);
                if (appliedTree == null) return appliedTemp;
                
                if (Model == null) return appliedTemp;

                // parts are layered by their render order (from low to high)
                foreach (var part in Model.SkinPartsByRenderOrder)
                {
                    if (part.Variants.Length == 0) continue;

                    string variantCode = appliedTree.GetString(part.Code);
                    if (variantCode == null) continue;

                    if (part.VariantsByCode.TryGetValue(variantCode, out KemonoSkinnablePartVariant variant))
                    {
                        // if skinnable part type is a model and has a texture,
                        // set the applied texture to base part's texture
                        var appliedTexture = variant.Texture;
                        if (part.Type == EnumKemonoSkinnableType.Shape && part.Texture != null)
                        {
                            appliedTexture = part.Texture;
                        }

                        // set variant color and glow if it exists in appliedColors tree
                        int color = appliedColors?.TryGetInt(part.Code) ?? 0;
                        int glow = appliedGlow?.TryGetInt(part.Code) ?? -1;

                        var applied = new AppliedKemonoSkinnablePartVariant {
                            PartCode = part.Code,
                            Code = variant.Code,
                            AltClothed = variant.AltClothed,
                            AltArmored = variant.AltArmored,
                            Shape = variant.Shape,
                            ShapeClothed = variant.ShapeClothed,
                            ShapeArmored = variant.ShapeArmored,
                            Texture = appliedTexture,
                            Sound = variant.Sound,
                            Skip = variant.Skip,
                            Color = color,
                            Glow = glow
                        };

                        appliedTemp.Add(applied);
                    }
                }

                return appliedTemp;
            }
        }


        public EntityBehaviorKemonoSkinnable(Entity entity) : base(entity)
        {

        }

        public override string PropertyName()
        {
            return "kemonoskinnable";
        }

        public override void Initialize(EntityProperties properties, JsonObject attributes)
        {
            base.Initialize(properties, attributes);

            skintree = entity.WatchedAttributes.GetTreeAttribute("skinConfig");
            if (skintree == null)
            {
                entity.WatchedAttributes["skinConfig"] = skintree = new TreeAttribute();
            }

            entity.WatchedAttributes.RegisterModifiedListener("characterRace", OnRaceChanged);
            entity.WatchedAttributes.RegisterModifiedListener("skinConfig", OnSkinConfigChanged);
            entity.WatchedAttributes.RegisterModifiedListener("voicetype", OnVoiceConfigChanged);
            entity.WatchedAttributes.RegisterModifiedListener("voicepitch", OnVoiceConfigChanged);

            KemonoMod kemo = entity.Api.ModLoader.GetModSystem<KemonoMod>();

            // initial race code
            RaceCode = entity.WatchedAttributes.GetString("characterRace", kemo.DefaultRaceCode);

            // select initial model
            string initialModelName = skintree.GetString(APPLIED_MODEL, kemo.DefaultModelCode);
            if (!kemo.AvailableModels.TryGetValue(initialModelName, out KemonoSkinnableModel modelType))
            {
                modelType = kemo.AvailableModels[kemo.DefaultModelCode];
            }
            Model = modelType;

            // create initial skin parts tree
            InitializeSkinParts();

            OnVoiceConfigChanged();
        }

        /// Makes sure all skin parts have some initial variant selected.
        /// Runs on server and client.
        public void InitializeSkinParts()
        {
            ITreeAttribute appliedTree = skintree.GetTreeAttribute(APPLIED_PARTS);
            ITreeAttribute appliedColors = skintree.GetTreeAttribute(APPLIED_COLORS);

            // initialize applied part tree if it doesn't exist
            if (appliedTree == null) skintree[APPLIED_PARTS] = appliedTree = new TreeAttribute();
            if (appliedColors == null) skintree[APPLIED_COLORS] = appliedColors = new TreeAttribute();

            if (Model == null)
            {
                entity.Api.World.Logger.Error($"[InitializeSkinParts] Model is null");
                return;
            }

            // remove any applied parts that are no longer available
            List<string> toRemove = new List<string>();
            string[] treeNames = { "appliedParts", "appliedColors" };
            ITreeAttribute[] trees = { appliedTree, appliedColors };
            for (int i = 0; i < trees.Length; i++)
            {
                string treeName = treeNames[i];
                ITreeAttribute tree = trees[i];

                foreach (var val in tree)
                {
                    // remove if key not in available parts
                    if (!Model.SkinPartsByCode.TryGetValue(val.Key, out KemonoSkinnablePart part))
                    {
                        toRemove.Add(val.Key);
                        continue;
                    }

                    // if string attribute (e.g. model or texture name)
                    // remove if not in list of available variants
                    if (val.Value is StringAttribute)
                    {
                        if (part.Variants.Length == 0 || !part.VariantsByCode.ContainsKey((val.Value as StringAttribute).value))
                        {
                            toRemove.Add(val.Key);
                            continue;
                        }
                    }
                }

                if (toRemove.Count > 0)
                {
                    entity.Api.Logger.Warning($"[InitializeSkinParts] Removing {toRemove.Count} invalid attributes from {treeName}:");

                    foreach (var partCode in toRemove)
                    {
                        entity.Api.Logger.Warning($"[InitializeSkinParts] - {partCode}");
                        tree.RemoveAttribute(partCode);
                    }

                    toRemove.Clear();
                }
            }

            // create initial applied part for all available parts
            foreach (var part in Model.SkinParts)
            {
                // voice done somewhere else
                if (part.Type == EnumKemonoSkinnableType.Voice) continue;

                string partCode = part.Code;

                // initial variant
                if (part.Variants.Length > 0)
                {
                    if (!appliedTree.HasAttribute(partCode))
                    {
                        appliedTree[partCode] = new StringAttribute(part.Variants[0].Code);
                    }
                }
                
                // initial random color
                if (!appliedColors.HasAttribute(partCode))
                {
                    appliedColors[partCode] = new IntAttribute(ColorUtil.ColorFromRgba(
                        entity.Api.World.Rand.Next(255),
                        entity.Api.World.Rand.Next(255),
                        entity.Api.World.Rand.Next(255),
                        255
                    ));
                }

                // mark skin part texture dirty
                DirtyPartTexture.Add(partCode);
            }
        }

        /// Randomize all skin parts and colors.
        /// This does not reload skin or re-tesselate the shape, the caller
        /// must make sure to properly re-render the entity.
        ///
        /// TODO: in future, make skin part config decide if it should randomize
        /// or force select a specific variant.
        public void RandomizeSkinParts()
        {
            ITreeAttribute appliedTree = skintree.GetTreeAttribute(APPLIED_PARTS);
            ITreeAttribute appliedColors = skintree.GetTreeAttribute(APPLIED_COLORS);

            // initialize applied part trees if do not exist
            if (appliedTree == null) skintree[APPLIED_PARTS] = appliedTree = new TreeAttribute();
            if (appliedColors == null) skintree[APPLIED_COLORS] = appliedColors = new TreeAttribute();

            if (Model == null)
            {
                entity.Api.World.Logger.Error($"[RandomizeSkinParts] Model is null");
                return;
            }

            foreach (var part in Model.SkinParts)
            {
                // do voice elsewhere
                if (part.Type == EnumKemonoSkinnableType.Voice) continue;

                string partCode = part.Code;

                // random variant
                if (part.Variants.Length > 1)
                {
                    int idx = entity.Api.World.Rand.Next(part.Variants.Length);
                    appliedTree[partCode] = new StringAttribute(part.Variants[idx].Code);
                }
                else if (part.Variants.Length == 1)
                {
                    appliedTree[partCode] = new StringAttribute(part.Variants[0].Code);
                }

                // random part color
                if (part.UseColorSlider)
                {
                    appliedColors[partCode] = new IntAttribute(ColorUtil.ColorFromRgba(
                        entity.Api.World.Rand.Next(255),
                        entity.Api.World.Rand.Next(255),
                        entity.Api.World.Rand.Next(255),
                        255
                    ));
                }

                // mark skin part texture dirty
                DirtyPartTexture.Add(partCode);
            }
        }

        /// <summary>
        /// Set a skin model, e.g. "kemono0", "horse0", etc. This initializes
        /// the skin parts and colors. Force required in cases when models
        /// are reloaded and currently stored model object is stale.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="force"></param>
        public void SetModel(string code, bool force = false)
        {
            if (force == false && Model != null && code == Model.Code) return; // ignore if same model

            KemonoMod kemo = entity.Api.ModLoader.GetModSystem<KemonoMod>();

            if (kemo.AvailableModels.TryGetValue(code, out KemonoSkinnableModel newModel))
            {
                Model = newModel;
                skintree[APPLIED_MODEL] = new StringAttribute(code);
                if (entity.World.Side == EnumAppSide.Client)
                {
                    ClientModelChanged = true;
                    var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
                    essr.MarkShapeModified();

                    // free texture allocations
                    FreeEmblemTextures();
                    essr.FreeAllTextureTargets();
                }

                // re-initialize skin parts
                InitializeSkinParts();
            }
        }

        public SkinPartsTreeDictionary GetSkinPartsTreeDictionary()
        {
            ITreeAttribute appliedTree = skintree?.GetTreeAttribute(APPLIED_PARTS);
            if (appliedTree != null)
            {
                return new SkinPartsTreeDictionary(appliedTree);
            }
            else
            {
                return new SkinPartsTreeDictionary(appliedTree);
            }
        }

        /// <summary>
        /// Client handler when character race changes on server.
        /// </summary>
        public void OnRaceChanged()
        {
            if (entity.World.Side != EnumAppSide.Client) return;

            KemonoMod kemo = entity.Api.ModLoader.GetModSystem<KemonoMod>();
            string newRaceCode = entity.WatchedAttributes.GetString("characterRace", kemo.DefaultRaceCode);
            if (newRaceCode != RaceCode)
            {
                RaceCode = newRaceCode;
                kemo.SetEntityRaceFromCode(entity, newRaceCode, true);
            }
        }

        /// Handler when skin config changes from external client.
        /// Mark all parts dirty and re-render.
        public void OnSkinConfigChanged()
        {
            skintree = entity.WatchedAttributes["skinConfig"] as ITreeAttribute;

            if (entity.World.Side == EnumAppSide.Client)
            {
                // model change
                string modelCode = skintree.GetString(APPLIED_MODEL);
                if (modelCode != null)
                {
                    SetModel(modelCode);
                }

                // mark all parts dirty
                foreach (var part in Model.SkinParts)
                {
                    DirtyPartTexture.Add(part.Code);
                }

                var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
                essr.MarkShapeModified();
            }
        }

        public void OnVoiceConfigChanged()
        {
            VoiceType = entity.WatchedAttributes.GetString("voicetype");
            VoicePitch = entity.WatchedAttributes.GetString("voicepitch");

            ApplyVoice(VoiceType, VoicePitch, false);
        }

        /// Get skintree applied scale tree, return null if it does not exist.
        public ITreeAttribute GetAppliedScale()
        {
            return skintree?.GetTreeAttribute(APPLIED_SCALE);
        }

        /// Get skintree applied color tree, return null if it does not exist.
        public ITreeAttribute GetAppliedColors()
        {
            return skintree?.GetTreeAttribute(APPLIED_COLORS);
        }

        /// Try to get skinnable part's applied color in applied color tree,
        /// or return null if not found.
        public int? TryGetAppliedColor(string partName)
        {
            return skintree?.GetTreeAttribute(APPLIED_COLORS)?.TryGetInt(partName);
        }

        /// Get skinnable custom drawn "emblem" texture pixels.
        /// (Used for horse cutie mark). Creates default empty 32x32 flat
        /// int array if no pixels found in skin tree.
        public int[] GetEmblemPixels()
        {
            if (skintree == null) return new int[EMBLEM_SIZE * EMBLEM_SIZE];

            byte[] emblemPixels = skintree.GetBytes(EMBLEM, null);
            if (emblemPixels != null)
            {
                // convert to int[]
                int[] pixels = new int[emblemPixels.Length / 4];
                Buffer.BlockCopy(emblemPixels, 0, pixels, 0, emblemPixels.Length);
                return pixels;
            }
            else
            {
                int[] pixels = new int[EMBLEM_SIZE * EMBLEM_SIZE];

                //// TEMPORARY: put some pattern in so we can tell texture is there
                // for (int i = 0; i < pixels.Length; i += 2)
                // {
                //     pixels[i] = ColorUtil.ColorFromRgba(255, 0, 0, 255);
                // }

                return pixels;
            }
        }

        /// Get emblem pixels as a SkiaSharp bitmap, used for easy saving.
        public SKBitmap GetEmblemBitmap()
        {
            int[] pixels = GetEmblemPixels();

            int width = EMBLEM_SIZE;
            int height = EMBLEM_SIZE;
            SKBitmap bitmap = new SKBitmap(width, height);

            for (int y = 0; y < height; y += 1)
            {
                for (int x = 0; x < width; x += 1)
                {
                    int pixel = pixels[y * width + x];
                    var (r, g, b, a) = KemonoColorUtil.ToRgba(pixel);
                    bitmap.SetPixel(x, y, new SKColor((byte) r, (byte) g, (byte) b, (byte) a));
                }
            }

            return bitmap;
        }

        /// Set custom drawable "emblem" texture pixels.
        /// (Used for horse cutie mark).
        public void SetEmblemPixels(int[] pixels)
        {
            if (skintree == null) return;

            byte[] emblemBytes = skintree.GetBytes(EMBLEM, null);
            if (emblemBytes == null)
            {
                emblemBytes = new byte[4 * EMBLEM_SIZE * EMBLEM_SIZE];
            }
            
            // write pixels to bytes, save into skin tree
            Buffer.BlockCopy(pixels, 0, emblemBytes, 0, emblemBytes.Length);
            skintree.SetBytes(EMBLEM, emblemBytes);

            DirtyEmblem = true;
        }

        /// Clear all pixels in the custom drawn "emblem" texture.
        /// TODO: currently render to texture atlas fucked, alpha test 
        /// does not work? so temporary solution is to re-rendering
        /// close-to-zero alpha multiple times to clear.
        /// Alpha parameter is that alpha value. 
        public void ClearEmblemPixels(int r, int g, int b, int alpha = 0)
        {
            if (skintree == null) return;
            
            int[] clearPixels = new int[EMBLEM_SIZE * EMBLEM_SIZE];
            int clearColor = ColorUtil.ColorFromRgba(r, g, b, alpha);

            // put clear pixels with alpha input
            for (int i = 0; i < clearPixels.Length; i += 1)
            {
                clearPixels[i] = clearColor;
            }

            byte[] clearBytes = new byte[4 * EMBLEM_SIZE * EMBLEM_SIZE];
            Buffer.BlockCopy(clearPixels, 0, clearBytes, 0, clearBytes.Length);
            skintree.SetBytes(EMBLEM, clearBytes);
        }

        /// Returns standard pixels used for clearing emblem texture. 
        public static int[] GetEmblemClearPixels()
        {
            int[] clearPixels = new int[EMBLEM_SIZE * EMBLEM_SIZE];
            int clearColor = ColorUtil.ColorFromRgba(255, 255, 255, 0);

            // put clear pixels with alpha input
            for (int i = 0; i < clearPixels.Length; i += 1)
            {
                clearPixels[i] = clearColor;
            }

            return clearPixels;
        }


        /// <summary>
        /// Set emblem pixels from a texture asset location. Texture must
        /// be 32x32 pixels. Return result with string error if fails.
        /// Path must be FULL TEXTURE PATH WITH DOMAIN, e.g.
        /// "kemono:textures/emblem/vinylscratch.png".
        /// </summary>
        /// <param name="texPath"></param>
        public Result<bool, string> SetEmblemFromTexture(AssetLocation texPath)
        {
            var api = entity.Api;

            IAsset asset = api.Assets.TryGet(texPath);
            if (asset == null)
            {
                return Lang.Get("kemono:msg-emblem-load-error-no-file-exist", texPath);
            }

            // convert texPath into full image path for loading
            string imgPath = asset.Origin.OriginPath + Path.DirectorySeparatorChar + texPath.Path;
            
            return SetEmblemFromImagePath(imgPath);
        }
        
        /// <summary>
        /// Set emblem pixels from a image path. Image must be a 32x32 pixels.
        /// Return result with string error if fails.
        /// </summary>
        /// <param name="texPath"></param>
        public Result<bool, string> SetEmblemFromImagePath(string imgPath)
        {
            // check if exists
            if (!File.Exists(imgPath))
            {
                return Lang.Get("kemono:msg-emblem-load-error-no-file-exist", imgPath);
            }

            // load png into pixels
            var bitmap = SKBitmap.Decode(imgPath);
            if (bitmap == null) return false;
            
            int emblemSize = EMBLEM_SIZE;
            if (bitmap.Width != emblemSize || bitmap.Height != emblemSize)
            {
                return Lang.Get("kemono:msg-emblem-load-error-size-wrong", emblemSize, emblemSize);
            }

            var skPixels = bitmap.Pixels;
            int[] pixels = new int[emblemSize * emblemSize];

            // make sure same length, note: error here should never occur
            if (skPixels.Length != pixels.Length)
            {
                return Lang.Get("kemono:msg-emblem-load-error-size-wrong", emblemSize, emblemSize);
            }

            // copy pixels, then write to skin
            // idk why but its loading in BGRA format :^(
            // so need to swap blue <-> red
            for (int i = 0; i < pixels.Length; i++)
            {
                var px = skPixels[i];
                pixels[i] = ColorUtil.ToRgba(px.Alpha, px.Blue, px.Green, px.Red);
            }

            SetEmblemPixels(pixels);

            return true;
        }

        public static int[] EmblemClearPixels = GetEmblemClearPixels();

        /// Get emblem loaded texture, used for rendering into texture atlas.
        public LoadedTexture GetEmblemTexture()
        {
            ICoreClientAPI capi = entity.World.Api as ICoreClientAPI;

            bool linearMag = false; // use nearest rendering
            int clampMode = 0;      // clamp mode

            // lazily create texture when needed
            if (EmblemTexture == null)
            {
                EmblemTexture = new LoadedTexture(capi)
                {
                    Width = EMBLEM_SIZE,
                    Height = EMBLEM_SIZE
                };
            }

            capi.Render.LoadOrUpdateTextureFromRgba(
                GetEmblemPixels(),
                linearMag,
                clampMode,
                ref EmblemTexture
            );

            return EmblemTexture;
        }

        /// Get emblem clear texture, used for rendering into texture atlas.
        public LoadedTexture GetEmblemClearTexture()
        {
            // lazily create texture when needed
            if (EmblemClearTexture == null)
            {
                ICoreClientAPI capi = entity.World.Api as ICoreClientAPI;

                bool linearMag = false; // use nearest rendering
                int clampMode = 0;      // clamp mode

                EmblemClearTexture = new LoadedTexture(capi)
                {
                    Width = EMBLEM_SIZE,
                    Height = EMBLEM_SIZE
                };

                capi.Render.LoadOrUpdateTextureFromRgba(
                    EmblemClearPixels,
                    linearMag,
                    clampMode,
                    ref EmblemClearTexture
                );
            }

            return EmblemClearTexture;
        }

        public void FreeEmblemTextures()
        {
            EmblemTexture?.Dispose();
            EmblemTexture = null;
            EmblemClearTexture?.Dispose();
            EmblemClearTexture = null;
        }

        /// Force re-render emblem textures into atlas locations for all
        /// parts that use emblem textures.
        public void RenderEmblemTexture()
        {
            ICoreClientAPI capi = entity.World.Api as ICoreClientAPI;
            var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            var appliedParts = AppliedSkinParts;

            foreach (var applied in appliedParts)
            {
                KemonoSkinnablePart part;
                Model.SkinPartsByCode.TryGetValue(applied.PartCode, out part);
                
                if (part.UseEmblemTexture && part.TextureTarget != null)
                {
                    KemonoTextureAtlasAllocation texAtlasTarget = essr.GetOrAllocateTextureTarget(
                        part.TextureTarget,
                        part.TextureTargetWidth,
                        part.TextureTargetHeight
                    );

                    renderEmblemToAtlas(
                        capi,
                        part,
                        texAtlasTarget.TexPos
                    );
                }
            }
        }

        /// Apply the preset config to this behavior.
        public void LoadPreset(KemonoSkinnableModelPreset preset)
        {
            KemonoMod kemo = entity.Api.ModLoader.GetModSystem<KemonoMod>();

            if (!kemo.AvailableModels.TryGetValue(preset.Model, out KemonoSkinnableModel newModel))
            {
                entity.Api.World.Logger.Error($"[kemono] LoadPreset {preset.Model} not found");
                return;
            }

            SetModel(preset.Model);

            // load applied variants and colors
            ITreeAttribute appliedPartsTree = skintree.GetTreeAttribute(APPLIED_PARTS);
            ITreeAttribute appliedColorsTree = skintree.GetTreeAttribute(APPLIED_COLORS);

            bool shapeModified = false; // flag that shape modified, need to retesselate

            foreach (var part in preset.Parts)
            {
                if (Model.SkinPartsByCode.TryGetValue(part.Key, out KemonoSkinnablePart skinpart))
                {
                    if (skinpart.VariantsByCode.ContainsKey(part.Value))
                    {
                        appliedPartsTree[part.Key] = new StringAttribute(part.Value);

                        if (skinpart.Type == EnumKemonoSkinnableType.Shape)
                        {
                            shapeModified = true;
                        }

                        // mark part dirty
                        DirtyPartTexture.Add(part.Key);
                    }
                }
            }

            foreach (var color in preset.Colors)
            {
                if (Model.SkinPartsByCode.ContainsKey(color.Key))
                {
                    var rgb = color.Value;
                    appliedColorsTree[color.Key] = new IntAttribute(ColorUtil.ColorFromRgba(
                        rgb.r,
                        rgb.g,
                        rgb.b,
                        255
                    ));

                    // mark part dirty
                    DirtyPartTexture.Add(color.Key);
                }
            }

            if (entity.World.Side == EnumAppSide.Client && shapeModified)
            {
                var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
                essr.MarkShapeModified();
            }
        }

        /// Convert behavior's current applied skin to a preset.
        /// Return preset object. External caller should do file save
        /// to json.
        public KemonoSkinnableModelPreset SavePreset()
        {
            var appliedParts = new Dictionary<string, string>();
            var appliedColors = new Dictionary<string, KemonoColorRGB>();

            if (Model != null)
            {
                ITreeAttribute appliedPartsTree = skintree.GetTreeAttribute(APPLIED_PARTS);
                ITreeAttribute appliedColorsTree = skintree.GetTreeAttribute(APPLIED_COLORS);

                // render order defined in config ensures correct layering
                foreach (var part in Model.SkinPartsByRenderOrder)
                {
                    // save part variant selection
                    if (part.Variants.Length > 0)
                    {
                        string variantCode = appliedPartsTree.GetString(part.Code);
                        if (variantCode != null && part.VariantsByCode.ContainsKey(variantCode))
                        {
                            appliedParts[part.Code] = variantCode;
                        }
                    }

                    // save part color selection
                    int? color = appliedColorsTree?.TryGetInt(part.Code);
                    if (color != null)
                    {
                        appliedColors[part.Code] = new KemonoColorRGB((int) color);
                    }
                }
            }

            return new KemonoSkinnableModelPreset
            {
                Model = Model.Code,
                Parts = appliedParts,
                Colors = appliedColors
            };
        }

        public override void OnEntityLoaded()
        {
            base.OnEntityLoaded();
            init();
        }

        public override void OnEntitySpawn()
        {
            base.OnEntitySpawn();
            init();
        }

        public override void OnEntityDespawn(EntityDespawnData despawn)
        {
            base.OnEntityDespawn(despawn);

            var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            if (essr != null)
            {
                essr.OnReloadSkin -= OnReloadSkin;
                essr.OnTesselation -= OnTesselation;
                essr.FreeAllTextureTargets();
            }

            // free local textures allocated
            FreeEmblemTextures();
        }

        bool didInit = false;
        public void init()
        {
            if (entity.World.Side != EnumAppSide.Client) return;

            if (!didInit)
            {
                var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
                if (essr == null) throw new InvalidOperationException("The extra skinnable requires the entity to use the KemonoSkinnableShape renderer.");

                essr.OnReloadSkin += OnReloadSkin;
                essr.OnTesselation += OnTesselation;
                didInit = true;
            }
        }

        /// TODO:
        /// - Do texture colorization + merging in cpu space, try to do only
        ///   one write to texture atlas/gpu memory
        /// - Add masking for texture parts. Issue right now: have to add
        ///   a special white texture for eyes because base skin colorization
        ///   is colorizing eye white part. Adding masks would remove need
        ///   for special textures just to overwrite unnecessary changes
        ///   in base texture.
        public void OnReloadSkin(LoadedTexture atlas, TextureAtlasPosition baseTexPos)
        {
            ICoreClientAPI capi = entity.World.Api as ICoreClientAPI;
            var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;

            // render emblems if dirty
            bool renderEmblems = DirtyEmblem;
            DirtyEmblem = false;

            foreach (var dirty in DirtyPartTexture)
            {
                Debug.WriteLine($"[OnReloadSkin] DIRTY: {dirty}");
            }

            foreach (var applied in AppliedSkinParts)
            {
                if (applied.Skip || applied.Texture == null) continue;

                KemonoSkinnablePart part = Model.SkinPartsByCode[applied.PartCode];
                if (part.TextureTarget == null) continue;

                bool partTextureDirty = DirtyPartTexture.Remove(part.Code);
                partTextureDirty |= part.UseEmblemTexture && renderEmblems;
                Debug.WriteLine($"[OnReloadSkin] {part.Code} {partTextureDirty} {part.Type} {part.TextureTarget} {applied.Texture} {baseTexPos}");

                // only render if:
                // 1. part texture dirty
                // 2. texture is base texture: always render, because other
                //    systems render overlays onto base. gets too messy
                //    synchronizing dirty flag with other systems, so always
                //    render base texture for safety.
                if (!partTextureDirty && part.TextureTarget != EntityKemonoSkinnableShapeRenderer.MAIN_TEXTURE) continue;

                // get texture target, if not the base texture (e.g. "seraph")
                // get or allocate texture atlas location for texture target
                // (allocation is unique to this entity behavior)
                KemonoTextureAtlasAllocation texAtlasTarget = essr.GetOrAllocateTextureTarget(
                    part.TextureTarget,
                    part.TextureTargetWidth,
                    part.TextureTargetHeight
                );

                Debug.WriteLine($"[OnReloadSkin] try rendering texture {part.TextureTarget} {applied.Texture} {texAtlasTarget.TexPos}");

                // render emblem into target atlas allocation
                // TODO: avoid double rendering emblem into same target
                // e.g. add flag to only render once, then use some set
                // to mark already rendered targets
                if (part.UseEmblemTexture)
                {
                    renderEmblemToAtlas(
                        capi,
                        part,
                        texAtlasTarget.TexPos
                    );
                }
                // default: load part texture asset, render into atlas allocation
                else
                {
                    var assetPath = applied.Texture.Clone().WithPathPrefixOnce("textures/").WithPathAppendixOnce(".png");

                    var texture = new LoadedTexture(capi);
                    capi.Render.GetOrLoadTexture(assetPath, ref texture);

                    IAsset skinAsset = capi.Assets.TryGet(assetPath);
                    if (skinAsset == null)
                    {
                        // try kemono domain
                        assetPath.Domain = "kemono";
                        skinAsset = capi.Assets.TryGet(assetPath);
                    }

                    if (skinAsset != null)
                    {
                        Debug.WriteLine($"[OnReloadSkin] rendering assetPath={assetPath} to {part.TextureRenderTo} tex size = {texture.Width}, {texture.Height})");

                        BitmapRef bmp = skinAsset.ToBitmap(capi);

                        renderColorizedTexture(
                            capi,
                            part,
                            applied,
                            bmp,
                            texture,
                            texAtlasTarget.TexPos
                        );
                    }
                    else {
                        capi.World.Logger.Error($"[OnReloadSkin] Failed loading texture: {applied.Texture} for part {part.Code}");
                    }
                }
            }
        }

        public void SetModelPartScale(string partCode, double scale, bool retesselateShape = true)
        {
            ITreeAttribute appliedTree = skintree.GetTreeAttribute(APPLIED_SCALE);
            if (appliedTree == null) skintree[APPLIED_SCALE] = appliedTree = new TreeAttribute();
            appliedTree[partCode] = new DoubleAttribute(scale);

            if (retesselateShape)
            {
                var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
                essr?.TesselateShape();
            }

            return;
        }

        public void SelectSkinPart(string partCode, string variantCode, bool retesselateShape = true, bool playVoice = true)
        {
            Model.SkinPartsByCode.TryGetValue(partCode, out var part);

            ITreeAttribute appliedTree = skintree.GetTreeAttribute(APPLIED_PARTS);
            if (appliedTree == null) skintree[APPLIED_PARTS] = appliedTree = new TreeAttribute();
            appliedTree[partCode] = new StringAttribute(variantCode);

            if (part?.Type == EnumKemonoSkinnableType.Voice)
            {
                entity.WatchedAttributes.SetString(partCode, variantCode);

                if (partCode == "voicetype")
                {
                    VoiceType = variantCode;
                }
                if (partCode == "voicepitch")
                {
                    VoicePitch = variantCode;
                }

                ApplyVoice(VoiceType, VoicePitch, playVoice);
                return;
            }

            // for now, just always mark texture dirty, easier
            // TODO: decide when necessary to re-render texture
            DirtyPartTexture.Add(partCode);

            if (retesselateShape)
            {
                var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
                essr?.TesselateShape();
            }
            
            return;
        }

        public void SetSkinPartColor(string partCode, int color, bool retesselateShape = true)
        {
            ITreeAttribute appliedTree = skintree.GetTreeAttribute(APPLIED_COLORS);
            if (appliedTree == null) skintree[APPLIED_COLORS] = appliedTree = new TreeAttribute();
            appliedTree[partCode] = new IntAttribute(color);

            // mark texture dirty
            DirtyPartTexture.Add(partCode);

            if (retesselateShape)
            {
                var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
                essr?.TesselateShape();
            }

            return;
        }

        public void SetSkinPartGlow(string partCode, int glow, bool retesselateShape = true)
        {
            ITreeAttribute appliedTree = skintree.GetTreeAttribute(APPLIED_GLOW);
            if (appliedTree == null) skintree[APPLIED_GLOW] = appliedTree = new TreeAttribute();
            appliedTree[partCode] = new IntAttribute(glow);

            if (retesselateShape)
            {
                var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
                essr?.TesselateShape();
            }

            return;
        }


        public void ApplyVoice(string voiceType, string voicePitch, bool testTalk)
        {
            if (!Model.SkinPartsByCode.TryGetValue("voicetype", out var availVoices) || !Model.SkinPartsByCode.TryGetValue("voicepitch", out var availPitches))
            {
                return;
            }

            VoiceType = voiceType;
            VoicePitch = voicePitch;

            if (entity is EntityPlayer plr && plr.talkUtil != null && voiceType != null) {

                if (!availVoices.VariantsByCode.ContainsKey(voiceType))
                {
                    voiceType = availVoices.Variants[0].Code;
                }

                plr.talkUtil.soundName = availVoices.VariantsByCode[voiceType].Sound;
                
                float pitchMod = 1;
                switch (VoicePitch)
                {
                    case "verylow": pitchMod = 0.6f; break;
                    case "low": pitchMod = 0.8f; break;
                    case "medium": pitchMod = 1f; break;
                    case "high": pitchMod = 1.2f; break;
                    case "veryhigh": pitchMod = 1.4f; break;
                }

                plr.talkUtil.pitchModifier = pitchMod;
                plr.talkUtil.chordDelayMul = 1.1f;

                if (testTalk)
                {
                    plr.talkUtil.Talk(EnumTalkType.Idle);
                }
            }
        }

        /// Force reload shapes and textures from disk, then re-tesselate
        /// character shape.
        /// If reloadSkinparts is true, also reload skin part shapes.
        /// If reloadClothing is true, also reload clothing shapes.
        /// If reloadTextures is true, also reload textures.
        /// Returns (num part shapes, num clothing shapes, num textures) reloaded.
        public (int, int, int) ReloadAssets(
            bool reloadSkinparts = false,
            bool reloadClothing = false,
            bool reloadTextures = false
        ) {
            int numSkinpartsReloaded = 0;
            int numClothingReloaded = 0;
            int numTexturesReloaded = 0;

            // reload all model shape paths (addons can add additional paths)
            foreach (var shapePath in Model.ShapePaths)
            {
                // reload main model shape
                AssetLocation mainShapeFolder = shapePath.Clone().WithPathAppendixOnce("main/");
                entity.Api.Assets.Reload(mainShapeFolder);

                if (reloadSkinparts)
                {
                    AssetLocation skinPartsFolder = shapePath.Clone().WithPathAppendixOnce("skinparts/");
                    numSkinpartsReloaded += entity.Api.Assets.Reload(skinPartsFolder);
                }

                if (reloadClothing)
                {
                    AssetLocation clothingFolder = shapePath.Clone().WithPathAppendixOnce("clothing/");
                    numClothingReloaded += entity.Api.Assets.Reload(clothingFolder);
                }
            }

            if (reloadTextures)
            {
                // reload all texture paths (addons can add additional paths)
                foreach (var texturePath in Model.TexturePaths)
                {
                    numTexturesReloaded += entity.Api.Assets.Reload(texturePath);
                }
            }

            // clear entity animation cache
            ClearAnimationCache(AnimCacheKey());

            // force animation reload
            ClientModelChanged = true;

            // force re-tesselate
            var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;
            essr?.TesselateShape();

            return (numSkinpartsReloaded, numClothingReloaded, numTexturesReloaded);
        }


        /// <summary>
        /// Event handler for entity renderer OnTesselation event. Constructs
        /// new kemono customized shape and replaces default entity shape.
        /// This is where all shape related aspects are handled: skin parts
        /// are attached to the base shape, skin part scaling, animation cache
        /// is reloaded and replaced.
        /// </summary>
        /// <param name="entityShape"></param>
        /// <param name="shapePathForLogging"></param>
        public void OnTesselation(ref Shape entityShape, string shapePathForLogging)
        {
            /// debug
            // Debug.WriteLine($"[OnTesselation] old shape JointsById:");
            // DebugPrintJoints(entityShape);
            // Debug.WriteLine($"[OnTesselation] old shape AttachmentPointsByCode:");
            // DebugPrintAttachmentPoints(entityShape);
            // Debug.WriteLine($"[OnTesselation] old shape textures count {entityShape.Textures.Count}");
            // DebugPrintTextures(entityShape);

            if (Model == null)
            {
                entity.Api.World.Logger.Error("[OnTesselation] Model is null");
                return; // no model selected yet
            }

            // var shapePath = Model.Model.CopyWithPath("shapes/" + Model.Model + ".json");
            var shapePath = Model.Model.Clone().WithPathPrefixOnce("shapes/").WithPathAppendixOnce(".json");
            Debug.WriteLine($"[OnTesselation] Model shape {shapePath}");

            Shape newShape = Shape.TryGet(entity.Api, shapePath).Clone();
            Debug.WriteLine($"[OnTesselation] newShape={newShape}");
            entityShape = newShape;
            entity.Properties.Client.LoadedShapeForEntity = newShape;
            Debug.WriteLine($"[OnTesselation] entity.Properties.Client.LoadedShapeForEntity={entity.Properties.Client.LoadedShapeForEntity}");
            
            // main body scale (used for eye height and hitbox)
            double mainScale = 1.0;

            // apply customizable model part scaling
            ITreeAttribute appliedScale = skintree?.GetTreeAttribute(APPLIED_SCALE);
            if (appliedScale != null && appliedScale.Count > 0)
            {
                // find main body scaling
                foreach (var scalePart in Model.ScaleParts)
                {
                    if (scalePart.IsMain)
                    {
                        double? partScale = appliedScale.TryGetDouble(scalePart.Code);
                        if (partScale != null)
                        {
                            mainScale = (double) partScale;
                        }
                        break;
                    }
                }

                // walk shape tree and apply body part scaling
                // (do it here before adding skin parts to minimize elements need to walk,
                // with caveat can only scale parts in base shape)
                void ProcessShapeElementScale(ShapeElement elem)
                {
                    if (Model.ScalePartsByTarget.TryGetValue(elem.Name, out KemonoModelScale scaling))
                    {
                        double? partScale = appliedScale.TryGetDouble(scaling.Code);
                        if (partScale != null)
                        {
                            double scale = (double) partScale;
                            elem.ScaleX = scale;
                            elem.ScaleY = scale;
                            elem.ScaleZ = scale;
                        }
                    }

                    // walk children
                    if (elem.Children != null)
                    {
                        foreach (var child in elem.Children)
                        {
                            ProcessShapeElementScale(child);
                        }
                    }
                }

                foreach (var elem in entityShape.Elements)
                {
                    ProcessShapeElementScale(elem);
                }
            }
            
            // apply eye height
            entity.Properties.EyeHeight = Model.EyeHeight * mainScale;

            // apply hitbox size
            entity.SetCollisionBox(Model.HitBoxSize.X * (float) mainScale, Model.HitBoxSize.Y * (float) mainScale);

            // apply skin parts

            var appliedParts = AppliedSkinParts;

            foreach (var applied in appliedParts)
            {
                KemonoSkinnablePart part;
                Model.SkinPartsByCode.TryGetValue(applied.PartCode, out part);
                
                if (part?.Type == EnumKemonoSkinnableType.Shape)
                {
                    if (applied.Skip) {
                        continue;
                    }

                    Debug.WriteLine($"[OnTesselation] ADD SKIN PART {applied.PartCode}");
                    entityShape = AddSkinPart(part, applied, entityShape, part.DisableElements, shapePathForLogging);
                }
            }

            var inv = (entity as EntityAgent).GearInventory;
            if (inv != null)
            {
                foreach (var slot in inv)
                {
                    if (slot.Empty) continue;

                    if ((entity as EntityAgent).hideClothing)
                    {
                        continue;
                    }

                    ItemStack stack = slot.Itemstack;
                    JsonObject attrObj = stack.Collectible.Attributes;

                    string[] disableElements = attrObj?["disableElements"]?.AsArray<string>(null);
                    if (disableElements != null)
                    {
                        foreach (var val in disableElements)
                        {
                            entityShape.RemoveElementByName(val);
                        }
                    }
                }
            }

            // new model animation initialization
            if (ClientModelChanged)
            {
                ClientModelChanged = false;
                Debug.WriteLine($"[OnTesselation] ClientModelChanged reloading joints and animation cache");
                
                // apply entity animation overrides
                foreach (var animOverride in Model.AnimationOverrides)
                {
                    if (entity.Properties.Client.AnimationsByMetaCode.TryGetValue(animOverride.Code, out var anim))
                    {
                        if (animOverride.Animation != null)
                        {
                            anim.Animation = animOverride.Animation;
                        }
                        if (animOverride.AnimationSpeed != null)
                        {
                            anim.AnimationSpeed = (float) animOverride.AnimationSpeed;
                        }
                        if (animOverride.BlendMode != null)
                        {
                            anim.BlendMode = (EnumAnimationBlendMode) animOverride.BlendMode;
                        }
                        if (animOverride.ElementBlendMode?.Count > 0)
                        {
                            anim.ElementBlendMode = new Dictionary<string, EnumAnimationBlendMode>(animOverride.ElementBlendMode);
                        }
                        if (animOverride.ElementWeight?.Count > 0)
                        {
                            anim.ElementWeight = new Dictionary<string, float>(animOverride.ElementWeight);
                        }
                    }
                    else
                    {
                        // create new animation (if code defined)
                        if (animOverride.Code != null)
                        {
                            var animMeta = animOverride.ToAnimationMeta().Init();
                            entity.Properties.Client.AnimationsByMetaCode[animMeta.Code] = animMeta;
                            entity.Properties.Client.AnimationsByCrc32[animMeta.CodeCrc32] = animMeta;
                        }
                        else
                        {
                            entity.Api.Logger.Error($"[OnTesselation] Animation override missing code: {animOverride.Animation}");
                        }
                    }
                }

                // rebuild joints (bones), which re-calculates matrix world
                // transforms for entire model tree 
                entityShape.ResolveReferences(entity.Api.World.Logger, Model.Model.ToString());
                CacheInvTransforms(entityShape.Elements);

                // rebuild animation cache
                // https://github.com/anegostudios/vsapi/blob/master/Common/Model/Animation/AnimationCache.cs
                string animCacheKey = AnimCacheKey();
                Debug.WriteLine($"[OnTesselation] animCacheKey={animCacheKey}");
                
                // entityShape.ResolveAndLoadJoints("head"); // deprecated in 1.19
                entityShape.ResolveAndFindJoints(entity.Api.World.Logger, animCacheKey, "head");

                entity.AnimManager = UpdateAnimations(
                    entity.Api,
                    entity.AnimManager,
                    entity,
                    newShape,
                    animCacheKey,
                    "head"
                );

                // NOTE: head controller modified in PatchEntityPlayer.cs
                // because EntityPlayer modifes head controller after
                // OnTesselation call, so we have to replace it afterwards 
                // if (entity is EntityPlayer)
                // {
                //     Debug.WriteLine($"Replacing head controller");
                //     string head = "b_Head";
                //     string neck = "b_Neck";
                //     string torsoUpper = "b_TorsoUpper";
                //     string torsoLower = "b_TorsoLower";
                //     string footUpperL = "b_FootUpperL";
                //     string footUpperR = "b_FootUpperR";
                    
                //     entity.AnimManager.HeadController = new KemonoPlayerHeadController(
                //         entity.AnimManager,
                //         entity as EntityPlayer,
                //         entityShape,
                //         head,
                //         neck,
                //         torsoUpper,
                //         torsoLower,
                //         footUpperL,
                //         footUpperR
                //     );
                // }
            }
            else
            {
                // just reload joints for skin parts shapes
                CacheInvTransforms(entityShape.Elements);
                // entityShape.ResolveAndLoadJoints("head"); // deprecated in 1.19
                entityShape.ResolveAndFindJoints(entity.Api.World.Logger, AnimCacheKey(), "head");
            }

            /// debug
            // Debug.WriteLine($"[OnTesselation] NEW shape joints:");
            // DebugPrintJoints(entityShape);
            // Debug.WriteLine($"[OnTesselation] NEW shape attachment points:");
            // DebugPrintAttachmentPoints(entityShape);
            // Debug.WriteLine($"[OnTesselation] NEW shape textures count {entityShape.Textures.Count}");
            // DebugPrintTextures(entityShape);
        }

        public void DebugPrintJoints(Shape shape)
        {
            foreach (var joint in shape.JointsById)
            {
                Console.WriteLine($"- {joint.Key}: {joint.Value.JointId} {joint.Value.Element.Name} {joint.Value.Element.inverseModelTransform}");
            }
        }

        public void DebugPrintTextures(Shape shape)
        {
            foreach (var val in shape.Textures)
            {
                Console.WriteLine($"- {val.Key} {val.Value}");
            }
        }

        public void DebugPrintAttachmentPoints(Shape shape)
        {
            foreach (var attachpoint in shape.AttachmentPointsByCode)
            {
                Console.WriteLine($"- {attachpoint.Key}: {attachpoint.Value.Code} {attachpoint.Value.ParentElement.Name}");
            }
        }

        public void DebugPrintTextureKeys()
        {
            var textures = entity.Properties.Client.Textures;

            Console.WriteLine("entity.Properties.Client.Textures:");
            foreach (var tex in textures)
            {
                Console.WriteLine($"- textures[{tex.Key}] = {tex.Value}");
            }
        }

        public void CacheInvTransforms(ShapeElement[] elements)
        {
            if (elements == null) return;

            for (int i = 0; i < elements.Length; i++)
            {
                elements[i].CacheInverseTransformMatrix();
                CacheInvTransforms(elements[i].Children);
            }
        }

        public Shape AddSkinPart(
            KemonoSkinnablePart part,
            AppliedKemonoSkinnablePartVariant applied,
            Shape entityShape,
            string[] disableElements,
            string shapePathForLogging
        ) {
            if (Model.SkinPartsByCode[applied.PartCode].Type == EnumKemonoSkinnableType.Voice)
            {
                entity.WatchedAttributes.SetString("voicetype", applied.Code);
                return entityShape;
            }

            var essr = entity.Properties.Client.Renderer as EntityKemonoSkinnableShapeRenderer;

            if (disableElements != null)
            {
                foreach (var val in disableElements)
                {
                    entityShape.RemoveElementByName(val);
                }
            }

            // alternate model when character is clothed or armored
            // ordering is
            //   1. altArmored
            //   2. altClothed
            //   3. default
            bool useAltClothed = false;
            bool useAltArmored = false;
            var eagent = entity as EntityAgent;
            var gearInv = eagent.GearInventory;
            if (gearInv != null && !eagent.hideClothing)
            {
                foreach (var slot in part.AltClothedRequirement)
                {
                    int slotid = (int) slot;
                    ItemStack stack = gearInv[slotid]?.Itemstack;
                    if (stack != null && stack.Item.FirstTexture != null) // check item texture is valid
                    {
                        useAltClothed = true;
                        break;
                    }
                }

                foreach (var slot in part.AltArmoredRequirement)
                {
                    int slotid = (int) slot;
                    ItemStack stack = gearInv[slotid]?.Itemstack;
                    if (stack != null && stack.Item.FirstTexture != null) // check item texture is valid
                    {
                        useAltArmored = true;
                        break;
                    }
                }
            }

            ICoreClientAPI api = entity.World.Api as ICoreClientAPI;
            CompositeShape tmpl = Model.SkinPartsByCode[applied.PartCode].ShapeTemplate;
            AssetLocation shapePath;

            if (applied.Shape == null && tmpl != null)
            {
                shapePath = tmpl.Base.Clone().WithPathPrefixOnce("shapes/").WithPathAppendixOnce(".json");
                if (useAltArmored && applied.AltArmored != null)
                {
                    shapePath.Path = shapePath.Path.Replace("{code}", applied.AltArmored);
                }
                else if (useAltClothed && applied.AltClothed != null)
                {
                    shapePath.Path = shapePath.Path.Replace("{code}", applied.AltClothed);
                }
                else
                {
                    shapePath.Path = shapePath.Path.Replace("{code}", applied.Code);
                }
            }
            else
            {
                if (useAltArmored && applied.ShapeArmored != null)
                {
                    shapePath = applied.ShapeArmored.Clone().WithPathPrefixOnce("shapes/").WithPathAppendixOnce(".json");
                }
                else if (useAltClothed && applied.ShapeClothed != null)
                {
                    shapePath = applied.ShapeClothed.Clone().WithPathPrefixOnce("shapes/").WithPathAppendixOnce(".json");
                }
                else
                {
                    shapePath = applied.Shape.Clone().WithPathPrefixOnce("shapes/").WithPathAppendixOnce(".json");
                }
            }


            Shape partShape = Shape.TryGet(api, shapePath);
            if (partShape == null)
            {
                api.World.Logger.Warning("Entity skin shape {0} defined in entity config {1} not found or errored, was supposed to be at {2}. Skin part will be invisible.", shapePath, entity.Properties.Code, shapePath);
                return entityShape;
            }

            // recursive processing on each shape element faces
            void ProcessShapeElementFaces(ShapeElement elem, int glow)
            {
                // apply face glow to element tree
                foreach (var face in elem.FacesResolved)
                {
                    if (face != null)
                    {
                        face.Glow = glow;
                    }
                }

                // walk children
                if (elem.Children != null)
                {
                    foreach (var child in elem.Children)
                    {
                        ProcessShapeElementFaces(child, glow);
                    }
                }
            }

            bool added = false;
            foreach (var elem in partShape.Elements)
            {
                // attach top level elements to step parent element
                ShapeElement parentElem;

                if (elem.StepParentName != null)
                {
                    parentElem = entityShape.GetElementByName(elem.StepParentName, StringComparison.InvariantCultureIgnoreCase);
                    if (parentElem == null)
                    {
                        api.World.Logger.Warning("Skin part shape {0} defined in entity config {1} requires step parent element with name {2}, but no such element was found in shape {3}. Will not be visible.", shapePath, entity.Properties.Code, elem.StepParentName, shapePathForLogging);
                        continue;
                    }
                }
                else
                {
                    api.World.Logger.Warning("Skin part shape element {0} in shape {1} defined in entity config {2} did not define a step parent element. Will not be visible.", elem.Name, shapePath, entity.Properties.Code);
                    continue;
                }

                if (parentElem.Children == null)
                {
                    parentElem.Children = new ShapeElement[] { elem };
                }
                else
                {
                    parentElem.Children = parentElem.Children.Append(elem);
                }

                elem.SetJointIdRecursive(parentElem.JointId);

                added = true;

                // recursively apply face glow to element tree
                // (or other face processing in future)
                if (applied.Glow > 0)
                {
                    ProcessShapeElementFaces(elem, applied.Glow);
                }
            }

            Debug.WriteLine($"[AddSkinPart] added skin part {applied.PartCode} {applied.Code} {added} {partShape.Textures}");
            
            if (added && partShape.Textures != null)
            {
                foreach (var tex in partShape.Textures)
                {
                    Debug.WriteLine($"[AddSkinPart] loading texture for skin part {applied.PartCode} {tex.Key} {tex.Value}");
                    
                    // for global texture targets defined in model, must
                    // pre-allocate texture target so that texture id can
                    // be assigned to shape. only allocates if the texture
                    // key is the same as part texture target. this allows
                    // parts to reference texture targets defined in other
                    // parts (e.g. hairextra part using "hair" texture)
                    // without allocating here.
                    if (Model.TextureTargets.Contains(tex.Key))
                    {
                        if (tex.Key == part.TextureTarget)
                        {
                            essr.GetOrAllocateTextureTarget(
                                part.TextureTarget,
                                part.TextureTargetWidth,
                                part.TextureTargetHeight
                            );
                        }
                    }
                    else // need to load custom texture specific to model
                    {
                        int[] partTextureSizes = partShape.TextureSizes.Get(tex.Key);
                        int partTextureWidth = partTextureSizes != null ? partTextureSizes[0] : partShape.TextureWidth;
                        int partTextureHeight = partTextureSizes != null ? partTextureSizes[1] : partShape.TextureHeight;
                        
                        loadTexture(
                            applied,
                            entityShape,
                            tex.Key,
                            tex.Value,
                            partTextureWidth,
                            partTextureHeight,
                            shapePathForLogging
                        );
                    }
                }

                foreach (var val in partShape.TextureSizes)
                {
                    entityShape.TextureSizes[val.Key] = val.Value;
                }
            }

            return entityShape;
        }

        /// Used for loading extra textures for skin parts, which do not
        /// use the part's main texture target.
        private void loadTexture(
            AppliedKemonoSkinnablePartVariant part,
            Shape entityShape,
            string code,
            AssetLocation shapeTexloc,
            int textureWidth,
            int textureHeight,
            string shapePathForLogging
        ) {
            var textures = entity.Properties.Client.Textures;
            ICoreClientAPI capi = entity.World.Api as ICoreClientAPI;

            var cmpt = textures[code] = new CompositeTexture(shapeTexloc);
            cmpt.Bake(capi.Assets);

            int textureSubId = 0;

            IAsset texAsset = capi.Assets.TryGet(shapeTexloc.Clone().WithPathPrefixOnce("textures/").WithPathAppendixOnce(".png"));
            if (texAsset != null)
            {
                BitmapRef bmp = texAsset.ToBitmap(capi);
                capi.EntityTextureAtlas.GetOrInsertTexture(
                    cmpt.Baked.TextureFilenames[0],
                    out textureSubId,
                    out _,
                    () => { return bmp; },
                    -1.0f
                );
            }
            else
            {
                capi.World.Logger.Warning($"[BehaviorKemonoSkinnable.loadTexture] Skin part shape {shapePathForLogging} defined texture {shapeTexloc}, no such texture found.");
            }

            cmpt.Baked.TextureSubId = textureSubId;

            entityShape.TextureSizes[code] = new int[] { textureWidth, textureHeight };
            textures[code] = cmpt;
        }

        // private void loadTexture(Shape entityShape, string code, AssetLocation location, int textureWidth, int textureHeight, string shapePathForLogging)
        // {
        //     var textures = entity.Properties.Client.Textures;
        //     ICoreClientAPI capi = entity.World.Api as ICoreClientAPI;

        //     var cmpt = textures[code] = new CompositeTexture(location);
        //     cmpt.Bake(capi.Assets);
        //     if (!capi.EntityTextureAtlas.GetOrInsertTexture(cmpt.Baked.TextureFilenames[0], out int textureSubid, out _, null, -1))
        //     {
        //         capi.Logger.Warning("Skin part shape {0} defined texture {1}, no such texture found.", shapePathForLogging, location);
        //     }
        //     cmpt.Baked.TextureSubId = textureSubid;

        //     entityShape.TextureSizes[code] = new int[] { textureWidth, textureHeight };
        //     textures[code] = cmpt;
        // }

        /// Apply part color tint to texture, render colorized texture into atlas.
        /// Currently only uses multiply blend mode for color tinting.
        /// In future could add other blend modes. 
        private void renderColorizedTexture(
            ICoreClientAPI capi,
            KemonoSkinnablePart part,
            AppliedKemonoSkinnablePartVariant applied,
            IBitmap bmp,
            LoadedTexture texture,
            TextureAtlasPosition texAtlasPos,
            bool linearMag = false, // use nearest rendering
            int clampMode = 0       // clamp mode
        ) {
            int[] texPixels = bmp.Pixels;

            if (part.UseColorSlider && applied.Color != 0)
            {
                // multiply part color onto texture bitmap
                BitmapSimple bmpColored = new BitmapSimple(bmp);
                bmpColored.MultiplyRgb(applied.Color);
                texPixels = bmpColored.Pixels;
            }

            capi.Render.LoadOrUpdateTextureFromRgba(
                texPixels,
                linearMag,
                clampMode,
                ref texture
            );

            int posx = part.TextureRenderTo.X;
            int posy = part.TextureRenderTo.Y;
            float alphaTest = 0f;

            if (part.TextureBlendOverlay)
            {
                capi.Render.GlToggleBlend(false, EnumBlendMode.Standard);
                capi.Render.GlToggleBlend(true, EnumBlendMode.Overlay);
            }
            else
            {
                capi.Render.GlToggleBlend(false, EnumBlendMode.Overlay);
                capi.Render.GlToggleBlend(true, EnumBlendMode.Standard);
                // idk why but we need to use -1 when not overlaying and 0 when overlaying
                // fuck this game
                alphaTest = -1f;
            }

            capi.EntityTextureAtlas.RenderTextureIntoAtlas(
                texAtlasPos.atlasTextureId,
                texture,
                0,
                0,
                texture.Width,
                texture.Height,
                texAtlasPos.x1 * capi.EntityTextureAtlas.Size.Width + posx,
                texAtlasPos.y1 * capi.EntityTextureAtlas.Size.Height + posy,
                alphaTest
            );
        }


        /// Renders custom emblem pixels into texture atlas location. 
        private void renderEmblemToAtlas(
            ICoreClientAPI capi,
            KemonoSkinnablePart part,
            TextureAtlasPosition texAtlasPos
        ) {
            LoadedTexture emblemTexture = GetEmblemTexture();
            LoadedTexture clearTexture = GetEmblemClearTexture();

            int posx = part.TextureRenderTo.X;
            int posy = part.TextureRenderTo.Y;

            // atlas is retarded, need to clear pixels first, then
            // render the emblem pixels in.
            // the blend modes dont work properly lol lmao

            // this clears the atlas render location
            // i have no idea why this blend mode works lol lmao
            capi.Render.GlToggleBlend(true, EnumBlendMode.Glow);
            capi.EntityTextureAtlas.RenderTextureIntoAtlas(
                texAtlasPos.atlasTextureId,
                clearTexture,
                0,
                0,
                clearTexture.Width,
                clearTexture.Height,
                texAtlasPos.x1 * capi.EntityTextureAtlas.Size.Width + posx,
                texAtlasPos.y1 * capi.EntityTextureAtlas.Size.Height + posy,
                -1f
            );
            capi.Render.GlToggleBlend(false, EnumBlendMode.Glow);

            // render actual pixels into atlas
            capi.EntityTextureAtlas.RenderTextureIntoAtlas(
                texAtlasPos.atlasTextureId,
                emblemTexture,
                0,
                0,
                emblemTexture.Width,
                emblemTexture.Height,
                texAtlasPos.x1 * capi.EntityTextureAtlas.Size.Width + posx,
                texAtlasPos.y1 * capi.EntityTextureAtlas.Size.Height + posy,
                -1f
            );
        }

        /// Returns animation cache key for this entity and selected model.
        public string AnimCacheKey()
        {
            return entity.Code + "-" + Model.Model.ToString();
        }

        /// Clear animation cache for cache key. Must clear when hot-reloading
        /// shape animations.
        public void ClearAnimationCache(string animCacheKey)
        {
            object animCacheObj;
            entity.Api.ObjectCache.TryGetValue("animCache", out animCacheObj);
            
            Dictionary<string, AnimCacheEntry> animCache;
            animCache = animCacheObj as Dictionary<string, AnimCacheEntry>;
            if (animCache == null)
            {
                return; // nothing to clear
            }

            if (animCache.ContainsKey(animCacheKey))
            {
                animCache.Remove(animCacheKey);
            }
        }

        /// Copy of animation cache initialization, but allows inputting anim cache key.
        /// https://github.com/anegostudios/vsapi/blob/master/Common/Model/Animation/AnimationCache.cs#L48
        public static IAnimationManager UpdateAnimations(
            ICoreAPI api,
            IAnimationManager manager,
            Entity entity,
            Shape entityShape,
            string animCacheKey,
            params string[] requireJointsForElements
        )
        {
            object animCacheObj;
            Dictionary<string, AnimCacheEntry> animCache;
            entity.Api.ObjectCache.TryGetValue("animCache", out animCacheObj);
            animCache = animCacheObj as Dictionary<string, AnimCacheEntry>;
            if (animCache == null)
            {
                entity.Api.ObjectCache["animCache"] = animCache = new Dictionary<string, AnimCacheEntry>();
            }

            IAnimator animator;

            AnimCacheEntry cacheObj;
            if (animCache.TryGetValue(animCacheKey, out cacheObj))
            {
                //// should not need to do, already done
                // manager.Init(entity.Api, entity);
                
                animator = api.Side == EnumAppSide.Client ? 
                    ClientAnimator.CreateForEntity(entity, cacheObj.RootPoses, cacheObj.Animations, cacheObj.RootElems, entityShape.JointsById) :
                    ServerAnimator.CreateForEntity(entity, cacheObj.RootPoses, cacheObj.Animations, cacheObj.RootElems, entityShape.JointsById)
                ;

                manager.Animator = animator;

            } else {
                //// should not need to do, already done
                // entityShape.ResolveAndLoadJoints(requireJointsForElements);
                // manager.Init(entity.Api, entity);

                IAnimator animatorbase = api.Side == EnumAppSide.Client ?
                    ClientAnimator.CreateForEntity(entity, entityShape.Animations, entityShape.Elements, entityShape.JointsById) :
                    ServerAnimator.CreateForEntity(entity, entityShape.Animations, entityShape.Elements, entityShape.JointsById)
                ;

                manager.Animator = animatorbase;

                animCache[animCacheKey] = new AnimCacheEntry()
                {
                    Animations = entityShape.Animations,
                    RootElems = (animatorbase as ClientAnimator).rootElements,
                    RootPoses = (animatorbase as ClientAnimator).RootPoses
                };
            }
            
            return manager;
        }
    }
}
