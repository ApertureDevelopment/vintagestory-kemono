using System;
using System.Collections.Generic;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.MathTools;

namespace kemono
{
    /// <summary>
    /// Stores an allocated texture atlas location. Each texture render
    /// target (e.g. #main, #hair, #emblem, etc.) needs an allocated
    /// texture location.
    /// </summary>
    public class KemonoTextureAtlasAllocation
    {
        // texture allocation width
        public int Width;
        // texture allocation height
        public int Height;
        // allocated texture id
        public int TextureSubId;
        // allocated texture position in entity texture atlas
        public TextureAtlasPosition TexPos;
    }

    // based on:
    // https://github.com/anegostudios/vsessentialsmod/blob/008c6970f51ec2f5358da912b9965c37446ca44f/EntityRenderer/EntitySkinnableShapeRenderer.cs

    /// <summary>
    /// Handles kemono skinnable shape rendering and manages memory for
    /// texture atlas allocations.
    /// </summary>
    public class EntityKemonoSkinnableShapeRenderer : Vintagestory.GameContent.EntityShapeRenderer
    {
        public event Action<LoadedTexture, TextureAtlasPosition> OnReloadSkin;

        public int skinTextureSubId { get; protected set; }

        // needed for EntityKemonoPlayerShapeRenderer
        protected bool IsSelf => entity.EntityId == capi.World.Player.Entity.EntityId;

        // copied from EntitySkinnableShapeRenderer
        public double RenderOrder => 1;
        public int RenderRange => 1;

        // initialization sequence needs to be:
        // tesselate -> reload skin -> tesselate
        // flag forces an initial double-tesselation after skin is loaded
        public bool SkinNeedsInitialization = true;

        // flag that skin was reloaded
        public bool SkinDirty = true;

        // flag that shape should modify and reload using MarkDirty
        public bool DoReloadShapeAndSkin = true;

        // texture atlas allocations: texture target name => texture atlas allocation
        // e.g. "#hair" => TextureAtlasAllocation
        public Dictionary<string, KemonoTextureAtlasAllocation> TextureAllocations = new Dictionary<string, KemonoTextureAtlasAllocation>();

        // main allocation for body texture, used for overlaying clothes
        // points to same object inside TextureAllocations for "main"
        public KemonoTextureAtlasAllocation MainTexture { get; protected set; }

        // hard-coded name for main body texture
        public const string MAIN_TEXTURE = "main";

        // hard-coded size for main body texture
        // LONG TERM TODO: make this configurable
        // for now, model main texture width/height has to match this
        public const int MAIN_TEXTURE_WIDTH = 128;
        public const int MAIN_TEXTURE_HEIGHT = 128;


        public EntityKemonoSkinnableShapeRenderer(Entity entity, ICoreClientAPI api) : base(entity, api)
        {
            api.Event.ReloadTextures += MarkShapeModified;
        }

        /// <summary>
        /// Return texture atlas position for target texture name.
        /// For kemono entities, this will try to search for entity-specific
        /// "keyed" textures (e.g. "hair-1234" for entity with id 1234)
        /// before falling back to vanilla shared textures.
        /// </summary>
        /// <param name="textureCode"></param>
        /// <returns></returns>
        public override TextureAtlasPosition this[string textureCode]
        {
            get
            {
                // first try to return unique per-entity keyed texture
                string key = GetTextureAtlasKey(textureCode);
                if (entity.Properties.Client.Textures.TryGetValue(key, out CompositeTexture tex))
                {
                    return capi.EntityTextureAtlas.Positions[tex.Baked.TextureSubId];
                }

                // fallback to vanilla shared textures
                return base[textureCode];
            }
        }

        /// <summary>
        /// Get unique entity-specific atlas key for a texture.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GetTextureAtlasKey(string name) 
        {   
            return $"{name}-{entity.EntityId}";
        }

        /// <summary>
        /// Allocate texture target for skin part specific to this entity.
        /// </summary>
        /// 
        /// <param name="name"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public KemonoTextureAtlasAllocation GetOrAllocateTextureTarget(
            string name,
            int width,
            int height
        ) {
            // ASSUMES THIS SIDE IS CLIENT
            // if (entity.World.Side != EnumAppSide.Client) return;

            string key = GetTextureAtlasKey(name);

            if (TextureAllocations.TryGetValue(key, out KemonoTextureAtlasAllocation tex))
            {
                return tex; // found existing allocation for target
            }

            // need to create new allocation
            ICoreClientAPI capi = entity.World.Api as ICoreClientAPI;

            capi.EntityTextureAtlas.AllocateTextureSpace(
                width,
                height,
                out int textureSubId,
                out TextureAtlasPosition texPos
            );

            // write empty alpha pixels into texture allocation
            // otherwise previous allocation pixel history will corrupt
            // due to the nonfunctional blend modes
            LoadedTexture clearTexture = new LoadedTexture(capi)
            {
                Width = width,
                Height = height
            };
            capi.Render.LoadOrUpdateTextureFromRgba(
                new int[width * height],
                false,
                0,
                ref clearTexture
            );

            capi.Render.GlToggleBlend(false, EnumBlendMode.Standard);
            capi.Render.GlToggleBlend(false, EnumBlendMode.Overlay);

            capi.EntityTextureAtlas.RenderTextureIntoAtlas(
                texPos.atlasTextureId,
                clearTexture,
                0,
                0,
                width,
                height,
                texPos.x1 * capi.EntityTextureAtlas.Size.Width,
                texPos.y1 * capi.EntityTextureAtlas.Size.Height,
                -1f
            );
            clearTexture.Dispose();

            var alloc = new KemonoTextureAtlasAllocation
            {
                Width = width,
                Height = height,
                TextureSubId = textureSubId,
                TexPos = texPos
            };

            TextureAllocations[key] = alloc;

            var keyAsAssetLocation = new AssetLocation("kemono", key);
            CompositeTexture ctex = new CompositeTexture() { Base = keyAsAssetLocation };
            ctex.Baked = new BakedCompositeTexture() { BakedName = keyAsAssetLocation, TextureSubId = textureSubId };
            
            // inject allocated textures into entity shape renderer
            entity.Properties.Client.Textures[key] = ctex;

            return alloc;
        }

        public void FreeTextureTarget(string name)
        {
            if (entity.World.Side != EnumAppSide.Client) return;

            ICoreClientAPI capi = entity.World.Api as ICoreClientAPI;

            string key = GetTextureAtlasKey(name);

            if (TextureAllocations.TryGetValue(key, out KemonoTextureAtlasAllocation tex))
            {
                capi.EntityTextureAtlas.FreeTextureSpace(tex.TextureSubId);
                TextureAllocations.Remove(key);
                entity.Properties.Client.Textures.Remove(key);
            }
        }

        public void FreeAllTextureTargets()
        {
            if (entity.World.Side != EnumAppSide.Client) return;

            ICoreClientAPI capi = entity.World.Api as ICoreClientAPI;
            var textures = entity.Properties.Client.Textures;

            foreach (var alloc in TextureAllocations)
            {
                string key = alloc.Key;
                KemonoTextureAtlasAllocation tex = alloc.Value;

                capi.EntityTextureAtlas.FreeTextureSpace(tex.TextureSubId);
                textures.Remove(key);
            }

            TextureAllocations.Clear();

            // remove main texture reference
            MainTexture = null;
        }

        protected override ITexPositionSource GetTextureSource()
        {
            if (MainTexture == null)
            {
                // do an initial allocation of main texture so that
                // `skinTexPos` points to a valid texture atlas location
                var alloc = GetOrAllocateTextureTarget(
                    MAIN_TEXTURE,
                    MAIN_TEXTURE_WIDTH,
                    MAIN_TEXTURE_HEIGHT
                );
                MainTexture = alloc;
                skinTexPos = alloc.TexPos;
                skinTextureSubId = alloc.TextureSubId;
            }

            return base.GetTextureSource();
        }

        public override void MarkShapeModified()
        {
            if (!DoReloadShapeAndSkin) return;

            base.MarkShapeModified();
        }

        public override void TesselateShape()
        {
            if (eagent is EntityPlayer && eagent.GearInventory == null) return; // Player is not fully initialized yet

            base.TesselateShape();

            if (eagent.GearInventory != null)
            {
                reloadSkin();
            }
        }

        public override void reloadSkin()
        {
            if (!DoReloadShapeAndSkin) return;

            SkinDirty = false;

            TextureAtlasPosition origTexPos = capi.EntityTextureAtlas.Positions[entity.Properties.Client.FirstTexture.Baked.TextureSubId];
            string skinBaseTextureKey = entity.Properties.Attributes?["skinBaseTextureKey"].AsString();
            if (skinBaseTextureKey != null) origTexPos = capi.EntityTextureAtlas.Positions[entity.Properties.Client.Textures[skinBaseTextureKey].Baked.TextureSubId];


            LoadedTexture entityAtlas = new LoadedTexture(null)
            {
                TextureId = origTexPos.atlasTextureId,
                Width = capi.EntityTextureAtlas.Size.Width,
                Height = capi.EntityTextureAtlas.Size.Height
            };

            capi.Render.GlToggleBlend(false, EnumBlendMode.Standard);

            // skip vanilla writing base texture to atlas,
            // instead done inside behavior as a skin part.
            // capi.EntityTextureAtlas.RenderTextureIntoAtlas(
            //     entityAtlas,
            //     (int)(origTexPos.x1 * AtlasSize.Width),
            //     (int)(origTexPos.y1 * AtlasSize.Height),
            //     (int)((origTexPos.x2 - origTexPos.x1) * AtlasSize.Width),
            //     (int)((origTexPos.y2 - origTexPos.y1) * AtlasSize.Height),
            //     skinTexPos.x1 * capi.EntityTextureAtlas.Size.Width,
            //     skinTexPos.y1 * capi.EntityTextureAtlas.Size.Height,
            //     -1
            // );

            OnReloadSkin?.Invoke(entityAtlas, skinTexPos);

            capi.Render.GlToggleBlend(false, EnumBlendMode.Standard);
            capi.Render.GlToggleBlend(true, EnumBlendMode.Overlay);

            int[] renderOrder = new int[]
            {
                (int)EnumCharacterDressType.LowerBody,
                (int)EnumCharacterDressType.Foot,
                (int)EnumCharacterDressType.UpperBody,
                (int)EnumCharacterDressType.UpperBodyOver,
                (int)EnumCharacterDressType.Waist,
                (int)EnumCharacterDressType.Shoulder,
                (int)EnumCharacterDressType.Emblem,
                (int)EnumCharacterDressType.Neck,
                (int)EnumCharacterDressType.Head,
                (int)EnumCharacterDressType.Hand,
                (int)EnumCharacterDressType.Arm,
                (int)EnumCharacterDressType.Face
            };

            if (gearInv == null && eagent?.GearInventory != null)
            {
                registerSlotModified(false);
            }

            for (int i = 0; i < renderOrder.Length; i++)
            {
                int slotid = renderOrder[i];

                ItemStack stack = gearInv[slotid]?.Itemstack;
                if (stack == null) continue;
                if (eagent.hideClothing) continue;
                if (stack.Item.FirstTexture == null) continue; // Invalid/Unknown/Corrupted item

                int itemTextureSubId = stack.Item.FirstTexture.Baked.TextureSubId;

                TextureAtlasPosition itemTexPos = capi.ItemTextureAtlas.Positions[itemTextureSubId];

                LoadedTexture itemAtlas = new LoadedTexture(null)
                {
                    TextureId = itemTexPos.atlasTextureId,
                    Width = capi.ItemTextureAtlas.Size.Width,
                    Height = capi.ItemTextureAtlas.Size.Height
                };

                capi.EntityTextureAtlas.RenderTextureIntoAtlas(
                    skinTexPos.atlasTextureId,
                    itemAtlas,
                    itemTexPos.x1 * capi.ItemTextureAtlas.Size.Width,
                    itemTexPos.y1 * capi.ItemTextureAtlas.Size.Height,
                    (itemTexPos.x2 - itemTexPos.x1) * capi.ItemTextureAtlas.Size.Width,
                    (itemTexPos.y2 - itemTexPos.y1) * capi.ItemTextureAtlas.Size.Height,
                    skinTexPos.x1 * capi.EntityTextureAtlas.Size.Width,
                    skinTexPos.y1 * capi.EntityTextureAtlas.Size.Height
                );
            }

            capi.Render.GlToggleBlend(true);
            capi.Render.BindTexture2d(skinTexPos.atlasTextureId);
            capi.Render.GlGenerateTex2DMipmaps();
        }


        public override void Dispose()
        {
            base.Dispose();

            capi.Event.ReloadTextures -= MarkShapeModified;
            if (eagent?.GearInventory != null)
            {
                eagent.GearInventory.SlotModified -= gearSlotModified;
            }

            capi.EntityTextureAtlas.FreeTextureSpace(skinTextureSubId);
        }
    }
}
